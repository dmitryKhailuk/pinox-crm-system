<?php

namespace Pinox\ToolsBundle\Form;


use Pinox\ToolsBundle\Entity\Participant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParticipantType extends AbstractType
{
    const PARTICIPANT_UPDATE = 'participant_update';
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options[self::PARTICIPANT_UPDATE]) {
            $builder->add('email', EntityType::class, [
                'label' => 'participant.email',
                'class' => 'PinoxToolsBundle:Participant',
                'choice_label' => 'email',
                'required' => false,
            ]);

        } else {
            $builder->add('name', TextType::class, [
                'label' => 'participant.name',
                'required' => true,
                'attr' => [
                    'maxlength' => 120
                ]
            ]);

            $builder->add('email', EmailType::class, [
                'label' => 'participant.email',
                'required' => true,
                'attr' => [
                    'maxlength' => 100
                ]
            ]);
        }

        $builder->add('distance', IntegerType::class, [
            'label' => 'participant.distance',
            'required' => true,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participant::class,
            self::PARTICIPANT_UPDATE => false,
        ]);

        $resolver->setAllowedValues(self::PARTICIPANT_UPDATE, [true, false]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_tools_participant';
    }
}