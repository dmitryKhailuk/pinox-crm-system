<?php

namespace Pinox\ToolsBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="ParticipantRepository")
 * @ORM\Table(
 *     name="participants",
 *     schema="pinox_tool",
 *     indexes={
 *          @ORM\Index(name="participants_email_unq_idx", columns={"email"})
 *      }
 * )
 * @UniqueEntity(
 *     "email",
 *     repositoryMethod="findInterferingParticipantByCriteria",
 *     message="participant.error_index"
 * )
 */
class Participant
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Email(message = "participant.error_email")
     * @Assert\Length(max="100")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="120")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="distance", type="integer", nullable=false, options={"default"=true})
     * @Assert\GreaterThanOrEqual(value=0)
     * @var integer
     */
    private $distance = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Participant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return Participant
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Participant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
