<?php

namespace Pinox\ToolsBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ParticipantRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingParticipantByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('p')
            ->where('p.email = :email')
            ->setParameter('email', $criteria['email'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return Participant[]
     */
    public function allParticipants()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.distance ', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @param $participantId
     * @return integer
     */
    public function getDistance($participantId)
    {
        return $this->createQueryBuilder('p')
            ->select('p.distance')
            ->where('p.id = :id')
            ->setParameter('id', $participantId)
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function getAllParticipants()
    {
        return $this->createQueryBuilder('p')
            ->select('p.distance, p.name')
            ->orderBy('p.distance', 'DESC')
            ->getQuery()->getArrayResult();
    }
}
