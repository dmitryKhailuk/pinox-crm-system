<?php

namespace Pinox\ToolsBundle\Services;

use Doctrine\ORM\EntityManager;
use Pinox\AffiseBundle\Entity\Offer;
use Pinox\AffiseBundle\Services\AffiseSystem;
use Pinox\DashboardBundle\Services\MailChimpApi;
use Symfony\Bundle\TwigBundle\TwigEngine;

class BuilderMailChimpTemplate
{

    const PINOX_URL = 'https://pinox.com';
    const SERVER_NAME = 'http://tools.pinox.com';
    const PINOX_SYSTEM = 'https://my.pinox.com/offer';
    const OFFICE_URL = 'http://office.pinox.com';

    /** @var  EntityManager $em */
    private $em = null;

    /** @var  AffiseSystem $em */
    private $affiseSystem = null;

    /** @var  TwigEngine $em */
    private $templating = null;

    /** @var BuilderMailChimpTemplate */
    public static $instance = null;

    /**
     * BuilderMailChimpTemplate constructor.
     * @param EntityManager $em
     * @param AffiseSystem $affiseSystem
     * @param TwigEngine $templating
     */
    private function __construct(EntityManager $em, AffiseSystem $affiseSystem, TwigEngine $templating)
    {
        $this->em = $em;
        $this->affiseSystem = $affiseSystem;
        $this->templating = $templating;
    }

    private function __clone()
    {
    }

    /**
     * @param EntityManager $em
     * @param AffiseSystem $affiseSystem
     * @param TwigEngine $templating
     * @return BuilderMailChimpTemplate
     */
    public function getInstance(EntityManager $em, AffiseSystem $affiseSystem, TwigEngine $templating)
    {
        if (null === self::$instance) {
            self::$instance = new BuilderMailChimpTemplate($em, $affiseSystem, $templating);
        }

        return self::$instance;
    }

    /**
     * @param array $data
     * @param string $pathFlags
     * @return string
     */
    public function getPreviewTemplate(array $data = [], $pathFlags)
    {
        $offers = $this->em->getRepository('PinoxAffiseBundle:Offer')->getNotSendOffers();

        $html = $this->getTemplate($pathFlags, $offers, $data);

        return $html;
    }

    /**
     * @param array $data
     * @param string $pathFlags
     * @param MailChimpApi $mailChumpApi
     * @return array
     */
    public function generateTemplateForMailChimp(array $data = [], $pathFlags, MailChimpApi $mailChumpApi)
    {
        $date = new \DateTime('now');

        $offers = $this->em->getRepository('PinoxAffiseBundle:Offer')->getNotSendOffers();

        $html = $this->getTemplate($pathFlags, $offers, $data);

        /** @var  $offer Offer */
        foreach ($offers as $offer) {
            $offer->setIsSend(true);
            $this->em->persist($offer);
        }

        $this->em->flush();

        $response = $mailChumpApi->createTemplate('Offers_' . $date->format('d.m.Y'), $html);
        return $response;
    }

    /**
     * @param array $data
     * @return array
     */
    private function getTopOffers(array $data = [])
    {
        $topOffers = [];

        if (array_key_exists('topOffers', $data) && $data['topOffers']) {
            $offersId = explode(';', $data['topOffers']);
            foreach ($offersId as $value) {
                $id = trim($value);
                if ($id && is_numeric($id)) {
                    $offer = $this->affiseSystem->getOfferById($id);
                    if ($offer) {
                        $topOffers[] = $offer;
                    }
                }
            }

        } else {
            $topOffers = $this->affiseSystem->getTopOffers(Offer::TOP_OFFERS_LIMIT);
        }

        return $topOffers;
    }

    /**
     * @param array $data
     * @return Offer[]
     */
    private function getHotOffers(array $data = [])
    {
        $hotOffers = [];
        if (array_key_exists('hotOffers', $data) && $data['hotOffers']) {
            $offersId = explode(';', $data['hotOffers']);
            foreach ($offersId as $key => $value) {
                $id = trim($value);
                if ($id && is_numeric($id)) {
                    $hotOffers[] = $this->em->getRepository('PinoxAffiseBundle:Offer')->getOfferWithTitleMailChimp($id);
                }
            }

        }

        return $hotOffers;
    }

    /**
     * @param Offer[] $offers
     * @return array
     */
    private function getMappingOfferByCategory($offers)
    {
        $mappingOffersByCategory = [];
        foreach ($offers as $key => $offer) {
            $mappingOffersByCategory[$offer->getCategory()->getName()][$key] = $offer;
        }

        return $mappingOffersByCategory;
    }

    /**
     * @param string $pathFlags
     * @param Offer[] $offers
     * @param array $data
     * @return string
     */
    private function getTemplate($pathFlags, $offers, array $data = [])
    {
        $mappingOffersByCategory = $this->getMappingOfferByCategory($offers);
        $codes = $this->em->getRepository('PinoxDashboardBundle:Country')->getCodeCountries();

        $html = $this->templating->render('PinoxDashboardBundle:templates:offers_mail.html.twig', [
            'url' => self::PINOX_URL,
            'pathFlags' => $pathFlags,
            'offers' => $offers,
            'topOffers' => $this->getTopOffers($data),
            'serverUrl' => self::SERVER_NAME,
            'myPinox' => self::PINOX_SYSTEM,
            'codes' => $codes,
            'offersByCategory' => $mappingOffersByCategory,
            'title' => $this->getTitle($data),
            'hotOffers' => $this->getHotOffers($data),
            'officeUrl' => self::OFFICE_URL,
        ]);

        return $html;
    }

    /**
     * @param array $data
     * @return string
     */
    private function getTitle(array $data = [])
    {
        return array_key_exists('title', $data) ? trim($data['title']) : '';
    }
}
