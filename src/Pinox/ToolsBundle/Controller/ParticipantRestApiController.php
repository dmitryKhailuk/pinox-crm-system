<?php
namespace Pinox\ToolsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * необходимо будет удалить и написать как рест апи
 */

/**
 * @Route("/tools/participants")
 */
class ParticipantRestApiController extends Controller
{
    /**
     * @Route("/all", name="rest_api_get_all_participants")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \LogicException
     */
    public function allAction()
    {
            $em = $this->getDoctrine()->getManager();
            $users = $em->getRepository('PinoxToolsBundle:Participant')->getAllParticipants();

        return new JsonResponse($users);
    }
}