<?php

namespace Pinox\ToolsBundle\Controller;

use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Pinox\ToolsBundle\Entity\Participant;
use Pinox\ToolsBundle\Form\ParticipantType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/tools/info_about_participants")
 * @Security("has_role('ROLE_PINOX_TOOLS_USER') or has_role('ROLE_SHOW_MODULE_TOOL')")
 */
class ParticipantController extends Controller
{
    use ProcessesEntityRemovalTrait;
    /**
     * @Route("/", name="participant_index")
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $participants = $em->getRepository('PinoxToolsBundle:Participant')->allParticipants();
        $form = $this->createForm(ParticipantType::class, null, [
            ParticipantType::PARTICIPANT_UPDATE => true,
        ]);

        return $this->render('PinoxToolsBundle:Participant:index.html.twig', [
            'participants' => $participants,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="participant_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {

        $participant = new Participant();
        $form = $this->createForm(ParticipantType::class, $participant);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($participant);
            $em->flush();

            return $this->redirect($this->generateUrl('participant_index'));
        }

        return $this->render('PinoxToolsBundle:Participant:form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/update_distance/{id}", name="participant_update_distance")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function updateDistanceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $participantData =$request->request->get('participant');
        $distance = $participantData['distance'];

        $participant = $em->getRepository('PinoxToolsBundle:Participant')
            ->findOneBy(['id' => $participantData['email']]);

        if ($participant && $distance && is_numeric($distance) && $distance > 0) {
            $participant = $em->getRepository('PinoxToolsBundle:Participant')
                ->findOneBy(['id' => $participantData['email']]);
            $participant->setDistance($distance);
            $em->persist($participant);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('participant_index'));
    }

    /**
     * @Route("/delete/{id}", name="participant_delete")
     * @Method("DELETE")
     * @ParamConverter("participant", class="PinoxToolsBundle:Participant")
     * @param Request $request
     * @param Participant $participant
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Participant $participant)
    {
        $this->processEntityRemoval(
            $participant,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('participant_index'));
    }

    /**
     * @Route("/get_distance", name="participant_get_distance")
     * @Method({"POST", "GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \LogicException
     */
    public function deleteRateCurrencyAction(Request $request)
    {
        $participantId = $request->query->get('participantId');
        $em = $this->getDoctrine()->getManager();
        $distance = $em->getRepository('PinoxToolsBundle:Participant')->getDistance($participantId);
        return new JsonResponse($distance);
    }
}
