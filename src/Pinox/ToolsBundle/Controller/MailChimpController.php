<?php

namespace Pinox\ToolsBundle\Controller;

use Pinox\AffiseBundle\Entity\Offer;
use Pinox\AffiseBundle\Form\OfferTopType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/tools/mail_chimp")
 * @Security("has_role('ROLE_PINOX_TOOLS_USER') or has_role('ROLE_SHOW_MODULE_TOOL')")
 */
class MailChimpController extends Controller
{
    /**
     * @Route("/", name="mail_chimp_index")
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $newOffersNoSend = $em->getRepository('PinoxAffiseBundle:Offer')->getNotSendOffers();
        $form = $this->createForm(OfferTopType::class);
        $topOffers = $this->get('pinox_affise_system')->getTopOffers(Offer::TOP_OFFERS_LIMIT);

        return $this->render('PinoxToolsBundle:MailChimp:index.html.twig', [
            'newOffersNoSend' => $newOffersNoSend,
            'form' => $form->createView(),
            'topOffers' => $topOffers
        ]);
    }

    /**
     * @Route("/generate", name="mail_chimp_generate")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function generateAction(Request $request)
    {
        set_time_limit(10000);
        $form = $this->createForm(OfferTopType::class);
        $form->handleRequest($request);
        $response = [];

        if ($form->isValid() && $form->isSubmitted()) {
            $data = $form->getData();
            $pathFlags = $this->getParameter('flags_path');
            $response = $this->get('pinox_tools.builder_mail_chimp_template')
                ->generateTemplateForMailChimp($data, $pathFlags, $this->get('pinox_mailchimp_api'));

        }

        if (array_key_exists('id', $response)) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('success_alerts.generate')
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('danger_alerts.generate')
            );
        }

        return $this->redirect($this->generateUrl('mail_chimp_index'));
    }

    /**
     * @Route("/preview", name="mail_chimp_preview")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function previewAction(Request $request)
    {
        $form = $this->createForm(OfferTopType::class);
        $html = '';

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $data = $form->getData();
            $pathFlags = $this->getParameter('flags_path');
            $html = $this->get('pinox_tools.builder_mail_chimp_template')->getPreviewTemplate($data, $pathFlags);
        }

        if ($html) {
            return new Response($html);
        }

        $this->get('session')->getFlashBag()->add(
            'danger',
            $this->get('translator')->trans('danger_alerts.preview_fail')
        );

        return $this->redirect($this->generateUrl('mail_chimp_index'));
    }
}
