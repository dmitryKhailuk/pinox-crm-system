<?php

namespace Pinox\TeamBundle\Controller;

use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Pinox\TeamBundle\Entity\Employee;
use Pinox\TeamBundle\Form\EmployeeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/employees")
 * @Security("has_role('ROLE_SHOW_MODULE_TEAM')")
 */
class EmployeeController extends Controller
{
    use ProcessesEntityRemovalTrait, GettingExchangeRatesAndAddToDb;

    /**
     * @Route("/{page}", name="employee_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_TEAM') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $sum = 0;

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('PinoxTeamBundle:Employee')->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Employee::EMPLOYEES_PER_PAGE
        );

        $amountWorkedEmployee = $em->getRepository('PinoxTeamBundle:Employee')->countWorkedEmployee();

        $salary = $em->getRepository('PinoxTeamBundle:Employee')->allSalaryEmployee();

        $usdCurrency = $em->getRepository('PinoxBookkeepingBundle:Currency')->findOneBy(['code' => Currency::CODE_USD]);
        $rates['rate'] = $this->gettingExchangeRatesAndAddToDb($usdCurrency, $em);
        $rates['currencyName'] = $usdCurrency->getName();
        $rates['currencyCode'] = $usdCurrency->getCode();

        if (count($salary) > 0 && $rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $total = $converter->converterCurrency($salary, $rates);
            foreach ($total as $item) {
                $sum += $item['payment'];
            }
        }

        return $this->render('@PinoxTeam/Employee/index.html.twig', [
            'pagination' => $pagination,
            'amountWorkedEmployee' => $amountWorkedEmployee,
            'salary' => $sum,
        ]);
    }

    /**
     * @Route("/new", name="employee_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($employee);
            $em->flush();

            return $this->redirect($this->generateUrl('employee_index'));
        }

        return $this->render(
            '@PinoxTeam/Employee/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="employee_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("employee", class="PinoxTeamBundle:Employee")
     * @param Request $request
     * @param Employee $employee
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Employee $employee)
    {
        $form = $this->createForm(EmployeeType::class, $employee, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($employee);
            $em->flush();

            return $this->redirect($this->generateUrl('employee_index'));
        }

        return $this->render('@PinoxTeam/Employee/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="employee_delete")
     * @Method("DELETE")
     * @ParamConverter("employee", class="PinoxTeamBundle:Employee")
     * @param Request $request
     * @param Employee $employee
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Employee $employee)
    {
        $this->processEntityRemoval(
            $employee,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('employee_index'));
    }

    /**
     * @Route("/archive", name="employee_archive")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function archiveAction()
    {
        $em = $this->getDoctrine()->getManager();
        $archiveEmployees = $em->getRepository('PinoxTeamBundle:Employee')->getArchiveEmployees();

        return $this->render('@PinoxTeam/Employee/archive.html.twig', [
            'archiveEmployees' => $archiveEmployees,
        ]);
    }
}
