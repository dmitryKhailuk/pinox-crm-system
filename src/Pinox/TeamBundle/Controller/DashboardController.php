<?php

namespace Pinox\TeamBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route("/dashboard")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/view", name="dashboard_view")
     * @Security("has_role('ROLE_SHOW_MODULE_TEAM') or has_role('ROLE_DEMO_USER')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $departmentsTree = [];
        $em = $this->getDoctrine()->getManager();

        $departments = $em->getRepository('PinoxTeamBundle:Department')->getDepartmentsForTree();

        foreach ($departments as $key => $department) {
            if (empty($department['parentId']) || $department['id'] === $department['parentId']) {
                $departments[$key]['parentId'] = 0;
            }
            $departmentsTree[$departments[$key]['parentId']][] = $departments[$key];
        }

        $tree = $this->createTree($departmentsTree, $departmentsTree[0]);
        return $this->render('@PinoxTeam/Dashboard/index.html.twig', [
            'tree' => $tree
        ]);
    }

    /**
     * @param $parents
     * @param $children
     * @return array
     */
    private function createTree(&$parents, $children) {
        $tree = [];

        foreach ($children as $child){

            if(isset($parents[$child['id']])) {
                $child['children'] = $this->createTree($parents, $parents[$child['id']]);
            }

            $tree[] = $child;
        }
        return $tree;
    }
}