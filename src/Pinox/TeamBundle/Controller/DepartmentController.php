<?php

namespace Pinox\TeamBundle\Controller;

use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Pinox\TeamBundle\Form\DepartmentType;
use Pinox\TeamBundle\Entity\Department;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/departments")
 * @Security("has_role('ROLE_SHOW_MODULE_TEAM')")
 */
class DepartmentController extends Controller
{
    use ProcessesEntityRemovalTrait, GettingExchangeRatesAndAddToDb;

    /**
     * @Route("/{page}", name="department_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_TEAM') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxTeamBundle:Department')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Department::DEPARTMENTS_PER_PAGE
        );

        return $this->render('@PinoxTeam/Department/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="department_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $department = new Department();
        $form = $this->createForm(DepartmentType::class, $department);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($department);
            $em->flush();

            return $this->redirect($this->generateUrl('department_index'));
        }

        return $this->render('@PinoxTeam/Department/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="department_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("department", class="PinoxTeamBundle:Department")
     * @param Request $request
     * @param Department $department
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Department $department)
    {
        $form = $this->createForm(DepartmentType::class, $department, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($department);
            $em->flush();

            return $this->redirect($this->generateUrl('department_index'));
        }

        return $this->render(
            '@PinoxTeam/Department/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="department_delete")
     * @Method("DELETE")
     * @ParamConverter("department", class="PinoxTeamBundle:Department")
     * @param Request $request
     * @param Department $department
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Department $department)
    {
        $this->processEntityRemoval(
            $department,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('department_index'));
    }

    /**
     * @Route("/get_department_info", name="department_get_info")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \LogicException
     */
    public function getDepartmentInfoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
        $departmentId = $request->get('department_id');
        $sum = 0;

        $departmentInfo = $em->getRepository('PinoxTeamBundle:Employee')
            ->getEmployeesDepartment($departmentId);

        $totalSum = $em->getRepository('PinoxTeamBundle:Employee')->getTotalSumByDepartment($departmentId);

        $usdCurrency = $em->getRepository('PinoxBookkeepingBundle:Currency')->findOneBy(['code' => Currency::CODE_USD]);

        $rates['rate'] = $this->gettingExchangeRatesAndAddToDb($usdCurrency, $em);
        $rates['currencyName'] = $usdCurrency->getName();
        $rates['currencyCode'] = $usdCurrency->getCode();

        $total = $converter->converterCurrency($totalSum, $rates);
        foreach ($total as $item) {
            $sum += $item['payment'];
        }

        $department['departmentInfo'] = $departmentInfo;
        $department['sum'] = $sum;

        return new JsonResponse($department);
    }
}