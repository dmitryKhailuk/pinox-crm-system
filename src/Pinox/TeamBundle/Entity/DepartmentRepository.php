<?php

namespace Pinox\TeamBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DepartmentRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingDepartmentsByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('d')
            ->where('lower(d.name) = lower(:name)')
            ->setParameter('name', $criteria['name'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('dep')
            ->orderBy('dep.name', 'ASC');
    }

    /**
     * @return Department
     */
    public function getDepartmentWithoutParent()
    {
        return $this->createQueryBuilder('dep')
            ->where('dep.parent IS NULL')
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @return array
     */
    public function getDepartmentsForTree()
    {
        return $this->createQueryBuilder('dep')
            ->select('dep.id, dep.name, p.id AS parentId, e.name AS firstName, e.surname')
            ->leftJoin('dep.parent', 'p')
            ->leftJoin('dep.employee', 'e')
            ->where('dep.enabled = :enabled')
            ->setParameter('enabled', true)
            ->getQuery()->getArrayResult();
    }
}
