<?php
namespace Pinox\TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="DepartmentRepository")
 * @ORM\Table(
 *     name="departments",
 *     schema="pinox_team",
 *     indexes={
 *          @ORM\Index(name="departments_parent_id_idx", columns={"parent_id"})
 *      }
 * )
 * @UniqueEntity(
 *     "name",
 *     repositoryMethod="findInterferingDepartmentsByCriteria",
 *     message="Отдел с таким название уже существует."
 * )
 */
class Department
{
    use TimestampableEntity;

    const DEPARTMENTS_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * Навешен регистронезависимый индекс (unique): lower(name).
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     * @var string
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean", nullable=true, options={"default"=true})
     */
    private $enabled = true;

    /**
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, unique=false)
     * @var Department|null
     */
    private $parent;

    /**
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", nullable=true)
     * @ORM\OneToOne(targetEntity="Employee",
     *     inversedBy="department",
     *     cascade={"persist"})
     * @var Employee
     */
    private $employee;

    /**
     * @return string
     */
    public function __toString()
    {
        $parts = [ $this->getName() ];
        if ($this->getEmployee()) {
            $parts[] = $this->getEmployee()->__toString();
        }
        return implode(' - ', $parts);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set parent
     *
     * @param \Pinox\TeamBundle\Entity\Department $parent
     *
     * @return Department
     */
    public function setParent(Department $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Pinox\TeamBundle\Entity\Department
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Department
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set employee
     *
     * @param \Pinox\TeamBundle\Entity\Employee $employee
     *
     * @return Department
     */
    public function setEmployee(Employee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \Pinox\TeamBundle\Entity\Employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
