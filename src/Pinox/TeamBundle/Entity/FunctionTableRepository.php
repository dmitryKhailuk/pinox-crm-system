<?php

namespace Pinox\TeamBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FunctionTableRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingFunctionsByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('f')
            ->where('lower(f.name) = lower(:name)')
            ->andWhere('f.department = :department')
            ->setParameters([
                'name' => $criteria['name'],
                'department' => $criteria['department'],
            ])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('f')
            ->select(['f', 'd'])
            ->leftJoin('f.department', 'd')
            ->orderBy('f.name', 'ASC');
    }

    /**
     * @param integer $departmentId
     * @return array
     */
    public function findFunctionsByDepartment($departmentId)
    {
        return $this->createQueryBuilder('f')
            ->select('f.id, f.name')
            ->join('f.department', 'd')
            ->where('d.id = :id')
            ->setParameter('id', $departmentId)
            ->getQuery()->getArrayResult();
    }
}
