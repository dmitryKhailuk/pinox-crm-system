<?php
namespace Pinox\TeamBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="FunctionTableRepository")
 * @ORM\Table(
 *     name="functions",
 *     schema="pinox_team"
 * )
 * @UniqueEntity(
 *     fields={"name", "department"},
 *     groups={"Default"},
 *     message="function.function_error",
 *     errorPath="name",
 *     repositoryMethod="findInterferingFunctionsByCriteria"
 * )
 */
class FunctionTable
{
    use TimestampableEntity;

    const FUNCTIONS_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * Навешен регистронезависимый индекс (unique): lower(name).
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="30")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="enabled", type="boolean", nullable=true, options={"default"=true})
     * @var bool
     */
    private $enabled = true;

    /**
     * @ORM\ManyToOne(targetEntity="Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank()
     * @var Department
     */
    private $department;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FunctionTable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return FunctionTable
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set department
     *
     * @param \Pinox\TeamBundle\Entity\Department $department
     *
     * @return FunctionTable
     */
    public function setDepartment(Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \Pinox\TeamBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }
}
