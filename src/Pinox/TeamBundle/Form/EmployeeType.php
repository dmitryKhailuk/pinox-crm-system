<?php

namespace Pinox\TeamBundle\Form;

use Pinox\BookkeepingBundle\Entity\CurrencyRepository;
use Pinox\TeamBundle\Entity\Employee;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'employee.name',
            'required' => true,
            'attr' => [
                'maxlength' => 35
            ]
        ]);

        $builder->add('surname', TextType::class, [
            'label' => 'employee.surname',
            'required' => true,
            'attr' => [
                'maxlength' => 35
            ]
        ]);

        $builder->add('patronymic', TextType::class, [
            'label' => 'employee.patronymic',
            'required' => false,
            'attr' => [
                'maxlength' => 35
            ]
        ]);

        $builder->add('birthday', DateType::class, [
            'label' => 'employee.birthday',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('passport', TextType::class, [
            'label' => 'employee.passport',
            'required' => false,
            'attr' => [
                'maxlength' => 20
            ]
        ]);

        $builder->add('phone', TextType::class, [
            'label' => 'employee.phone',
            'required' => false,
            'attr' => [
                'maxlength' => 15
            ]
        ]);

        $builder->add('address', TextType::class, [
            'label' => 'employee.address',
            'required' => false,
        ]);

        $builder->add('department', EntityType::class, [
            'label'    => 'employee.department',
            'class' => 'PinoxTeamBundle:Department',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('functionTable', EntityType::class, [
            'label'    => 'employee.function_table',
            'class' => 'PinoxTeamBundle:FunctionTable',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('paymentMethod', EntityType::class, [
            'label'    => 'employee.payment_method',
            'class' => 'PinoxTeamBundle:PaymentMethod',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('currency', EntityType::class, [
            'label' => 'bill.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => true,
            'query_builder' => function (CurrencyRepository $repository) {
                return $repository->getListQueryBuilder();
            }
        ]);

        $builder->add('salary', IntegerType::class, [
            'label' => 'employee.salary',
            'required' => false,
        ]);

        $builder->add('cardNumber', TextType::class, [
            'label' => 'employee.card_number',
            'required' => false,
            'attr' => [
                'maxlength' => 20
            ]
        ]);

        $builder->add('dateEmployment', DateType::class, [
            'label' => 'employee.date_employment',
            'html5' => false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('dateDismissal', DateType::class, [
            'label' => 'employee.date_dismissal',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('comment', TextType::class, [
            'label' => 'employee.comment',
            'required' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_team_employees';
    }
}