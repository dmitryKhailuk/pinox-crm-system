<?php

namespace Pinox\TeamBundle\Form;


use Pinox\TeamBundle\Entity\FunctionTable;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FunctionTableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'function.name',
            'required' => true,
        ]);

        $builder->add('department', EntityType::class, [
            'label'    => 'function.department',
            'class' => 'PinoxTeamBundle:Department',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('enabled', CheckboxType::class, [
            'label' => 'function.enabled',
            'required' => false,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FunctionTable::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_team_functions';
    }
}