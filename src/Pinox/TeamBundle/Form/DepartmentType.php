<?php
namespace Pinox\TeamBundle\Form;

use Doctrine\ORM\EntityManager;
use Pinox\TeamBundle\Entity\Department;
use Pinox\TeamBundle\Entity\EmployeeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class DepartmentType extends AbstractType
{
    /** @var EntityManager  */
    private $em;

    /** @var Translator  */
    private $translator;

    /** @var Department $department */
    private $department;
    /**
     * DepartmentType constructor.
     * @param EntityManager $em
     * @param Translator $translator
     */
    public function __construct(EntityManager $em, Translator $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'department.name',
            'required' => true,
        ]);

        $builder->add('employee', EntityType::class, [
            'label'    => 'department.employee_main',
            'class' => 'PinoxTeamBundle:Employee',
            'required' => false,
            'query_builder' => function (EmployeeRepository $repository) {
                return $repository->getListQueryBuilder();
            }
        ]);

        $builder->add('parent', EntityType::class, [
            'label'    => 'department.parent',
            'class' => 'PinoxTeamBundle:Department',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('enabled', CheckboxType::class, [
            'label' => 'department.enabled',
            'required' => false,
        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event){
            /** @var Department $department */
            $this->department = $event->getData();
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event){
            $data = $event->getData();
            $form = $event->getForm();

            if (!$data['parent']) {
                $departmentWithoutParent = $this->em->getRepository('PinoxTeamBundle:Department')
                    ->getDepartmentWithoutParent();
                if (is_object($departmentWithoutParent) &&
                    $this->department->getId() !== $departmentWithoutParent->getId() ) {

                    $form->addError(new FormError(
                            $this->translator->trans('department.parent_error'))
                    );
                }
            }
            $event->setData($data);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Department::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_team_department';
    }

}