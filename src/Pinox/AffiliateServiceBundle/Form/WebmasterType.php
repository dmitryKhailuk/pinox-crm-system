<?php

namespace Pinox\AffiliateServiceBundle\Form;

use Pinox\AffiliateServiceBundle\Entity\Webmaster;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WebmasterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('countries', EntityType::class, [
            'label' => 'webmaster.country',
            'class' => 'PinoxDashboardBundle:Country',
            'required' => false,
            'choice_label' => 'name',
            'multiple' => true,
        ]);

        $builder->add('trafficSource', EntityType::class, [
            'label'    => 'webmaster.traffic_source',
            'class' => 'PinoxAffiliateServiceBundle:TrafficSource',
            'required' => false,
        ]);

        $builder->add('verticalOffer', EntityType::class, [
            'label'    => 'webmaster.vertical_offer',
            'class' => 'PinoxAffiliateServiceBundle:VerticalOffer',
            'required' => false,
            'choice_label' => 'name',
        ]);

        $builder->add('status', EntityType::class, [
            'label'    => 'webmaster.status',
            'class' => 'PinoxAffiliateServiceBundle:Status',
            'required' => false,
            'choice_label' => 'name',
        ]);

        $builder->add('reminder', TextType::class, [
            'label' => 'webmaster.reminder',
            'required' => false,
        ]);

        $builder->add('dateReminder', DateType::class, [
            'label' => 'webmaster.date_reminder',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('comment', TextType::class, [
            'label' => 'webmaster.comment',
            'required' => false,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Webmaster::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_affiliate_service_webmaster';
    }
}
    {

    }