<?php

namespace Pinox\AffiliateServiceBundle\Controller;

use Pinox\AffiliateServiceBundle\Entity\Webmaster;
use Pinox\AffiliateServiceBundle\Form\WebmasterType;
use Pinox\AffiseBundle\Entity\Affiliate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/affiliate_service/webmasters")
 * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE')")
 */
class WebmasterController extends Controller
{
    /**
     * @Route("/{affiliate}/new", name="webmaster_new")
     * @Method({"GET", "POST"})
     * @ParamConverter("affiliate", class="PinoxAffiseBundle:Affiliate")
     * @param Request $request
     * @param Affiliate $affiliate
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request, Affiliate $affiliate)
    {
        $webmaster = new Webmaster();
        $form = $this->createForm(WebmasterType::class, $webmaster);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $webmaster->setAffiliate($affiliate);
            $em->persist($webmaster);
            $em->flush();

            return $this->redirect($this->generateUrl('affiliate_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:Webmaster:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
            'affiliate' => $affiliate
        ]);
    }

    /**
     * @Route("/edit/{id}", name="webmaster_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("webmaster", class="PinoxAffiliateServiceBundle:Webmaster")
     * @param Request $request
     * @param Webmaster $webmaster
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Webmaster $webmaster)
    {
        $form = $this->createForm(WebmasterType::class, $webmaster, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($webmaster);
            $em->flush();

            return $this->redirect($this->generateUrl('affiliate_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:Webmaster:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'affiliate' => $webmaster->getAffiliate(),
        ]);
    }

    /**
     * @Route("/reminder/{id}", name="webmaster_reminder")
     * @Method({"GET", "PUT"})
     * @ParamConverter("webmaster", class="PinoxAffiliateServiceBundle:Webmaster")
     * @param Webmaster $webmaster
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function reminderAction(Webmaster $webmaster)
    {
        $em = $this->getDoctrine()->getManager();
        if ($webmaster->getRecalled() === false) {
            $webmaster->setRecalled(true);
        } else {
            $webmaster->setRecalled(false);
        }

        $em->persist($webmaster);
        $em->flush();
        return $this->redirect($this->generateUrl('affiliate_reminder'));
    }

    /**
     * @Route("/archive/{page}", name="webmaster_archive", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiliateServiceBundle:Webmaster')
            ->getRecalledWebmasters();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Webmaster::WEBMASTER_PER_PAGE
        );

        return $this->render('PinoxAffiliateServiceBundle:Webmaster:archiveReminder.html.twig', compact('pagination'));
    }

}
