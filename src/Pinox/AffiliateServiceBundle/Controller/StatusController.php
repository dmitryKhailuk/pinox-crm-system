<?php

namespace Pinox\AffiliateServiceBundle\Controller;

use Pinox\AffiliateServiceBundle\Entity\Status;
use Pinox\AffiliateServiceBundle\Form\StatusType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/affiliate_service/statuses")
 * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE')")
 */
class StatusController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="status_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiliateServiceBundle:Status')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Status::STATUS_PER_PAGE
        );

        return $this->render('PinoxAffiliateServiceBundle:Status:index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="status_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $status = new Status();
        $form = $this->createForm(StatusType::class, $status);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($status);
            $em->flush();

            return $this->redirect($this->generateUrl('status_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:Status:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="status_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("tatus", class="PinoxAffiliateServiceBundle:Status")
     * @param Request $request
     * @param Status $status
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Status $status)
    {
        $form = $this->createForm(StatusType::class, $status, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($status);
            $em->flush();

            return $this->redirect($this->generateUrl('status_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:Status:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="status_delete")
     * @Method("DELETE")
     * @ParamConverter("tatus", class="PinoxAffiliateServiceBundle:Status")
     * @param Request $request
     * @param Status $status
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Status $status)
    {
        $this->processEntityRemoval(
            $status,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('status_index'));
    }

}
