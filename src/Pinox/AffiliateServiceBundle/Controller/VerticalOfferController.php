<?php

namespace Pinox\AffiliateServiceBundle\Controller;

use Pinox\AffiliateServiceBundle\Entity\VerticalOffer;
use Pinox\AffiliateServiceBundle\Form\VerticalOfferType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/affiliate_service/vertical_offer")
 * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE') or has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class VerticalOfferController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="vertical_offer_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiliateServiceBundle:VerticalOffer')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            VerticalOffer::VERTICAL_OFFER_PER_PAGE
        );

        return $this->render('PinoxAffiliateServiceBundle:VerticalOffer:index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="vertical_offer_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $verticalOffer = new VerticalOffer();
        $form = $this->createForm(VerticalOfferType::class, $verticalOffer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($verticalOffer);
            $em->flush();

            return $this->redirect($this->generateUrl('vertical_offer_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:VerticalOffer:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="vertical_offer_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("verticalOffer", class="PinoxAffiliateServiceBundle:VerticalOffer")
     * @param Request $request
     * @param VerticalOffer $verticalOffer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, VerticalOffer $verticalOffer)
    {
        $form = $this->createForm(VerticalOfferType::class, $verticalOffer, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($verticalOffer);
            $em->flush();

            return $this->redirect($this->generateUrl('vertical_offer_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:VerticalOffer:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="vertical_offer_delete")
     * @Method("DELETE")
     * @ParamConverter("verticalOffer", class="PinoxAffiliateServiceBundle:VerticalOffer")
     * @param Request $request
     * @param VerticalOffer $verticalOffer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, VerticalOffer $verticalOffer)
    {
        $this->processEntityRemoval(
            $verticalOffer,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('vertical_offer_index'));
    }

}
