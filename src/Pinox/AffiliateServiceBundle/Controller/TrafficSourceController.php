<?php

namespace Pinox\AffiliateServiceBundle\Controller;

use Pinox\AffiliateServiceBundle\Entity\TrafficSource;
use Pinox\AffiliateServiceBundle\Form\TrafficSourceType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/affiliate_service/traffic_sources")
 * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE') or has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class TrafficSourceController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="traffic_source_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiliateServiceBundle:TrafficSource')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            TrafficSource::TRAFFIC_SOURCE_PER_PAGE
        );

        return $this->render('PinoxAffiliateServiceBundle:TrafficSource:index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="traffic_source_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $trafficSource = new TrafficSource();
        $form = $this->createForm(TrafficSourceType::class, $trafficSource);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trafficSource);
            $em->flush();

            return $this->redirect($this->generateUrl('traffic_source_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:TrafficSource:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="traffic_source_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("trafficSource", class="PinoxAffiliateServiceBundle:TrafficSource")
     * @param Request $request
     * @param TrafficSource $trafficSource
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, TrafficSource $trafficSource)
    {
        $form = $this->createForm(TrafficSourceType::class, $trafficSource, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($trafficSource);
            $em->flush();

            return $this->redirect($this->generateUrl('traffic_source_index'));
        }

        return $this->render('PinoxAffiliateServiceBundle:TrafficSource:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="traffic_source_delete")
     * @Method("DELETE")
     * @ParamConverter("trafficSource", class="PinoxAffiliateServiceBundle:TrafficSource")
     * @param Request $request
     * @param TrafficSource $trafficSource
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, TrafficSource $trafficSource)
    {
        $this->processEntityRemoval(
            $trafficSource,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('traffic_source_index'));
    }

}
