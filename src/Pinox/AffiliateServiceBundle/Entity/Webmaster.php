<?php
namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\AffiseBundle\Entity\Affiliate;
use Pinox\DashboardBundle\Entity\Country;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="WebmasterRepository")
 * @ORM\Table(
 *     name="webmasters",
 *     schema="affiliate_service"
 * )
 */
class Webmaster
{
    const WEBMASTER_PER_PAGE = 50;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TrafficSource")
     * @ORM\JoinColumn(name="traffic_source_id", referencedColumnName="id", nullable=true)
     * @var TrafficSource
     */
    private $trafficSource;

    /**
     * @ORM\ManyToOne(targetEntity="VerticalOffer")
     * @ORM\JoinColumn(name="vertical_offer_id", referencedColumnName="id", nullable=true)
     * @var VerticalOffer
     */
    private $verticalOffer;

    /**
     * @ORM\ManyToOne(targetEntity="Status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true)
     * @var Status
     */
    private $status;

    /**
     * @ORM\Column(name="reminder", type="text", nullable=true)
     * @var string
     */
    private $reminder;

    /**
     * @ORM\Column(name="date_reminder", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $dateReminder;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity="Pinox\AffiseBundle\Entity\Affiliate", inversedBy="webmaster")
     * @ORM\JoinColumn(name="affiliate_id", referencedColumnName="id", nullable=false)
     * @var Affiliate
     */
    private $affiliate;

    /**
     * @ORM\Column(name="recalled", type="boolean", nullable=true, options={"default"=false})
     * @var bool
     */
    private $recalled = false;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\DashboardBundle\Entity\Country",
     *      inversedBy="webmasters",
     *      cascade={"persist"}
     * )
     * @ORM\JoinTable(
     *      name="affiliate_service.webmasters_countries",
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *      }
     * )
     * @Assert\Valid()
     */
    private $countries;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reminder
     *
     * @param string $reminder
     *
     * @return Webmaster
     */
    public function setReminder($reminder)
    {
        $this->reminder = $reminder;

        return $this;
    }

    /**
     * Get reminder
     *
     * @return string
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * Set dateReminder
     *
     * @param \DateTime $dateReminder
     *
     * @return Webmaster
     */
    public function setDateReminder($dateReminder)
    {
        $this->dateReminder = $dateReminder;

        return $this;
    }

    /**
     * Get dateReminder
     *
     * @return \DateTime
     */
    public function getDateReminder()
    {
        return $this->dateReminder;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Webmaster
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set trafficSource
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\TrafficSource $trafficSource
     *
     * @return Webmaster
     */
    public function setTrafficSource(TrafficSource $trafficSource = null)
    {
        $this->trafficSource = $trafficSource;

        return $this;
    }

    /**
     * Get trafficSource
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\TrafficSource
     */
    public function getTrafficSource()
    {
        return $this->trafficSource;
    }

    /**
     * Set verticalOffer
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\VerticalOffer $verticalOffer
     *
     * @return Webmaster
     */
    public function setVerticalOffer(VerticalOffer $verticalOffer = null)
    {
        $this->verticalOffer = $verticalOffer;

        return $this;
    }

    /**
     * Get verticalOffer
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\VerticalOffer
     */
    public function getVerticalOffer()
    {
        return $this->verticalOffer;
    }

    /**
     * Set status
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\Status $status
     *
     * @return Webmaster
     */
    public function setStatus(Status $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set affiliate
     *
     * @param \Pinox\AffiseBundle\Entity\Affiliate $affiliate
     *
     * @return Webmaster
     */
    public function setAffiliate(Affiliate $affiliate = null)
    {
        $this->affiliate = $affiliate;

        return $this;
    }

    /**
     * Get affiliate
     *
     * @return \Pinox\AffiseBundle\Entity\Affiliate
     */
    public function getAffiliate()
    {
        return $this->affiliate;
    }

    /**
     * Set recalled
     *
     * @param boolean $recalled
     *
     * @return Webmaster
     */
    public function setRecalled($recalled)
    {
        $this->recalled = $recalled;

        return $this;
    }

    /**
     * Get recalled
     *
     * @return boolean
     */
    public function getRecalled()
    {
        return $this->recalled;
    }

    /**
     * Add country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     *
     * @return Webmaster
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
