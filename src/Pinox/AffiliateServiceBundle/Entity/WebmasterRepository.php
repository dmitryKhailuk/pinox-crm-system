<?php

namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\EntityRepository;

class WebmasterRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getRecalledWebmasters()
    {
        return $this->createQueryBuilder('w')
            ->innerJoin('w.affiliate', 'a')
            ->where('w.recalled = :recalled')
            ->andWhere('w.dateReminder IS NOT NULL')
            ->setParameter('recalled', true)
            ->orderBy('w.dateReminder', 'DESC');
    }
}
