<?php
namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="TrafficSourceRepository")
 * @ORM\Table(
 *     name="traffic_sources",
 *     schema="affiliate_service",
 *     indexes={
 *          @ORM\Index(name="traffic_sources_traffic_source_unq_idx", columns={"traffic_source"})
 *      }
 * )
 * @UniqueEntity(
 *     "trafficSource",
 *     repositoryMethod="findInterferingTrafficSourcesByCriteria",
 *     message="traffic_source.error_index"
 * )
 */
class TrafficSource
{
    const TRAFFIC_SOURCE_PER_PAGE = 25;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;


    /**
     * @ORM\Column(name="traffic_source", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $trafficSource;

    public function __toString()
    {
        return $this->trafficSource;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trafficSource
     *
     * @param string $trafficSource
     *
     * @return TrafficSource
     */
    public function setTrafficSource($trafficSource)
    {
        $this->trafficSource = $trafficSource;

        return $this;
    }

    /**
     * Get trafficSource
     *
     * @return string
     */
    public function getTrafficSource()
    {
        return $this->trafficSource;
    }
}
