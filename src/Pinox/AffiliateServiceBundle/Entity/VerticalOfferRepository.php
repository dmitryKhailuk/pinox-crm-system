<?php

namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VerticalOfferRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingVerticalOfferByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('vo')
            ->where('lower(vo.name) = lower(:name)')
            ->setParameter('name', $criteria['name'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('vo')
            ->orderBy('vo.name', 'ASC');
    }
}
