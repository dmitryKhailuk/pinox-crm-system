<?php
namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="StatusRepository")
 * @ORM\Table(
 *     name="statuses",
 *     schema="affiliate_service",
 *     indexes={
 *          @ORM\Index(name="statuses_name_unq_idx", columns={"name"})
 *      }
 * )
 * @UniqueEntity(
 *     "name",
 *     repositoryMethod="findInterferingStatusesByCriteria",
 *     message="status.error_index"
 * )
 */
class Status
{
    const STATUS_PER_PAGE = 25;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;


    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"=true})
     * @var bool
     */
    private $active = true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Status
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

}
