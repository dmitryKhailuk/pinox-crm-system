<?php

namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\EntityRepository;

class StatusRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingStatusesByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('s')
            ->where('lower(s.name) = lower(:name)')
            ->setParameter('name', $criteria['name'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('s')
            ->orderBy('s.name', 'ASC');
    }
}
