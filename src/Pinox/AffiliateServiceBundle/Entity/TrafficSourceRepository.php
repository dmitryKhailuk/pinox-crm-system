<?php

namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TrafficSourceRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingTrafficSourcesByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('ts')
            ->where('lower(ts.trafficSource) = lower(:trafficSource)')
            ->setParameter('trafficSource', $criteria['trafficSource'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('ts')
            ->orderBy('ts.trafficSource', 'ASC');
    }
}
