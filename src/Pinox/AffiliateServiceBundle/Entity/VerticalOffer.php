<?php
namespace Pinox\AffiliateServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="VerticalOfferRepository")
 * @ORM\Table(
 *     name="vertical_offers",
 *     schema="affiliate_service",
 *     indexes={
 *          @ORM\Index(name="vertical_offers_name_unq_idx", columns={"name"})
 *      }
 * )
 * @UniqueEntity(
 *     "name",
 *     repositoryMethod="findInterferingVerticalOfferByCriteria",
 *     message="vertical_offer.error_index"
 * )
 */
class VerticalOffer
{
    const VERTICAL_OFFER_PER_PAGE = 25;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;


    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $name;
    
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return VerticalOffer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
