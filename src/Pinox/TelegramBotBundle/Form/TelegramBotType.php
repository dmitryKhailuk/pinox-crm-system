<?php

namespace Pinox\TelegramBotBundle\Form;

use Pinox\TelegramBotBundle\Entity\TelegramBot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TelegramBotType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('token', TextType::class, [
            'label' => 'telegram.token',
            'required' => true,
        ]);

        $builder->add('commands', TextareaType::class, [
            'label' => 'telegram.commands',
            'required' => false,
        ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            $data = $event->getData();
            $allCommands = [];
            if (array_key_exists('commands', $data)) {
                $commands = explode(';', $data['commands']);
                foreach ($commands as $command) {
                    $oneCommand = explode('-', $command);
                    if (count($oneCommand) > 1) {
                        $allCommands[trim($oneCommand[0])] = trim($oneCommand[1]);
                    }
                }
                $data['commands'] = $allCommands;
                $event->setData($data);
            }

        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TelegramBot::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_telegram_bot_telegram_bots';
    }

}