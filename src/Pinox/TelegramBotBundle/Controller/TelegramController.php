<?php

namespace Pinox\TelegramBotBundle\Controller;

use Pinox\TelegramBotBundle\Entity\TelegramBot;
use Pinox\TelegramBotBundle\Form\TelegramBotType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/telegram_bot")
 */
class TelegramController extends Controller
{
    /** @var TelegramBot */
    private $bot = null;

    /**
     * @Route("/", name="telegram_bot_index")
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_TOOL')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $telegramBots = $em->getRepository('PinoxTelegramBotBundle:TelegramBot')->findAll();

        return $this->render('@PinoxTelegramBot/Telegram/index.html.twig', [
            'telegramBots' => $telegramBots
        ]);
    }

    /**
     * @Route("/webhook_commands", name="telegram_bot_webhook_commands")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function webhookCommandsAction(Request $request)
    {
        $token = $request->query->get('token');
        $bot = $this->getBot();
        /** получаем поток от telegram из вебхука */
        $update = file_get_contents('php://input');
        $update = json_decode($update, true);

        $telegramBot = $this->get('pinox_telegram_bot_api');
        $telegramBot->setToken($bot->getToken());

        if ((strcasecmp($token, $bot->getToken()) === 0) && is_array($update) && array_key_exists('message', $update)) {
            $command = $this->get('pinox_telegram_bot_command');
            $command->setMessages($bot->getAllCommands());
            if (array_key_exists('chat', $update['message'])) {
                $chatId = $update['message']['chat']['id'];
                list($message, $buttons) = $command->getCommand($update['message']['text'], ['chat_id' => $chatId]);
                $telegramBot->sendMessage($chatId, $message, $buttons);
            }
        }

        return new Response('Ok');
    }

    /**
     * @Route("/command", name="telegram_bot_command")
     * @Security("has_role('ROLE_SHOW_MODULE_TOOL')")
     */
    public function commandAction()
    {
        $bot = $this->getBot();
        $telegramBot = $this->get('pinox_telegram_bot_api');
        $telegramBot->setToken($bot->getToken());

        $updates = $telegramBot->getUpdates();
        $command = $this->get('pinox_telegram_bot_command');
        $command->setMessages($bot->getAllCommands());

        if (array_key_exists('ok', $updates) && array_key_exists('result', $updates) && $updates['ok']) {
            $updates = array_pop($updates['result']);

            if (array_key_exists('message', $updates)) {
                $chatId = $updates['message']['chat']['id'];
                list($message, $buttons) = $command->getCommand($updates['message']['text'], ['chat_id' => $chatId]);

                $telegramBot->sendMessage($chatId, $message, $buttons);
            }

        }
        return new Response('Ok');
    }

    /**
     * @Route("/edit/{id}", name="telegram_bot_edit")
     * @Method({"GET", "PUT"})
     * @Security("has_role('ROLE_SHOW_MODULE_TOOL')")
     * @ParamConverter("telegramBot", class="PinoxTelegramBotBundle:TelegramBot")
     * @param Request $request
     * @param TelegramBot $telegramBot
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, TelegramBot $telegramBot)
    {
        $form = $this->createForm(TelegramBotType::class, $telegramBot, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($telegramBot);
            $em->flush();

            return $this->redirect($this->generateUrl('telegram_bot_index'));
        }

        return $this->render(
            '@PinoxTelegramBot/Telegram/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @return object|TelegramBot
     */
    private function getBot()
    {
        if (!$this->bot) {
            $em = $this->getDoctrine()->getManager();
            $this->bot = $em->getRepository('PinoxTelegramBotBundle:TelegramBot')->findOneBy(['id' => 1]);
        }

        return $this->bot;
    }
}
