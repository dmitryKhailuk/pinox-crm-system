<?php

namespace Pinox\TelegramBotBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TelegramBotRepository")
 * @ORM\Table(
 *     name="telegram_bot",
 *     schema="pinox_tool"
 * )
 */
class TelegramBot
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name_bot", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="20")
     * @var string
     */
    private $nameBot;

    /**
     * @ORM\Column(name="token", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @var string
     */
    private $token;

    /**
     * @ORM\Column(name="commands", type="json_array", nullable=true)
     * @var array
     */
    private $commands;

    /**
     * Get id
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameBot
     * @param string $nameBot
     * @return TelegramBot
     */
    public function setNameBot($nameBot)
    {
        $this->nameBot = $nameBot;

        return $this;
    }

    /**
     * Get nameBot
     * @return string
     */
    public function getNameBot()
    {
        return $this->nameBot;
    }

    /**
     * Set token
     * @param string $token
     * @return TelegramBot
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set commands
     * @param array $commands
     * @return TelegramBot
     */
    public function setCommands(array $commands)
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * Get commands
     * @return string
     */
    public function getCommands()
    {
        $commandsInLine = '';
        foreach ($this->commands as $key => $command) {
            $commandsInLine .= $key . ' - ' .$command .'; ';
        }

        return $commandsInLine;
    }

    /**
     * Get commands
     * @return array
     */
    public function getAllCommands()
    {
        return $this->commands;
    }
}
