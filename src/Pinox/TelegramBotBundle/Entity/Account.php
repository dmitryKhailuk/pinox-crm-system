<?php

namespace Pinox\TelegramBotBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AccountRepository")
 * @ORM\Table(
 *     name="accounts",
 *     schema="pinox_tool"
 * )
 */
class Account
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="chat_id", type="string", length=128, nullable=false)
     * @var string
     */
    private $chatId;

    /**
     * @ORM\Column(name="api_key", type="string", length=128, nullable=false)
     * @var string
     */
    private $apiKey;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chatId
     *
     * @param string $chatId
     *
     * @return Account
     */
    public function setChatId($chatId)
    {
        $this->chatId = $chatId;

        return $this;
    }

    /**
     * Get chatId
     *
     * @return string
     */
    public function getChatId()
    {
        return $this->chatId;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return Account
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
}
