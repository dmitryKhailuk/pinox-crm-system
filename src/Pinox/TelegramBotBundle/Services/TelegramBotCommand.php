<?php

/**
 * commands for bot in telegram
 */
namespace Pinox\TelegramBotBundle\Services;

use Doctrine\ORM\EntityManager;
use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;
use Pinox\TelegramBotBundle\Entity\Account;
use Symfony\Component\Translation\TranslatorInterface;

class TelegramBotCommand
{
    use CurlRequestTrait;

    const COMMAND_HELP = 'Help';
    const COMMAND_GET_BY_DATE = 'Stat By Date';
    const COMMAND_GET_BY_HOUR = 'Stat By Hour';
    const COMMAND_GET_CONTACTS = 'Contacts';
    const COMMAND_GET_BALANCE = 'Balance';
    const COMMAND_GET_TOP_OFFERS = 'TOP Offers';
    const COMMAND_GET_STAT_PANEL = 'Get stat';
    const COMMAND_SET_CONNECT_ACCOUNT = 'Connect account';
    const COMMAND_GET_BY_OFFERS = '/getbyoffers';
    const COMMAND_BACK = 'Back';
    const COMMAND_START = '/start';

    /**  @var TelegramBotCommand */
    public static $instance = null;

    /** @var  TranslatorInterface $translator */
    private $translator;

    /** @var  EntityManager $em */
    private $em;

    /** @var array */
    private $messages = [];

    /** @var array */
    private $headers = [
        'API-Key: 5ba4a0f34550b6147c0e376ffc560e6fa1e3bfbe',
        'Accept: application/json',
    ];

    /** @var array */
    private $apiKeys = [];

    /** @var array */
    private $buttonsMain = [
        self::COMMAND_SET_CONNECT_ACCOUNT,
        self::COMMAND_GET_STAT_PANEL,
        self::COMMAND_GET_TOP_OFFERS,
        self::COMMAND_HELP,
        self::COMMAND_GET_CONTACTS,

    ];
    /** @var array */
    private $buttonsStat = [
        self::COMMAND_GET_BY_DATE,
        self::COMMAND_GET_BY_HOUR,
        self::COMMAND_GET_BALANCE,
        self::COMMAND_BACK,
    ];

    /** @var array */
    private $buttonsConnect = [
        self::COMMAND_BACK,
    ];

    /**
     * TelegramBotCommand constructor.
     * @param EntityManager $em
     * @param TranslatorInterface $translator
     */
    private function __construct(EntityManager $em, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->em = $em;
    }

    private function __clone()
    {
    }

    /**
     * @param EntityManager $em
     * @param TranslatorInterface $translator
     * @return TelegramBotCommand
     */
    public static function getInstance(EntityManager $em, TranslatorInterface $translator)
    {
        if (self::$instance === null) {
            self::$instance = new TelegramBotCommand($em, $translator);
        }

        return self::$instance;
    }

    /**
     * @param array $messages
     */
    public function setMessages(array $messages)
    {
      $this->messages = $messages;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /** @return array */
    public function getButtonsMain()
    {
        return $this->buttonsMain;
    }

    /** @return array */
    public function getButtonsStat()
    {
        return $this->buttonsStat;
    }

        /** @return array */
    public function getButtonsConnect()
    {
        return $this->buttonsConnect;
    }

    /**
     * @param string $command
     * @param array $parameters
     * @return string
     */
    public function getCommand($command = null, array $parameters = [])
    {

        $buttons = $this->getButtonsMain();
        if ($command) {
            $apiKey = $this->getApiKeyAffise($parameters);
            $this->setHeaders([
                'API-Key: '. $apiKey,
                'Accept: application/json',
            ]);

            $message = array_key_exists($command, $this->messages) ? $this->messages[$command] . " \n" : '';
            switch (trim($command)) {
                case self::COMMAND_HELP:
                    break;
                case self::COMMAND_GET_STAT_PANEL:
                    $buttons = $this->getButtonsStat();
                    break;
                case self::COMMAND_GET_BY_DATE:
                    $message .= $this->getByDateStatAffise();
                    $buttons = $this->getButtonsStat();
                    break;
                case self::COMMAND_GET_BY_HOUR:
                    $message .= $this->getByHourStatAffise();
                    $buttons = $this->getButtonsStat();
                    break;
                case self::COMMAND_GET_BY_OFFERS:
                    $message .= $this->getByOffersStatAffise();
                    $buttons = $this->getButtonsStat();
                    break;
                case self::COMMAND_GET_CONTACTS:
                    $message .= $this->getContactMessage();
                    break;
                case self::COMMAND_GET_BALANCE:
                    $message .= $this->getBalanceAffise();
                    $buttons = $this->getButtonsStat();
                    break;
                case self::COMMAND_SET_CONNECT_ACCOUNT:
                    $buttons = $this->getButtonsConnect();
                    break;
                case self::COMMAND_GET_TOP_OFFERS:
                    $message .= $this->getTopOffersAffise();
                    break;
                case self::COMMAND_BACK:
                    break;
                case self::COMMAND_START:
                    $message .= $this->translator->trans('telegram.start');
                    break;
                default:
                    $command = trim($command);
                    if (strlen($command) === 40) {
                        $message .= $this->connectAccount($command, $parameters);
                        break;
                    }
                    $message = $this->translator->trans('telegram.wrong_command');
                    break;
            }

        } else {
            $message = $this->translator->trans('telegram.wrong_command');
        }

        return [$message, $buttons];
    }

    /**
     * @return string
     */
    private function getByDateStatAffise()
    {

        $stats = json_decode($this->curlGet('http://api.pinox.com/2.1/stats/getbydate', [
            'date_from' => date('Y-m-d', strtotime('-3 days'))],
            $this->getHeaders()), true);

        $statsMessage = $this->generateStringStatistics($stats);

        return $statsMessage;
    }

    /**
     * @return string
     */
    private function getByHourStatAffise()
    {

        $stats = json_decode($this->curlGet('http://api.pinox.com/2.1/stats/getbyhour', [
            'date_from' => date('Y-m-d', strtotime('-3 days'))],
            $this->getHeaders()), true);

        $statsMessage = $this->generateStringStatistics($stats);

        return $statsMessage;
    }

    /**
     * @return string
     */
    private function getByOffersStatAffise()
    {

        $stats = json_decode($this->curlGet('http://api.pinox.com/2.1/stats/getbydate', [],
            $this->getHeaders()), true);

        $statsMessage = $this->generateStringStatistics($stats);

        return $statsMessage;
    }

    /**
     * @return string
     */
    public function getBalanceAffise()
    {
        $balanceMessage = '';
        $balance = json_decode($this->curlGet('http://api.pinox.com/2.1/balance', [],
            $this->getHeaders()), true);

        if (array_key_exists('balance', $balance)) {
            foreach ($balance['balance']['balance'] as $key => $balance) {
                $balanceMessage .= "<strong>$key:</strong> " . $balance . " \n";
            }
            $balanceMessage .= "\n";
        } else {
            $balanceMessage = $this->translator->trans('telegram.not_data');
        }

        return $balanceMessage;
    }

    /**
     * @param string $command
     * @param array $parameters
     * @return string
     */
    private function connectAccount($command, array $parameters)
    {

        if ($command && array_key_exists('chat_id', $parameters)) {
            $account = $this->em->getRepository('PinoxTelegramBotBundle:Account')
                ->findOneBy(['chatId' => $parameters['chat_id']]);
            if (!$account) {
                $account = new Account();
                $account->setChatId($parameters['chat_id']);
                $message = $this->translator->trans('telegram.account_success_add');
            } else {
                $message = $this->translator->trans('telegram.account_already_use');
            }

            $account->setApiKey($command);
            $this->em->persist($account);
            $this->em->flush();
        } else {
            $message = $this->translator->trans('telegram.account_not_add');
        }

        return $message;
    }

    /**
     * @return string
     */
    private function getTopOffersAffise()
    {
        $message = '';
        $topOffers = json_decode($this->curlGet('http://api.pinox.com/2.1/offers', [
            'sort' => ['epc'=> 'desc'], 'limit' => 10 ],
            $this->getHeaders()), true);
        if (array_key_exists('offers', $topOffers)) {
            foreach ($topOffers['offers'] as $offer) {
                $message .= '<a href="' . $offer['preview_url'] . '">' . $offer['title'] . "</a> \n";
                $message .= '<i>Offer id:</i> ' . $offer['offer_id'] . " \n";
                $message .= "\n";
            }
        } else {
            $message = $this->translator->trans('telegram.not_data');
        }

        return $message;
    }

    private function getContactMessage()
    {
        $message = 'Affiliate Network: <a href="https://pinox.com">https://pinox.com' . "</a> \n \n" .
            'Support Telegram: @needdja' . " \n" . 'Skype: nikita.pinox' . " \n \n" .
            'E-mail: <a href="mailto:go@pinox.com">go@pinox.com' . "</a> \n" .
            'Facebook: <a href="https://www.facebook.com/pinoxcom/">https://www.facebook.com/pinoxcom/' . "</a>";

        return $message;
    }

    /**
     * @param array $stats
     * @return string
     */
    private function generateStringStatistics(array $stats)
    {
        $statistic = '';

        if (count($stats) > 0 && !array_key_exists('status', $stats)) {
            foreach ($stats as $key => $stat) {
                if (strcasecmp($key, 'total') !== 0 && isset($stat['actions'])) {
                    $statistic .= '<strong>' . date('d.m.Y', strtotime($key)) . ": </strong> \n";
                    if (array_key_exists('count', $stat['actions']['total'])) {
                        $statistic .= '<i>Count:</i> ' . $stat['actions']['total']['count'] . " \n";
                    }
                    if (array_key_exists('revenue', $stat['actions']['total'])) {
                        $statistic .= '<i>Revenue:</i> ' . $stat['actions']['total']['revenue'] . " \n";
                    }
                    if (array_key_exists('charge', $stat['actions']['total'])) {
                        $statistic .= '<i>Charge:</i> ' . $stat['actions']['total']['charge'] . " \n";
                    }
                    if (array_key_exists('earning', $stat['actions']['total'])) {
                        $statistic .= '<i>Earning:</i> ' . $stat['actions']['total']['earning'] . " \n";
                    }

                    $statistic .= "\n";
                }
            }
        } else {
            $statistic = $this->translator->trans('telegram.not_data');
        }

        return $statistic;
    }

    /**
     * @param array $parameters
     * @return string
     */
    private function getApiKeyAffise(array $parameters = [])
    {
        $apiKey = '';
        if (isset($parameters['chat_id']) ) {
            if (isset($this->apiKeys[$parameters['chat_id']])) {
                $apiKey = $this->apiKeys[$parameters['chat_id']]->getApiKey();
            } else {
                $account = $this->em->getRepository('PinoxTelegramBotBundle:Account')
                    ->findOneBy(['chatId' => $parameters['chat_id']]);
                if ($account) {
                    $this->apiKeys[$account->getChatId()] = $account;
                    $apiKey = $account->getApiKey();
                }

            }
        }

        return $apiKey;
    }
}
