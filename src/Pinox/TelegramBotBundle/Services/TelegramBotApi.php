<?php

/**
 * check documentation for telegram bot api
 * https://tlgrm.ru/docs/bots/api
 */
namespace Pinox\TelegramBotBundle\Services;

use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;

class TelegramBotApi
{
    use CurlRequestTrait;

    const STYLE_FOR_TEXT_MESSAGE = 'HTML';
    const URL = 'https://api.telegram.org/bot';

    /**  @var TelegramBotApi */
    public static $instance = null;

    /** @var string */
    private $token = '337759481:AAF43v6s37Mm1kg_BFEZMF2Q0q5vzknONME';


    /** TelegramBotApi constructor. */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @return TelegramBotApi
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new TelegramBotApi();
        }

        return self::$instance;
    }

    /**
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Get Token
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * send message about telegram bot
     * @return mixed
     */
    public function getMe()
    {
        return $this->curlGet(self::URL . $this->getToken() . '/getMe');
    }

    /**
     * check all send message in telegram bot. Removed after 24 hours
     * @return mixed
     */
    public function getUpdates()
    {
        return json_decode($this->curlGet(self::URL . $this->getToken() . '/getUpdates'), true);
    }

    /**
     * set webhook for instant messages
     * @param string $url
     * @return mixed
     */
    public function setWebhook($url)
    {
        return $this->curlPost(self::URL . $this->getToken() . '/setWebhook', [
            'url' => $url .'?token='. $this->getToken()], [ 'Accept: application/json' ]);
    }

    /**
     * information about set webhook
     * @return mixed
     */
    public function getWebhookInfo()
    {
        return $this->curlGet(self::URL . $this->getToken() . '/getWebhookInfo');
    }

    /**
     * send message in chat from bot
     * @param integer $chatId
     * @param string $text
     * @param array $buttons
     * @return mixed
     */
    public function sendMessage($chatId, $text, array $buttons)
    {
        $keyboard = [
            'keyboard' => array_chunk($buttons, 2),
            'resize_keyboard' => true,
            'one_time_keyboard' => false
        ];

        $keyboard = json_encode($keyboard);
        return $this->curlPost(self::URL . $this->getToken() . '/sendMessage', [
            'chat_id' => $chatId,
            'text' => $text,
            'parse_mode' =>self::STYLE_FOR_TEXT_MESSAGE,
            'reply_markup' => $keyboard
        ]);
    }

}