<?php

namespace Pinox\AdvertiserServiceBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Pinox\AffiseBundle\Entity\Advertiser;

class PaymentAdvertiserRepository extends EntityRepository
{
    /**
     * @param Advertiser $advertiser
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder(Advertiser $advertiser)
    {
        return $this->createQueryBuilder('pa')
            ->leftJoin('pa.advertiser', 'a')
            ->where('a.id = :advertiser')
            ->setParameter('advertiser', $advertiser->getId())
            ->orderBy('pa.datePayment', 'DESC');

    }

    /**
     * @param Advertiser $advertiser
     * @return array
     */
    public function getTotalSumPaymentsAdvertiserFromCurrency(Advertiser $advertiser)
    {
        return $this->createQueryBuilder('pa')
            ->select('SUM(pa.payment) as payment, c.name as currency, c.code')
            ->innerJoin('pa.currency', 'c')
            ->where('pa.advertiser = :advertiser')
            ->setParameter('advertiser', $advertiser)
            ->groupBy('c.code', 'c.name')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();
    }

    /**
     * @param Advertiser $advertiser
     * @return float
     */
    public function getTotalSumPaymentsAdvertiser(Advertiser $advertiser)
    {
        return $this->createQueryBuilder('pa')
            ->select('SUM(pa.convertedPayment) as total')
            ->where('pa.advertiser = :advertiser')
            ->setParameter('advertiser', $advertiser)
            ->getQuery()->getOneOrNullResult();
    }
}
