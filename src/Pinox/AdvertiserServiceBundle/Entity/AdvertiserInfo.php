<?php
namespace Pinox\AdvertiserServiceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\AffiliateServiceBundle\Entity\TrafficSource;
use Pinox\AffiliateServiceBundle\Entity\VerticalOffer;
use Pinox\AffiseBundle\Entity\Advertiser;
use Pinox\DashboardBundle\Entity\Country;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AdvertiserInfoRepository")
 * @ORM\Table(
 *     name="advertisers_info",
 *     schema="advertiser_service"
 * )
 */
class AdvertiserInfo
{
    const ADVERTISERS_INFO_PER_PAGE = 50;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pinox\AffiliateServiceBundle\Entity\TrafficSource")
     * @ORM\JoinColumn(name="traffic_source_id", referencedColumnName="id", nullable=true)
     * @var TrafficSource
     */
    private $trafficSource;

    /**
     * @ORM\ManyToOne(targetEntity="Pinox\AffiliateServiceBundle\Entity\VerticalOffer")
     * @ORM\JoinColumn(name="vertical_offer_id", referencedColumnName="id", nullable=true)
     * @var VerticalOffer
     */
    private $verticalOffer;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @ORM\OneToOne(targetEntity="Pinox\AffiseBundle\Entity\Advertiser", inversedBy="advertiserInfo")
     * @ORM\JoinColumn(name="advertiser_id", referencedColumnName="id", nullable=false)
     * @var Advertiser
     */
    private $advertiser;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\DashboardBundle\Entity\Country",
     *      inversedBy="advertisersInfo",
     *      cascade={"persist"}
     * )
     * @ORM\JoinTable(
     *      name="advertiser_service.advertisers_info_countries",
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *      }
     * )
     * @Assert\Valid()
     */
    private $countries;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"=true})
     * @var bool
     */
    private $active = true;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return AdvertiserInfo
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set trafficSource
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\TrafficSource $trafficSource
     *
     * @return AdvertiserInfo
     */
    public function setTrafficSource(TrafficSource $trafficSource = null)
    {
        $this->trafficSource = $trafficSource;

        return $this;
    }

    /**
     * Get trafficSource
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\TrafficSource
     */
    public function getTrafficSource()
    {
        return $this->trafficSource;
    }

    /**
     * Set verticalOffer
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\VerticalOffer $verticalOffer
     *
     * @return AdvertiserInfo
     */
    public function setVerticalOffer(VerticalOffer $verticalOffer = null)
    {
        $this->verticalOffer = $verticalOffer;

        return $this;
    }

    /**
     * Get verticalOffer
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\VerticalOffer
     */
    public function getVerticalOffer()
    {
        return $this->verticalOffer;
    }

    /**
     * Set advertiser
     *
     * @param \Pinox\AffiseBundle\Entity\Advertiser $advertiser
     *
     * @return AdvertiserInfo
     */
    public function setAdvertiser(Advertiser $advertiser)
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * Get advertiser
     *
     * @return \Pinox\AffiseBundle\Entity\Advertiser
     */
    public function getAdvertiser()
    {
        return $this->advertiser;
    }

    /**
     * Add country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     *
     * @return AdvertiserInfo
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return AdvertiserInfo
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
