<?php
namespace Pinox\AdvertiserServiceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\AffiliateServiceBundle\Entity\TrafficSource;
use Pinox\AffiliateServiceBundle\Entity\VerticalOffer;
use Pinox\AffiseBundle\Entity\Advertiser;
use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Entity\Country;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="PaymentAdvertiserRepository")
 * @ORM\Table(
 *     name="payments_advertiser",
 *     schema="advertiser_service"
 * )
 */
class PaymentAdvertiser
{
    const PAYMENT_PER_PAGE = 25;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="payment", type="decimal", precision=15, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payment = 0;

    /**
     * @ORM\Column(name="comment", type="text", nullable=true)
     * @var string
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Pinox\AffiseBundle\Entity\Advertiser", inversedBy="paymentAdvertisers")
     * @ORM\JoinColumn(name="advertiser_id", referencedColumnName="id", nullable=false)
     * @var Advertiser
     */
    private $advertiser;


    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Pinox\BookkeepingBundle\Entity\Currency",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;

    /**
     * @ORM\Column(name="date_payment", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $datePayment;

    /**
     * @ORM\Column(name="converted_payment", type="decimal", precision=12, scale=2, nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $convertedPayment = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return PaymentAdvertiser
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return PaymentAdvertiser
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set advertiser
     *
     * @param \Pinox\AffiseBundle\Entity\Advertiser $advertiser
     *
     * @return PaymentAdvertiser
     */
    public function setAdvertiser(Advertiser $advertiser = null)
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    /**
     * Get advertiser
     *
     * @return \Pinox\AffiseBundle\Entity\Advertiser
     */
    public function getAdvertiser()
    {
        return $this->advertiser;
    }

    /**
     * Set currency
     *
     * @param \Pinox\BookkeepingBundle\Entity\Currency $currency
     *
     * @return PaymentAdvertiser
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Pinox\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set datePayment
     *
     * @param \DateTime $datePayment
     *
     * @return PaymentAdvertiser
     */
    public function setDatePayment($datePayment)
    {
        $this->datePayment = $datePayment;

        return $this;
    }

    /**
     * Get datePayment
     *
     * @return \DateTime
     */
    public function getDatePayment()
    {
        return $this->datePayment;
    }

    /**
     * Set convertedPayment
     *
     * @param string $convertedPayment
     *
     * @return PaymentAdvertiser
     */
    public function setConvertedPayment($convertedPayment)
    {
        $this->convertedPayment = $convertedPayment;

        return $this;
    }

    /**
     * Get convertedPayment
     *
     * @return string
     */
    public function getConvertedPayment()
    {
        return $this->convertedPayment;
    }
}
