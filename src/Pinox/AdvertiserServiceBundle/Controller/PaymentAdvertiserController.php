<?php

namespace Pinox\AdvertiserServiceBundle\Controller;

use Doctrine\ORM\EntityManager;
use Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser;
use Pinox\AdvertiserServiceBundle\Form\PaymentAdvertiserType;
use Pinox\AffiseBundle\Entity\Advertiser;
use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/advertiser_service/payments")
 * @Security("has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class PaymentAdvertiserController extends Controller
{
    use ProcessesEntityRemovalTrait, GettingExchangeRatesAndAddToDb;
    /**
     * @Route("/advertiser/{id}/{page}",
     *     name="advertiser_payment_index",
     *     defaults={"page": 1},
     *     requirements={"page": "\d+"}
     * )
     * @Method("GET")
     * @ParamConverter("advertiser", class="PinoxAffiseBundle:Advertiser")
     * @param int $page
     * @param Advertiser $advertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Advertiser $advertiser, $page)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('PinoxAdvertiserServiceBundle:PaymentAdvertiser')
            ->getListQueryBuilder($advertiser);

        $totalSum = $em->getRepository('PinoxAdvertiserServiceBundle:PaymentAdvertiser')
            ->getTotalSumPaymentsAdvertiserFromCurrency($advertiser);

        $total = $em->getRepository('PinoxAdvertiserServiceBundle:PaymentAdvertiser')
            ->getTotalSumPaymentsAdvertiser($advertiser);

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            PaymentAdvertiser::PAYMENT_PER_PAGE
        );

        return $this->render('PinoxAdvertiserServiceBundle:PaymentAdvertiser:index.html.twig', [
            'pagination' => $pagination,
            'advertiser' => $advertiser,
            'totalSum' => $totalSum,
            'total' => $total,
        ]);
    }

    /**
     * @Route("/advertiser/{id}/new", name="payment_advertiser_new")
     * @Method({"GET", "POST"})
     * @ParamConverter("advertiser", class="PinoxAffiseBundle:Advertiser")
     * @param Request $request
     * @param Advertiser $advertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request, Advertiser $advertiser)
    {
        $paymentAdvertiser = new PaymentAdvertiser();
        $form = $this->createForm(PaymentAdvertiserType::class, $paymentAdvertiser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($paymentAdvertiser->getPayment() > 0 ) {
                $paymentAdvertiser->setConvertedPayment($this->convertedPayment($paymentAdvertiser, $em));
            }

            $paymentAdvertiser->setAdvertiser($advertiser);
            $em->persist($paymentAdvertiser);
            $em->flush();

            return $this->redirect($this->generateUrl('advertiser_payment_index', [
                    'id' => $advertiser->getId()
                ])
            );
        }

        return $this->render('PinoxAdvertiserServiceBundle:PaymentAdvertiser:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
            'advertiser' => $advertiser
        ]);
    }

    /**
     * @Route("/edit/{id}", name="payment_advertiser_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("paymentAdvertiser", class="PinoxAdvertiserServiceBundle:PaymentAdvertiser")
     * @param Request $request
     * @param PaymentAdvertiser $paymentAdvertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, PaymentAdvertiser $paymentAdvertiser)
    {
        $form = $this->createForm(PaymentAdvertiserType::class, $paymentAdvertiser, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($paymentAdvertiser->getPayment() > 0 ) {
                $paymentAdvertiser->setConvertedPayment($this->convertedPayment($paymentAdvertiser, $em));
            }
            $em->persist($paymentAdvertiser);
            $em->flush();

            return $this->redirect($this->generateUrl('advertiser_payment_index', [
                    'id' => $paymentAdvertiser->getAdvertiser()->getId()
                ])
            );
        }

        return $this->render('PinoxAdvertiserServiceBundle:PaymentAdvertiser:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'advertiser' => $paymentAdvertiser->getAdvertiser(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="payment_advertiser_delete")
     * @Method("DELETE")
     * @ParamConverter("paymentAdvertiser", class="PinoxAdvertiserServiceBundle:PaymentAdvertiser")
     * @param Request $request
     * @param PaymentAdvertiser $paymentAdvertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, PaymentAdvertiser $paymentAdvertiser)
    {
        $advertiser = $paymentAdvertiser->getAdvertiser();

        $this->processEntityRemoval(
            $paymentAdvertiser,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('advertiser_payment_index', [
                'id' => $advertiser->getId()
            ])
        );
    }

    /**
     * @param PaymentAdvertiser $paymentAdvertiser
     * @param EntityManager $em
     * @return float
     */
    private function convertedPayment(PaymentAdvertiser $paymentAdvertiser, EntityManager $em)
    {
        $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
        $usdCurrency = $em->getRepository('PinoxBookkeepingBundle:Currency')
            ->findOneBy(['code' => Currency::CODE_USD]);

        $rates['rate'] = $this->gettingExchangeRatesAndAddToDb($usdCurrency, $em);
        $rates['currencyName'] = $usdCurrency->getName();
        $rates['currencyCode'] = $usdCurrency->getCode();
        $convertedPayment = $converter->converterCurrency(
            [[
                'code' => $paymentAdvertiser->getCurrency()->getCode(),
                'payment' => $paymentAdvertiser->getPayment()
            ]],
            $rates
        );

        return $convertedPayment[0]['payment'];
    }
}
