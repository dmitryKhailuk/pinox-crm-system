<?php

namespace Pinox\AdvertiserServiceBundle\Controller;

use Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo;
use Pinox\AdvertiserServiceBundle\Form\AdvertiserInfoType;
use Pinox\AffiseBundle\Entity\Advertiser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/advertiser_service/advertisers")
 * @Security("has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class AdvertiserInfoController extends Controller
{
    /**
     * @Route("/{advertiser}/new", name="advertiser_info_new")
     * @Method({"GET", "POST"})
     * @ParamConverter("advertiser", class="PinoxAffiseBundle:Advertiser")
     * @param Request $request
     * @param Advertiser $advertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request, Advertiser $advertiser)
    {
        $advertiserInfo = new AdvertiserInfo();
        $form = $this->createForm(AdvertiserInfoType::class, $advertiserInfo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $advertiserInfo->setAdvertiser($advertiser);
            $em->persist($advertiserInfo);
            $em->flush();

            return $this->redirect($this->generateUrl('advertiser_index'));
        }

        return $this->render('PinoxAdvertiserServiceBundle:AdvertiserInfo:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
            'advertiser' => $advertiser
        ]);
    }

    /**
     * @Route("/edit/{id}", name="advertiser_info_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("advertiserInfo", class="PinoxAdvertiserServiceBundle:AdvertiserInfo")
     * @param Request $request
     * @param AdvertiserInfo $advertiserInfo
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, AdvertiserInfo $advertiserInfo)
    {
        $form = $this->createForm(AdvertiserInfoType::class, $advertiserInfo, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($advertiserInfo);
            $em->flush();

            return $this->redirect($this->generateUrl('advertiser_index'));
        }

        return $this->render('PinoxAdvertiserServiceBundle:AdvertiserInfo:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'advertiser' => $advertiserInfo->getAdvertiser(),
        ]);
    }

}
