<?php

namespace Pinox\AdvertiserServiceBundle\Form;

use Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser;
use Pinox\BookkeepingBundle\Entity\CurrencyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentAdvertiserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('datePayment', DateType::class, [
            'label' => 'payment_advertiser.date_payment',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('currency', EntityType::class, [
            'label' => 'revenue.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
            'query_builder' => function (CurrencyRepository $repository) {
                return $repository->getListQueryBuilder();
            }
        ]);

        $builder->add('payment', TextType::class, [
            'label' => 'payment_advertiser.payment',
            'required' => true,
        ]);

        $builder->add('comment', TextareaType::class, [
            'label' => 'payment_advertiser.comment',
            'required' => false,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PaymentAdvertiser::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_advertiser_service_payment_advertiser';
    }
}
    {

    }