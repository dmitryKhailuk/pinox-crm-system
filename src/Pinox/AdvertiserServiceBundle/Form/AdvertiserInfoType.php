<?php

namespace Pinox\AdvertiserServiceBundle\Form;

use Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertiserInfoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('countries', EntityType::class, [
            'label' => 'advertiser_info.country',
            'class' => 'PinoxDashboardBundle:Country',
            'required' => false,
            'choice_label' => 'name',
            'multiple' => true,
        ]);

        $builder->add('trafficSource', EntityType::class, [
            'label'    => 'advertiser_info.traffic_source',
            'class' => 'PinoxAffiliateServiceBundle:TrafficSource',
            'required' => false,
        ]);

        $builder->add('verticalOffer', EntityType::class, [
            'label'    => 'advertiser_info.vertical_offer',
            'class' => 'PinoxAffiliateServiceBundle:VerticalOffer',
            'required' => false,
            'choice_label' => 'name',
        ]);

        $builder->add('active', CheckboxType::class, [
            'label' => 'status.active',
            'required' => false,
        ]);

        $builder->add('comment', TextareaType::class, [
            'label' => 'advertiser_info.comment',
            'required' => false,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AdvertiserInfo::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_advertiser_service_advertiser_info';
    }
}
    {

    }