<?php
namespace Pinox\AffiseBundle\Services;

use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;

class AffiseApi
{
    use CurlRequestTrait;

    const URL_AFFISE = 'https://api.pinox.com/2.1';
    const LIMIT = 100;
    const PRIVACY = 'private';

    /** @var AffiseApi  */
    static $instance = null;

    private $headers = [
        'API-Key: 970eebd695c6d6a8dfe33628ebb7e40e29f32e3e', 'Accept: application/json'
    ];

    /** AffiseApi constructor. */
    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @return null|AffiseApi
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new AffiseApi();
        }

        return self::$instance;
    }

    /**
     * @param integer $limit
     * @param integer $page
     * @return array
     */
    public function getPartnersByLimit($limit, $page = null)
    {

        $url = AffiseApi::URL_AFFISE . '/admin/partners';

        $affiliatesAffise = $this->curlGet(
            $url,
            ['limit' => $limit, 'page' => $page],
            $this->headers
        );

        return json_decode($affiliatesAffise, true);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function getOffers(array $parameters = [])
    {
        $url = AffiseApi::URL_AFFISE . '/offers';
        $offers = $this->curlGet(
            $url,
            $parameters,
            $this->headers
        );

        return json_decode($offers, true);
    }

    /**
     * @param int $limit
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @return mixed
     */
    public function getTopOffersByLimit($limit, \DateTime $dateFrom, \DateTime $dateTo)
    {
        $url = AffiseApi::URL_AFFISE . '/stats/gettopoffers';
        $offers = [];

        $offersStat = $this->curlGet($url, [
                'limit' => $limit,
                'date_from' => $dateFrom->format('Y-m-d 00:00:00'),
                'date_to' => $dateTo->format('Y-m-d 00:00:00'),
            ],
            $this->headers
        );
        $offersStat = json_decode($offersStat, true);

        foreach ($offersStat as $offerStat) {
            $offerInfo = $this->getOffers(['q' => $offerStat['id']]);
            if (array_key_exists('offers', $offerInfo) && isset($offerInfo['offers'][0])
                && strcasecmp($offerInfo['offers'][0]['privacy'], self::PRIVACY) !== 0 ) {
                $offers[] = $offerInfo['offers'][0];
            }
        }
        return $offers;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $url = AffiseApi::URL_AFFISE . '/offer/categories';
        $offers = $this->curlGet(
            $url,
            ['limit' => self::LIMIT],
            $this->headers
        );

        return json_decode($offers, true);
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getOfferById($id)
    {
        $url = AffiseApi::URL_AFFISE . '/offer/'.$id;
        $offer = $this->curlGet(
            $url,
            [],
            $this->headers
        );

        return json_decode($offer, true);
    }

    /**
     * @param $limit
     * @param integer $page
     * @return mixed
     */
    public function getAdvertisersByLimit($limit, $page = null)
    {
        $url = AffiseApi::URL_AFFISE . '/admin/advertisers';

        $advertisersAffise = $this->curlGet(
            $url,
            ['limit' => $limit, 'page' => $page],
            $this->headers
        );

        return json_decode($advertisersAffise, true);
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function getStatsByAdvertiser(array $parameters = []) //$currency, \DateTime $dateFrom = null, $supplier = null)
    {
        $url = AffiseApi::URL_AFFISE . '/stats/getbysupplier';

        $advertisersStat = $this->curlGet(
            $url,
            $parameters,
            $this->headers
        );

        return json_decode($advertisersStat, true);
    }


}