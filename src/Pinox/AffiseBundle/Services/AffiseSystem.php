<?php

namespace Pinox\AffiseBundle\Services;

use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;

class AffiseSystem
{

    use CurlRequestTrait;

    const LIMIT  = 100;

    /** @var AffiseSystem  */
    static $instance = null;

    /** @var AffiseApi  */
    private $affiseApi = null;

    /**
     * AffiseSystem constructor.
     * @param AffiseApi $affiseApi
     */
    private function __construct(AffiseApi $affiseApi)
    {
        $this->affiseApi = $affiseApi;
    }

    private function __clone()
    {
    }

    /**
     * @param AffiseApi $affiseApi
     * @return AffiseSystem
     */
    public static function getInstance(AffiseApi $affiseApi)
    {
        if (self::$instance === null) {
            self::$instance = new AffiseSystem($affiseApi);
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    public function getAllAffiliates()
    {
        $affiliates = [];
        $affiliatesAffise = $this->affiseApi->getPartnersByLimit(AffiseSystem::LIMIT);

        if (array_key_exists('status', $affiliatesAffise) && $affiliatesAffise['status'] == 1) {
            $affiliates = $this->getAffiliates($affiliatesAffise);
            $amountPage = $this->getAmountPage($affiliatesAffise, AffiseSystem::LIMIT);
            if ($amountPage > 0) {
                for ($i = 2; $i <= $amountPage; $i++) {
                    $affiliatesAffise = $this->affiseApi->getPartnersByLimit(AffiseSystem::LIMIT, $i);
                    $affiliates = array_merge($this->getAffiliates($affiliatesAffise), $affiliates);
                }
            }

        }

        return $affiliates;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getLastAffiliates($limit)
    {
        $affiliates = [];
        $affiliatesAffise = $this->affiseApi->getPartnersByLimit($limit);
        $amountPage = $this->getAmountPage($affiliatesAffise, $limit);
        if ($amountPage > 0) {
            $affiliates = $this->getAffiliates($this->affiseApi->getPartnersByLimit($limit, $amountPage));
        }

        return $affiliates;
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getNewOffers($limit)
    {
        $offers = $this->affiseApi->getOffers(['limit' => $limit, 'sort[id]' => 'desc']);
        return array_key_exists('offers', $offers) ? $offers['offers'] : [];
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getTopOffers($limit)
    {
        return $this->affiseApi->getTopOffersByLimit($limit, new \DateTime('now -7 days'), new \DateTime('now'));
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        $categories = $this->affiseApi->getCategories();
        return array_key_exists('categories',  $categories) ? $categories['categories'] : [];
    }

    /**
     * @param integer $id
     * @return array
     */
    public function getOfferById($id)
    {
        $offer = $this->affiseApi->getOfferById($id);
        return array_key_exists('offer',  $offer) ? $offer['offer'] : [];
    }

    /**
     * @return array
     */
    public function getAllAdvertisers()
    {
        $advertisers = [];
        $advertisersAffise = $this->affiseApi->getAdvertisersByLimit(AffiseSystem::LIMIT);

        if (array_key_exists('status', $advertisersAffise) && $advertisersAffise['status'] == 1) {
            $advertisers = $this->getAdvertisers($advertisersAffise);
            $amountPage = $this->getAmountPage($advertisersAffise, AffiseSystem::LIMIT);
            if ($amountPage > 0) {
                for ($i = 2; $i <= $amountPage; $i++) {
                    $advertisersAffise = $this->affiseApi->getAdvertisersByLimit(AffiseSystem::LIMIT, $i);
                    $advertisers = array_merge($this->getAdvertisers($advertisersAffise), $advertisers);
                }
            }

        }

        return $advertisers;
    }

    /**
     * @param string $currency
     * @param \DateTime $dateFrom
     * @return array
     */
    public function getBalanceAdvertisers($currency, \DateTime $dateFrom = null)
    {
        $advertisers = [];
        if (null === $dateFrom) {
            $dateFrom = new \DateTime('-7 days');
        }
        $advertisersBalance = $this->affiseApi->getStatsByAdvertiser([
            'currency' => $currency,
            'date_from' => $dateFrom->format('Y-m-d')
        ]);

        foreach ($advertisersBalance as $key => $advertiserBalance) {
            if (array_key_exists('actions', $advertiserBalance) && array_key_exists('_id', $advertiserBalance)) {
                $advertisers[$advertiserBalance['_id']] = [
                    'actions' => $advertiserBalance['actions'],
                    'currency' => $currency,
                    'title' => $key
                ];
            }
        }

        return $advertisers;
    }

    /**
     * @param array $affiliatesAffise
     * @return array
     */
    private function getAffiliates(array $affiliatesAffise = [])
    {
        $affiliates = [];
        if (array_key_exists('status', $affiliatesAffise) && $affiliatesAffise['status'] == 1 &&
            array_key_exists('partners', $affiliatesAffise)) {

            foreach ($affiliatesAffise['partners'] as $partner) {
                $affiliates[$partner['id']] = $partner;
            }
        }

        return $affiliates;
    }

    /**
     * @param array $advertisersAffise
     * @return array
     */
    private function getAdvertisers(array $advertisersAffise = [])
    {
        $advertisers = [];
        if (array_key_exists('status', $advertisersAffise) && $advertisersAffise['status'] == 1 &&
            array_key_exists('advertisers', $advertisersAffise)) {

            foreach ($advertisersAffise['advertisers'] as $advertiser) {
                $advertisers[$advertiser['id']] = $advertiser;
            }
        }

        return $advertisers;
    }

    /**
     * @param array $affiliatesAffise
     * @param int $limit
     * @return float|int
     */
    private function getAmountPage(array $affiliatesAffise, $limit)
    {
        $amountPage = 0;
        if (array_key_exists('pagination', $affiliatesAffise)
            && array_key_exists('total_count', $affiliatesAffise['pagination'])) {
            $amountPage = ceil($affiliatesAffise['pagination']['total_count'] / $limit);
        }
        return $amountPage;
    }

}
