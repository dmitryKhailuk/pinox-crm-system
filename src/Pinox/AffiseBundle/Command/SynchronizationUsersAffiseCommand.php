<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Command\Traits\SynchronizationUsersAffaiseTrait;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizationUsersAffiseCommand extends ContainerAwareCommand
{
    use SynchronizationUsersAffaiseTrait;

    const SYNCHRONIZATION_USERS_LIMIT = 50;

    protected function configure()
    {
        $this
            ->setName('affise:synchronization:users')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $affiseSystem = $this->getContainer()->get('pinox_affise_system');

        $affiliatePartners = $affiseSystem->getLastAffiliates(self::SYNCHRONIZATION_USERS_LIMIT);
        $affiliates = $em->getRepository('PinoxAffiseBundle:Affiliate')->getLastFiftyAffiliates();
        echo $this->synchronizationUsersAffaise($affiliates, $affiliatePartners, $em);

    }

}