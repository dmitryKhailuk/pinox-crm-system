<?php

namespace Pinox\AffiseBundle\Command\Traits;

use Pinox\AffiseBundle\Entity\Offer;

trait SetParametersOfferTrait
{

    /**
     * @param Offer $offer
     * @param array $parameters
     * @param $categories
     * @param $countries
     * @return Offer
     */
    public function setOfferParameters(Offer $offer, array $parameters = [], $categories, $countries)
    {
        $offer->setActive(strcasecmp($parameters['status'], Offer::STATUS_ACTIVE) != 0 ? false : true);
        $offer->setAffiseId($parameters['id']);
        $offer->setOfferAffiseId($parameters['offer_id']);
        $offer->setTitle($parameters['title']);
        $offer->setDescription($parameters['description']);
        $offer->setPreviewUrl($parameters['preview_url']);
        $offer->setPrivacy($parameters['privacy']);
        $offer->setIsTop($parameters['is_top']);
        $offer->setEpc($parameters['epc']);
        $offer->setCr($parameters['cr']);
        $offer->setLogo($parameters['logo']);

        if (array_key_exists('categories',$parameters) && count($parameters['categories']) > 0
            && isset($categories[$parameters['categories'][0]])) {
            $offer->setCategory($categories[$parameters['categories'][0]]);
        }

        if (array_key_exists('payments',$parameters) && array_key_exists(0, $parameters['payments'])) {
            $offer->setCurrency($parameters['payments'][0]['currency']);
            $offer->setPayout($parameters['payments'][0]['revenue']);
        }

        if (array_key_exists('countries', $parameters)) {
            foreach ($parameters['countries'] as $country) {
                $upperCode = strtoupper($country);
                if (array_key_exists($upperCode,  $countries)) {
                    $offer->addCountry($countries[$upperCode]);
                }
            }
        }

        return $offer;
    }
}