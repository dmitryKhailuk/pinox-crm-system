<?php

namespace Pinox\AffiseBundle\Command\Traits;


use Doctrine\ORM\EntityManager;
use Pinox\DashboardBundle\Entity\Country;

trait MappingCountriesByCodeTrait
{
    /**
     * @param EntityManager $em
     * @return Country[]
     */
    public function mappingCountriesByCode(EntityManager $em)
    {
        $mappingCountriesByCode = [];
        $countries = $em->getRepository('PinoxDashboardBundle:Country')->findAll();

        foreach ($countries as $country) {
            $mappingCountriesByCode[$country->getCode()] = $country;
        }

        return $mappingCountriesByCode;
    }
}