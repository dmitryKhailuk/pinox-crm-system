<?php

namespace Pinox\AffiseBundle\Command\Traits;

use Doctrine\ORM\EntityManager;
use Pinox\AffiseBundle\Entity\Category;

trait MappingCategoriesByIdTrait
{
    /**
     * @param EntityManager $em
     * @return Category[]
     */
    public function mappingCategoriesById(EntityManager $em)
    {
        $mappingCategoriesById = [];
        $categories = $em->getRepository('PinoxAffiseBundle:Category')->findAll();

        foreach ($categories as $category) {
            $mappingCategoriesById[$category->getAffiseId()] = $category;
        }

        return $mappingCategoriesById;
    }
}