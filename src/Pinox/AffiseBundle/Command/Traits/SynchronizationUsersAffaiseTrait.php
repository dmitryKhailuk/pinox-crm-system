<?php

namespace Pinox\AffiseBundle\Command\Traits;

use Doctrine\ORM\EntityManager;
use Pinox\AffiseBundle\Entity\Affiliate;

trait SynchronizationUsersAffaiseTrait
{

    /**
     * @param array $affiliates
     * @param array $affiliatePartners
     * @param EntityManager $em
     * @return string
     */
    private function synchronizationUsersAffaise(array $affiliates, array $affiliatePartners, EntityManager $em)
    {
        $allAffiliates = [];

        foreach ($affiliates as $affiliate) {
            $allAffiliates[$affiliate->getAffiseId()] = $affiliate;
        }

        foreach ($affiliatePartners as $affiliatePartner) {

            if (array_key_exists($affiliatePartner['id'], $allAffiliates)) {
                if ($affiliatePartner['status'] !== Affiliate::STATUS_ACTIVE ) {
                    $allAffiliates[$affiliatePartner['id']]->setActive(false);
                } else {
                    $allAffiliates[$affiliatePartner['id']]->setActive(true);
                }

                if (strcmp($affiliatePartner['api_key'], $allAffiliates[$affiliatePartner['id']]->getApiKey()) !== 0) {
                    $allAffiliates[$affiliatePartner['id']]->setApiKey($affiliatePartner['api_key']);
                }
                $allAffiliates[$affiliatePartner['id']]->setBalance($affiliatePartner['balance']);
                $em->persist($allAffiliates[$affiliatePartner['id']]);

            } else {
                $affiliate = new Affiliate();
                $affiliate->setActive($affiliatePartner['status'] !== Affiliate::STATUS_ACTIVE ? false : true);
                $affiliate->setAffiseId($affiliatePartner['id']);
                $affiliate->setEmail($affiliatePartner['email']);
                $affiliate->setLogin($affiliatePartner['login']
                    ? $affiliatePartner['login'] : $affiliatePartner['email']);
                $affiliate->setApiKey($affiliatePartner['api_key']);
                $affiliate->setBalance($affiliatePartner['balance']);
                $em->persist($affiliate);
            }
        }
        $em->flush();
        return 'ok';
    }
}