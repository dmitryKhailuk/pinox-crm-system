<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Entity\Advertiser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizationAdvertisersAffiseCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('affise:receiving:advertisers')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $affiseSystem = $this->getContainer()->get('pinox_affise_system');
        $advertisersInDB = [];
        $affiliateAdvertisers = $affiseSystem->getAllAdvertisers();
        $advertisers = $em->getRepository('PinoxAffiseBundle:Advertiser')->findAll();

        foreach ($advertisers as $advertiser) {
            $advertisersInDB[$advertiser->getAffiseId()] = $advertiser;
        }

        foreach ($affiliateAdvertisers as $affiliateAdvertiser) {
            if (!array_key_exists($affiliateAdvertiser['id'], $advertisersInDB)) {
                $advertiser = new Advertiser();
                $advertiser->setAffiseId($affiliateAdvertiser['id']);
                $advertiser->setContact($affiliateAdvertiser['contact']);
                $advertiser->setEmail($affiliateAdvertiser['email']);
                $advertiser->setTitle($affiliateAdvertiser['title']);
                $advertiser->setUrl($affiliateAdvertiser['url']);
                $advertiser->setManager($affiliateAdvertiser['manager']);
                $advertiser->setSkype($affiliateAdvertiser['skype']);
                $advertiser->setNote($affiliateAdvertiser['note']);
                $advertiser->setOffers($affiliateAdvertiser['offers']);
                $em->persist($advertiser);
            }
        }

        $em->flush();
        echo 'ok';
    }
}