<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Command\Traits\MappingCategoriesByIdTrait;
use Pinox\AffiseBundle\Command\Traits\MappingCountriesByCodeTrait;
use Pinox\AffiseBundle\Command\Traits\SetParametersOfferTrait;
use Pinox\AffiseBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewOffersAffiseCommand extends ContainerAwareCommand
{
    const LIMIT_OFFERS = 100;

    use MappingCountriesByCodeTrait, SetParametersOfferTrait, MappingCategoriesByIdTrait;

    protected function configure()
    {
        $this
            ->setName('affise:new:offers')
            ->addArgument('limit', InputArgument::REQUIRED, 'Limit for request')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $limit = $input->getArgument('limit');
        $em = $this->getContainer()->get('doctrine')->getManager();

        $mappingCountries = $this->mappingCountriesByCode($em);
        $mappingCategories = $this->mappingCategoriesById($em);

        $offers = $em->getRepository('PinoxAffiseBundle:Offer')->getLastInserted(self::LIMIT_OFFERS + $limit);

        $affiseSystem = $this->getContainer()->get('pinox_affise_system');
        $offersAffise = $affiseSystem->getNewOffers($limit);

        foreach ($offers as $offer) {
            foreach ($offersAffise as $key => $offerAffise) {
                if (strcasecmp($offer['offerAffiseId'], $offerAffise['offer_id']) == 0 ) {
                    unset($offersAffise[$key]);
                }
            }
        }

        foreach ($offersAffise as $offerAffise) {
            if (strcasecmp($offerAffise['privacy'], Offer::PRIVATE_PRIVACY) !== 0 ) {
                $offer = new Offer();
                $offer = $this->setOfferParameters($offer, $offerAffise, $mappingCategories, $mappingCountries);
                $em->persist($offer);
            }
        }

        $em->flush();

        echo 'ok';
    }

}
