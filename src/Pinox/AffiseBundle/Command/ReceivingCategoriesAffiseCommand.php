<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Command\Traits\SynchronizationUsersAffaiseTrait;
use Pinox\AffiseBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReceivingCategoriesAffiseCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('affise:receiving:categories')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $affiseSystem = $this->getContainer()->get('pinox_affise_system');

        $affiseCategories = $affiseSystem->getCategories();
        $categories = $em->getRepository('PinoxAffiseBundle:Category')->findAll();

        if (count($affiseCategories) > 0) {
            foreach ($affiseCategories as $key => $affiseCategory) {
                foreach ($categories as $category) {
                    if (strcasecmp($category->getAffiseId(), $affiseCategory['id']) === 0) {
                        unset($affiseCategories[$key]);
                    }
                }
            }
        }

        foreach ($affiseCategories as $affiseCategory) {
            $category = new Category();
            $category->setAffiseId($affiseCategory['id']);
            $category->setName($affiseCategory['title']);
            $em->persist($category);
        }

        $em->flush();

        echo 'ok';
    }

}