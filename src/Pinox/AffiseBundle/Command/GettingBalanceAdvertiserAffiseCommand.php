<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Entity\Advertiser;
use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GettingBalanceAdvertiserAffiseCommand extends ContainerAwareCommand
{
    use GettingExchangeRatesAndAddToDb;

    protected function configure()
    {
        $this
            ->setName('affise:balance:advertisers')
            ->addArgument('currencies', InputArgument::REQUIRED, 'Limit for request')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $balances = [];
        $advertisersBalance = [];
        $advertisersMappingById = [];
        $dateFrom = new \DateTime('2016-09-01');
        $container = $this->getContainer();

        $currencies = explode(',', $input->getArgument('currencies'));
        $em = $container->get('doctrine')->getManager();
        $affiseSystem = $container->get('pinox_affise_system');
        $converter = $container->get('pinox_bookkeeping_converter_currency_yahoo');

        $advertisers = $em->getRepository('PinoxAffiseBundle:Advertiser')->findAll();
        $usdCurrency = $em->getRepository('PinoxBookkeepingBundle:Currency')->findOneBy(['code' => Currency::CODE_USD]);

        $rates['rate'] = $this->gettingExchangeRatesAndAddToDb($usdCurrency, $em);
        $rates['currencyName'] = $usdCurrency->getName();
        $rates['currencyCode'] = $usdCurrency->getCode();

        foreach ($currencies as $currency) {
            $balances[$currency] = $affiseSystem->getBalanceAdvertisers($currency, $dateFrom);
        }

        /** @var  $advertiser Advertiser */
        foreach ($advertisers as $advertiser) {
            $advertisersMappingById[$advertiser->getAffiseId()] = $advertiser;
        }

        foreach ($balances as $balance) {
            foreach ($balance as $key => $item) {

                $keyInt = crc32($key);
                if (!isset($advertisersBalance[$keyInt])) {
                    $advertisersBalance[$keyInt] = [
                        'id' => $key,
                        'balance' => [
                            $item['currency'] => $item['actions']['confirmed']['charge'],
                        ],
                    ];
                } else {
                    $advertisersBalance[$keyInt]['balance'][$item['currency']] = $item['actions']['confirmed']['charge'];
                }

            }
        }

        foreach ($advertisersBalance as $advertiserBalance) {
            if(array_key_exists($advertiserBalance['id'], $advertisersMappingById)) {
                $convertedBalance = 0 ;

                $advertisersMappingById[$advertiserBalance['id']]->setConfirmedCharge($advertiserBalance['balance']);
                foreach ($advertiserBalance['balance'] as $currency => $balance) {
                    if ($balance > 0) {
                         $temp = $converter->converterCurrency(
                            [['code' => $currency , 'payment' => $balance]],
                            $rates
                        );
                        $convertedBalance += $temp[0]['payment'];
                    }
                }
                $advertisersMappingById[$advertiserBalance['id']]->setConvertedCharge($convertedBalance);
                $em->persist($advertisersMappingById[$advertiserBalance['id']]);
            }
        }
        $em->flush();

        echo 'ok';
    }

}