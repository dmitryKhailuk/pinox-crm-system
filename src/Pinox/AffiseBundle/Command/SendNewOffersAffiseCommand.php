<?php

namespace Pinox\AffiseBundle\Command;

use Pinox\AffiseBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendNewOffersAffiseCommand extends ContainerAwareCommand
{

    const PINOX_URL = 'https://pinox.com';
    const SERVER_NAME = 'http://tools.pinox.com';
    const PINOX_SYSTEM = 'https://my.pinox.com/offer';

    protected function configure()
    {
        $this
            ->setName('affise:send:offers')
            ->setDescription('send new and top offer to email');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);

        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $pathFlags = $container->getParameter('flags_path');
        $emails = [];
        $offersByCategory = [];
        $message = '';

        $emailPartners = $em->getRepository('PinoxAffiseBundle:Affiliate')->getEmailActivePartners();
        $offers = $em->getRepository('PinoxAffiseBundle:Offer')->getNotSendOffers();
        $topOffers = $container->get('pinox_affise_system')->getTopOffersByConversionRate(Offer::TOP_OFFERS_LIMIT);
        $codes = $em->getRepository('PinoxDashboardBundle:Country')->getCodeCountries();

        foreach ($offers as $key => $offer) {
            $offersByCategory[$offer->getCategory()->getName()][$key] = $offer;
        }

        $html = $container->get('templating')->render('PinoxAffiseBundle:templates:email_with_offers.html.twig', [
            'url' => self::PINOX_URL,
            'pathFlags' => $pathFlags,
            'offers' => $offers,
            'topOffers' => $topOffers,
            'serverUrl' => self::SERVER_NAME,
            'myPinox' => self::PINOX_SYSTEM,
            'codes' => $codes,
            'offersByCategory' => $offersByCategory,
        ]);

        foreach ($emailPartners as $emailPartner) {
            $emails[] = $emailPartner['email'];
        }

        $emails = (array_chunk($emails, 50));

        foreach ($emails as $email) {
            $message = \Swift_Message::newInstance()
                ->setSubject('New and top Offers')
                ->setFrom(['noreply@pinox-mail.com' => 'PINOX'])
                ->setTo($email)
                ->setBody($html->getContent(),'text/html');

            $container->get('mailer')->send($message);
        }


        $container->get('mailer')->send($message);

        foreach ($offers as $offer) {
            $offer->setIsSend(true);
            $em->persist($offer);
        }

        $em->flush();
        echo 'ok';
    }

}