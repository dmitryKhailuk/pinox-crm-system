<?php

namespace Pinox\AffiseBundle\Entity;

use Doctrine\ORM\EntityRepository;

class OfferRepository extends EntityRepository
{
    /**
     * @param int $limit
     * @return array
     */
    public function getLastInserted($limit)
    {
        return $this->createQueryBuilder('o')
            ->orderBy('o.affiseId', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()->getArrayResult();
    }

    /**
     * @return Offer[]
     */
    public function getNotSendOffers()
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'c', 'country'])
            ->leftJoin('o.countries', 'country')
            ->innerJoin('o.category', 'c')
            ->where('o.active = :active')
            ->andWhere('c.id NOT IN (:badCategory)')
            ->andWhere('o.isTop = :top')
            ->andWhere('o.isSend = :send')
            ->andWhere('o.privacy <> :privacy')
            ->setParameters([
                'active' => true,
                'badCategory' => [Category::ADULT, Category::SEXY],
                'send' => false,
                'privacy' => Offer::PRIVATE_PRIVACY,
                'top' => false,
            ])
            ->orderBy('c.id', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @return Offer[]
     */
    public function getTopOffers()
    {
        return $this->createQueryBuilder('o')
            ->where('o.active = :active')
            ->andWhere('o.isTop = :top')
            ->andWhere('o.isSend = :send')
            ->andWhere('o.privacy <> :privacy')
            ->setParameters([
                'active' => true,
                'send' => false,
                'privacy' => Offer::PRIVATE_PRIVACY,
                'top' => true,
            ])
            ->orderBy('o.id', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'c', 'country'])
            ->leftJoin('o.countries', 'country')
            ->innerJoin('o.category', 'c')
            ->where('o.active = :active')
            ->setParameter('active', true)
            ->orderBy('o.affiseId', 'DESC');
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getOffersByAffiseId(array $ids = [])
    {
        return $this->createQueryBuilder('o')
            ->select(['o', 'c', 'country'])
            ->leftJoin('o.countries', 'country')
            ->innerJoin('o.category', 'c')
            ->where('o.affiseId IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()->getResult();
    }

    /**
     * @param int $id
     * @return array
     */
    public function getOfferWithTitleMailChimp($id)
    {
        return $this->createQueryBuilder('o')
            ->select('o.title, o.affiseId, o.payout, o.logo, o.descriptionMailchimp AS description')
            ->where('o.affiseId = :id')
            ->setParameter('id', $id)
            ->andWhere('o.descriptionMailchimp IS NOT NULL')
            ->getQuery()->getOneOrNullResult();
    }
}
