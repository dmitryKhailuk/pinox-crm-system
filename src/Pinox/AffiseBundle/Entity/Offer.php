<?php

namespace Pinox\AffiseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\DashboardBundle\Entity\Country;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="OfferRepository")
 * @ORM\Table(
 *     name="offers",
 *     schema="affise_system",
 *     indexes={
 *          @ORM\Index(name="offers_privacy_id_idx", columns={"privacy"}),
 *          @ORM\Index(name="offers_offer_affise_id_id_idx", columns={"offer_affise_id"}),
 *          @ORM\Index(name="offers_currency_id_idx", columns={"currency"})
 *      }
 * )
 */
class Offer
{
    const OFFER_PER_PAGE = 50;
    const STATUS_ACTIVE = 'active';
    const PRIVATE_PRIVACY  = 'private';
    const TOP_OFFERS_LIMIT = 5;

    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="affise_id", type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(value=0)
     * @var string
     */
    private $affiseId;

    /**
     * @ORM\Column(name="offer_affise_id", type="string", length=60, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="60")
     * @var string
     */
    private $offerAffiseId;

    /**
     * @ORM\Column(name="title", type="string", length=250, nullable=true)
     * @Assert\Length(max="250")
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(name="preview_url", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @var string
     */
    private $previewUrl;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"=false})
     * @var bool
     */
    private $active = false;

    /**
     * @ORM\Column(name="description", type="string", length=3000, nullable=true)
     * @Assert\Length(max="3000")
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(name="privacy", type="string", length=40, nullable=true)
     * @Assert\Length(max="40")
     * @var string
     */
    private $privacy;

    /**
     * @ORM\Column(name="is_top", type="boolean", nullable=false, options={"default"=false})
     * @var bool
     */
    private $isTop = false;

    /**
     * @ORM\Column(name="currency", type="string", length=5, nullable=true)
     * @Assert\Length(max="5")
     * @var string
     */
    private $currency;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\DashboardBundle\Entity\Country",
     *      inversedBy="offers",
     *      cascade={"persist"}
     * )
     * @ORM\JoinTable(
     *      name="affise_system.offers_countries",
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *      }
     * )
     * @Assert\Valid()
     */
    private $countries;

    /**
     * @ORM\Column(name="is_send", type="boolean", nullable=false, options={"default"=false})
     * @var bool
     */
    private $isSend = false;

    /**
     * @ORM\Column(name="cr", type="decimal", precision=10, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $cr = 0;

    /**
     * @ORM\Column(name="epc", type="decimal", precision=10, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $epc = 0;

    /**
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="Category",
     *     cascade={"persist"})
     * @var Category
     */
    private $category;

    /**
     * @ORM\Column(name="payout", type="decimal", precision=10, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payout = 0;

    /**
     * @ORM\Column(name="logo", type="string", length=2000, nullable=true)
     * @Assert\Length(max="2000")
     * @var string
     */
    private $logo;

    /**
     * @ORM\Column(name="description_mailchimp", type="string", length=3000, nullable=true)
     * @Assert\Length(max="3000")
     * @var string
     */
    private $descriptionMailchimp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set affiseId
     *
     * @param integer $affiseId
     *
     * @return Offer
     */
    public function setAffiseId($affiseId)
    {
        $this->affiseId = $affiseId;

        return $this;
    }

    /**
     * Get affiseId
     *
     * @return integer
     */
    public function getAffiseId()
    {
        return $this->affiseId;
    }

    /**
     * Set offerAffiseId
     *
     * @param string $offerAffiseId
     *
     * @return Offer
     */
    public function setOfferAffiseId($offerAffiseId)
    {
        $this->offerAffiseId = $offerAffiseId;

        return $this;
    }

    /**
     * Get offerAffiseId
     *
     * @return string
     */
    public function getOfferAffiseId()
    {
        return $this->offerAffiseId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Offer
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set previewUrl
     *
     * @param string $previewUrl
     *
     * @return Offer
     */
    public function setPreviewUrl($previewUrl)
    {
        $this->previewUrl = $previewUrl;

        return $this;
    }

    /**
     * Get previewUrl
     *
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->previewUrl;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Offer
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Offer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set privacy
     *
     * @param string $privacy
     *
     * @return Offer
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return string
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set isTop
     *
     * @param boolean $isTop
     *
     * @return Offer
     */
    public function setIsTop($isTop)
    {
        $this->isTop = $isTop;

        return $this;
    }

    /**
     * Get isTop
     *
     * @return boolean
     */
    public function getIsTop()
    {
        return $this->isTop;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Offer
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Add country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     *
     * @return Offer
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \Pinox\DashboardBundle\Entity\Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return Offer
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set cr
     *
     * @param string $cr
     *
     * @return Offer
     */
    public function setCr($cr)
    {
        $this->cr = $cr;

        return $this;
    }

    /**
     * Get cr
     *
     * @return string
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * Set erp
     *
     * @param string $epc
     *
     * @return Offer
     */
    public function setEpc($epc)
    {
        $this->epc = $epc;

        return $this;
    }

    /**
     * Get erp
     *
     * @return string
     */
    public function getEpc()
    {
        return $this->epc;
    }

    /**
     * Set category
     *
     * @param \Pinox\AffiseBundle\Entity\Category $category
     *
     * @return Offer
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Pinox\AffiseBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set payout
     *
     * @param string $payout
     *
     * @return Offer
     */
    public function setPayout($payout)
    {
        $this->payout = $payout;

        return $this;
    }

    /**
     * Get payout
     *
     * @return string
     */
    public function getPayout()
    {
        return $this->payout;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Offer
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Set descriptionMailchimp
     *
     * @param string $descriptionMailchimp
     *
     * @return Offer
     */
    public function setDescriptionMailchimp($descriptionMailchimp)
    {
        $this->descriptionMailchimp = $descriptionMailchimp;

        return $this;
    }

    /**
     * Get descriptionMailchimp
     *
     * @return string
     */
    public function getDescriptionMailchimp()
    {
        return $this->descriptionMailchimp;
    }
}
