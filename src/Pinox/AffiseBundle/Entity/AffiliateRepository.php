<?php

namespace Pinox\AffiseBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Pinox\AffiseBundle\Command\SynchronizationUsersAffiseCommand;

class AffiliateRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function  getListQueryBuilder()
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'w'])
            ->leftJoin('a.webmaster', 'w')
            ->orderBy('a.affiseId', 'DESC');
    }

    /**
     * @return Affiliate[]
     */
    public function getAffiliates()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.affiseId', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @return Affiliate[]
     */
    public function getLastFiftyAffiliates()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.affiseId', 'DESC')
            ->setMaxResults(SynchronizationUsersAffiseCommand::SYNCHRONIZATION_USERS_LIMIT)
            ->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getEmailActivePartners()
    {
        return $this->createQueryBuilder('a')
            ->select('a.email')
            ->where('a.active = :active')
            ->setParameter('active', true)
            ->getQuery()->getArrayResult();
    }

    /**
     * @return array
     */
    public function getBalanceActiveAffiliates()
    {
        return $this->createQueryBuilder('a')
            ->select('a.balance')
            ->where('a.active = :active')
            ->setParameter('active', true)
            ->getQuery()->getArrayResult();
    }

    /**
     * @param \DateTime $date
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getAffiliateReminderToday(\DateTime $date)
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.webmaster', 'w')
            ->where('w.dateReminder <= :date')
            ->andWhere('w.recalled = :recalled')
            ->setParameters([
                'date' => $date->format('Y-m-d 00:00:00'),
                'recalled' => false,
            ])
            ->orderBy('w.dateReminder', 'DESC');
    }

    /**
     * @param integer $id
     * @return Affiliate
     */
    public function findWithJoinsForViewAction($id)
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'w'])
            ->leftJoin('a.webmaster', 'w')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getOneOrNullResult();
    }
}
