<?php
namespace Pinox\AffiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CategoryRepository")
 * @ORM\Table(
 *     name="categories",
 *     schema="affise_system"
 * )
 */
class Category
{
    const ADULT = 1;
    const SEXY = 3;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="affise_id", type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     * @var string
     */
    private $affiseId;

    /**
     * @ORM\Column(name="name", type="string", length=80, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="80")
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set affiseId
     *
     * @param string $affiseId
     *
     * @return Category
     */
    public function setAffiseId($affiseId)
    {
        $this->affiseId = $affiseId;

        return $this;
    }

    /**
     * Get affiseId
     *
     * @return string
     */
    public function getAffiseId()
    {
        return $this->affiseId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
