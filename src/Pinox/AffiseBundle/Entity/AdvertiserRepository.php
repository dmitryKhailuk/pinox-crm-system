<?php

namespace Pinox\AffiseBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AdvertiserRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'ai', 'pa'])
            ->leftJoin('a.advertiserInfo', 'ai')
            ->leftJoin('a.paymentAdvertisers', 'pa')
            ->where('(ai.active = :active OR ai.active IS NULL)')
            ->setParameter('active', true)
            ->orderBy('a.title', 'ASC');
    }

    /**
     * @return array
     */
    public function getArchiveAdvertisers()
    {
        return $this->createQueryBuilder('a')
            ->innerJoin('a.advertiserInfo', 'ai')
            ->where('ai.active = :active')
            ->setParameter('active', false)
            ->orderBy('a.contact', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @param integer $id
     * @return Advertiser
     */
    public function findWithJoinsForViewAction($id)
    {
        return $this->createQueryBuilder('a')
            ->select(['a', 'ai'])
            ->leftJoin('a.advertiserInfo', 'ai')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getOneOrNullResult();
    }


    /**
     * @return array
     */
    public function getBalance()
    {
        return $this->createQueryBuilder('a')
            ->select('a.confirmedCharge')
            ->where('a.confirmedCharge IS NOT NULL')
            ->getQuery()->getResult();
    }
}
