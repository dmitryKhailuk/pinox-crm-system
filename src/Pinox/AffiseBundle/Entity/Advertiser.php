<?php
namespace Pinox\AffiseBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo;
use Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AdvertiserRepository")
 * @ORM\Table(
 *     name="advertisers",
 *     schema="affise_system",
 *     indexes={
 *          @ORM\Index(name="advertisers_affise_id_unq_idx", columns={"affise_id"}),
 *          @ORM\Index(name="advertisers_email_unq_idx", columns={"email"})
 *      }
 * )
 */
class Advertiser
{
    const ADVERTISER_PER_PAGE = 50;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="email", type="string", length=70, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="70")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="affise_id", type="string", length=50, nullable=false)
     * @Assert\NotBlank()
     * @var string
     */
    private $affiseId;

    /**
     * @ORM\Column(name="title", type="string", length=80, nullable=true)
     * @Assert\Length(max="80")
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(name="contact", type="string", length=80, nullable=true)
     * @Assert\Length(max="80")
     * @var string
     */
    private $contact;

    /**
     * @ORM\Column(name="url", type="string", length=3000, nullable=true)
     * @Assert\Length(max="3000")
     * @var string
     */
    private $url;

    /**
     * @ORM\Column(name="manager", type="string", length=80, nullable=true)
     * @Assert\Length(max="80")
     * @var string
     */
    private $manager;

    /**
     * @ORM\Column(name="skype", type="string", length=40, nullable=true)
     * @Assert\Length(max="40")
     * @var string
     */
    private $skype;

    /**
     * @ORM\Column(name="offers", type="integer", nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var integer
     */
    private $offers = 0;

    /**
     * @ORM\Column(name="note", type="string", length=3000, nullable=true)
     * @Assert\Length(max="3000")
     * @var string
     */
    private $note;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo",
     *     mappedBy="advertiser",
     *     cascade={"persist","remove"}
     *     )
     * @var AdvertiserInfo
     */
    private $advertiserInfo;

    /**
     * @ORM\Column(name="confirmed_charge", type="json_array", nullable=true)
     * @var array
     */
    private $confirmedCharge;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser",
     *     mappedBy="advertiser",
     *     cascade={"persist","remove"}
     *     )
     * @var PaymentAdvertiser
     */
    private $paymentAdvertisers;

    /**
     * @ORM\Column(name="converted_charge", type="decimal", precision=12, scale=2, nullable=true)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $convertedCharge = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paymentAdvertisers = new ArrayCollection();
    }

    /**
     * @return float
     */
    public function getBalanceAdvertiser()
    {
        $sum = $this->getConvertedCharge();

        /** @var PaymentAdvertiser $paymentAdvertiser */
        foreach ($this->getPaymentAdvertisers() as $paymentAdvertiser) {
            $sum -= $paymentAdvertiser->getConvertedPayment();
        }

        return $sum;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Advertiser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set affiseId
     *
     * @param string $affiseId
     *
     * @return Advertiser
     */
    public function setAffiseId($affiseId)
    {
        $this->affiseId = $affiseId;

        return $this;
    }

    /**
     * Get affiseId
     *
     * @return string
     */
    public function getAffiseId()
    {
        return $this->affiseId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Advertiser
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Advertiser
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Advertiser
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set manager
     *
     * @param string $manager
     *
     * @return Advertiser
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return string
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return Advertiser
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set offers
     *
     * @param integer $offers
     *
     * @return Advertiser
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;

        return $this;
    }

    /**
     * Get offers
     *
     * @return integer
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Advertiser
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set advertiserInfo
     *
     * @param \Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo $advertiserInfo
     *
     * @return Advertiser
     */
    public function setAdvertiserInfo(AdvertiserInfo $advertiserInfo = null)
    {
        $this->advertiserInfo = $advertiserInfo;

        return $this;
    }

    /**
     * Get advertiserInfo
     *
     * @return \Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo
     */
    public function getAdvertiserInfo()
    {
        return $this->advertiserInfo;
    }

    /**
     * Set confirmedCharge
     *
     * @param array $confirmedCharge
     *
     * @return Advertiser
     */
    public function setConfirmedCharge($confirmedCharge)
    {
        $this->confirmedCharge = $confirmedCharge;

        return $this;
    }

    /**
     * Get confirmedCharge
     *
     * @return array
     */
    public function getConfirmedCharge()
    {
        return $this->confirmedCharge;
    }

    /**
     * Set convertedCharge
     *
     * @param string $convertedCharge
     *
     * @return Advertiser
     */
    public function setConvertedCharge($convertedCharge)
    {
        $this->convertedCharge = $convertedCharge;

        return $this;
    }

    /**
     * Get convertedCharge
     *
     * @return string
     */
    public function getConvertedCharge()
    {
        return $this->convertedCharge;
    }

    /**
     * Add paymentAdvertiser
     *
     * @param \Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser $paymentAdvertiser
     *
     * @return Advertiser
     */
    public function addPaymentAdvertiser(PaymentAdvertiser $paymentAdvertiser)
    {
        $this->paymentAdvertisers[] = $paymentAdvertiser;

        return $this;
    }

    /**
     * Remove paymentAdvertiser
     *
     * @param \Pinox\AdvertiserServiceBundle\Entity\PaymentAdvertiser $paymentAdvertiser
     */
    public function removePaymentAdvertiser(PaymentAdvertiser $paymentAdvertiser)
    {
        $this->paymentAdvertisers->removeElement($paymentAdvertiser);
    }

    /**
     * Get paymentAdvertisers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentAdvertisers()
    {
        return $this->paymentAdvertisers;
    }
}
