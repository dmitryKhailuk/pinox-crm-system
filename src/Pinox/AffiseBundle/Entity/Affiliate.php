<?php
namespace Pinox\AffiseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\AffiliateServiceBundle\Entity\Webmaster;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AffiliateRepository")
 * @ORM\Table(
 *     name="affiliates",
 *     schema="affise_system"
 * )
 */
class Affiliate
{
    const AFFILIATE_PER_PAGE = 50;
    const STATUS_ACTIVE = 'active';
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="login", type="string", length=150, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="150")
     * @var string
     */
    private $login;

    /**
     * @ORM\Column(name="email", type="string", length=150, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="150")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="affise_id", type="integer", nullable=false)
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(value=0)
     * @var string
     */
    private $affiseId;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"=false})
     * @var bool
     */
    private $active = false;

    /**
     * @ORM\Column(name="api_key", type="string", length=50, nullable=true)
     * @Assert\Length(max="50")
     * @var string
     */
    private $apiKey;

    /**
     * @ORM\Column(name="in_mailchump", type="boolean", nullable=false, options={"default"=false})
     * @var bool
     */
    private $inMailchump = false;

    /**
     * @ORM\Column(name="balance", type="json_array", nullable=true)
     * @var array
     */
    private $balance;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Pinox\AffiliateServiceBundle\Entity\Webmaster",
     *     mappedBy="affiliate",
     *     cascade={"persist","remove"}
     *     )
     * @var Webmaster
     */
    private $webmaster;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Affiliate
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Affiliate
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set affiseId
     *
     * @param integer $affiseId
     *
     * @return Affiliate
     */
    public function setAffiseId($affiseId)
    {
        $this->affiseId = $affiseId;

        return $this;
    }

    /**
     * Get affiseId
     *
     * @return integer
     */
    public function getAffiseId()
    {
        return $this->affiseId;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Affiliate
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return Affiliate
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set inMailchump
     *
     * @param boolean $inMailchump
     *
     * @return Affiliate
     */
    public function setInMailchump($inMailchump)
    {
        $this->inMailchump = $inMailchump;

        return $this;
    }

    /**
     * Get inMailchump
     *
     * @return boolean
     */
    public function getInMailchump()
    {
        return $this->inMailchump;
    }

    /**
     * Set balance
     *
     * @param array $balance
     *
     * @return Affiliate
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return array
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set webmaster
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\Webmaster $webmaster
     *
     * @return Affiliate
     */
    public function setWebmaster(\Pinox\AffiliateServiceBundle\Entity\Webmaster $webmaster = null)
    {
        $this->webmaster = $webmaster;

        return $this;
    }

    /**
     * Get webmaster
     *
     * @return \Pinox\AffiliateServiceBundle\Entity\Webmaster
     */
    public function getWebmaster()
    {
        return $this->webmaster;
    }
}
