<?php

namespace Pinox\AffiseBundle\Controller;

use Pinox\AffiseBundle\Command\Traits\MappingCategoriesByIdTrait;
use Pinox\AffiseBundle\Command\Traits\MappingCountriesByCodeTrait;
use Pinox\AffiseBundle\Command\Traits\SetParametersOfferTrait;
use Pinox\AffiseBundle\Entity\Offer;
use Pinox\AffiseBundle\Form\AffiseOfferType;
use Pinox\AffiseBundle\Form\OfferFilterType;
use Pinox\AffiseBundle\Form\OfferType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/offers")
 * @Security("has_role('ROLE_PINOX_TOOLS_USER') or has_role('ROLE_SHOW_MODULE_TOOL')")
 */
class OfferController extends Controller
{
    use MappingCountriesByCodeTrait, MappingCategoriesByIdTrait, SetParametersOfferTrait;
    /**
     * @Route("/{page}", name="offer_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiseBundle:Offer')
            ->getListQueryBuilder();

        $form = $this->createForm(OfferFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Offer::OFFER_PER_PAGE
        );

        return $this->render('PinoxAffiseBundle:Offer:index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
        ]);
    }

    /**
     * @Route("/offer_for_template/{id}", name="offer_offer_for_template")
     * @Method({"GET", "PUT"})
     * @ParamConverter("offer", class="PinoxAffiseBundle:Offer")
     * @param Offer $offer
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function addToTemplateAction(Request $request, Offer $offer)
    {
        $urlForGenerate = 'offer_index';
        $em = $this->getDoctrine()->getManager();

        if ($offer->getIsSend() === true) {
            $offer->setIsSend(false);
        } else {
            $offer->setIsSend(true);
            if ($request->query->get('offer') != true) {
                $urlForGenerate = 'mail_chimp_index';
            }
        }

        $em->persist($offer);
        $em->flush();

        return $this->redirect($this->generateUrl($urlForGenerate));
    }

    /**
     * @Route("/affise_update", name="offer_affise_update")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function affiseUpdateAction(Request $request)
    {
        $form = $this->createForm(AffiseOfferType::class);
        $form->handleRequest($request);
        $affiseOffers = [];
        $offersMappingByAffiseId = [];

        if($form->isSubmitted() && $form->isValid()) {
            $affiseSystem = $this->get('pinox_affise_system');
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();

            $data = array_key_exists('offers', $data) ? explode(';', $data['offers']) : [];
            $mappingCountries = $this->mappingCountriesByCode($em);
            $mappingCategories = $this->mappingCategoriesById($em);

            foreach ($data as $key => $value) {
                $id = trim($value);
                if ($id && is_numeric($id)) {
                    $affiseOffers[] = $affiseSystem->getOfferById($id);
                } else {
                    unset($data[$key]);
                }
            }

            $offers = $em->getRepository('PinoxAffiseBundle:Offer')->getOffersByAffiseId($data);
            foreach ($offers as $offer) {
                $offersMappingByAffiseId[$offer->getAffiseId()] = $offer;
            }

            foreach ($affiseOffers as $affiseOffer) {
                if ($affiseOffer) {
                    $offer = array_key_exists($affiseOffer['id'], $offersMappingByAffiseId)
                        ? $offersMappingByAffiseId[$affiseOffer['id']] : new Offer();

                    $offer = $this->setOfferParameters($offer, $affiseOffer, $mappingCategories, $mappingCountries);
                    $em->persist($offer);
                }

            }
            $em->flush();

            return $this->redirect($this->generateUrl('offer_index'));
        }


        return $this->render('PinoxAffiseBundle:Offer:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'update',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="offer_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("offer", class="PinoxAffiseBundle:Offer")
     * @param Request $request
     * @param Offer $offer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Offer $offer)
    {
        $form = $this->createForm(OfferType::class, $offer, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();

            return $this->redirect($this->generateUrl('offer_index'));
        }

        return $this->render('PinoxAffiseBundle:Offer:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'offer' => $offer
        ]);

    }

}
