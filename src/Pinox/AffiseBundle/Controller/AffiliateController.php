<?php

namespace Pinox\AffiseBundle\Controller;

use Pinox\AffiseBundle\Entity\Affiliate;
use Pinox\AffiseBundle\Form\AffiliateFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/affiliates")
 */
class AffiliateController extends Controller
{

    /**
     * @Route("/{page}", name="affiliate_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE')")
     * @param Request $request
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiseBundle:Affiliate')
            ->getListQueryBuilder();
        $form = $this->createForm(AffiliateFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Affiliate::AFFILIATE_PER_PAGE
        );

        return $this->render('PinoxAffiseBundle:Affiliate:index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
        ]);
    }

    /**
     * @Route("/reminders/{page}", name="affiliate_reminder", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function reminderAction($page)
    {
        $today = new \DateTime('now +1 day');

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiseBundle:Affiliate')
            ->getAffiliateReminderToday($today);

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Affiliate::AFFILIATE_PER_PAGE
        );

        return $this->render('PinoxAffiseBundle:Affiliate:reminder.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/view/{id}", name="affiliate_view")
     * @Method("GET")
     * @ParamConverter(
     *     "advertiser",
     *     class="PinoxAffiseBundle:Affiliate",
     *     options={"repository_method" = "findWithJoinsForViewAction"}
     * )
     * @param Affiliate $affiliate
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function viewAction(Affiliate $affiliate)
    {
        return $this->render('PinoxAffiseBundle:Affiliate:view.html.twig', [
            'affiliate' => $affiliate
        ]);
    }
}
