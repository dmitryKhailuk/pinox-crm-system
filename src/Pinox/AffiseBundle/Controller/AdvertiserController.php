<?php

namespace Pinox\AffiseBundle\Controller;

use Pinox\AffiseBundle\Entity\Advertiser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/advertisers")
 * @Security("has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class AdvertiserController extends Controller
{

    /**
     * @Route("/{page}", name="advertiser_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxAffiseBundle:Advertiser')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Advertiser::ADVERTISER_PER_PAGE
        );

        return $this->render('PinoxAffiseBundle:Advertiser:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }


    /**
     * @Route("/archive", name="advertiser_archive")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function archiveAction(Request $request)
    {
        $balance = $request->query->get('balance');
        $em = $this->getDoctrine()->getManager();
        $archiveAdvertisers = $em->getRepository('PinoxAffiseBundle:Advertiser')->getArchiveAdvertisers();

        return $this->render('PinoxAffiseBundle:Advertiser:archive.html.twig', [
            'archiveAdvertisers' => $archiveAdvertisers,
            'balance' => $balance,
        ]);
    }

    /**
     * @Route("/view/{id}", name="advertiser_view")
     * @Method("GET")
     * @ParamConverter(
     *     "advertiser",
     *     class="PinoxAffiseBundle:Advertiser",
     *     options={"repository_method" = "findWithJoinsForViewAction"}
     * )
     * @param Advertiser $advertiser
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function viewAction(Advertiser $advertiser)
    {
        return $this->render('PinoxAffiseBundle:Advertiser:view.html.twig', [
            'advertiser' => $advertiser
        ]);
    }

    /**
     * @Route("/balance/{page}", name="advertiser_balance", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function balanceAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $totalBalanceByCurrency = 0;

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $em->getRepository('PinoxAffiseBundle:Advertiser')
            ->getListQueryBuilder();


        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Advertiser::ADVERTISER_PER_PAGE
        );

        foreach ($pagination as $advertiser) {
            $totalBalanceByCurrency += $advertiser->getBalanceAdvertiser();
        }

        return $this->render('PinoxAffiseBundle:Advertiser:balance.html.twig', [
            'pagination' => $pagination,
            'totalBalanceByCurrency' => $totalBalanceByCurrency,
        ]);
    }

}
