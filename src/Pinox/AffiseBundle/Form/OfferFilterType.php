<?php

namespace Pinox\AffiseBundle\Form;

use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\NumberFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;

class OfferFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('affiseId', NumberFilterType::class, [
            'label' => 'offer.affise_id',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
            'constraints' => [ new GreaterThanOrEqual(0) ],
        ]);

        $builder->add('title', TextFilterType::class, [
            'label' => 'offer.title_column',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
            'attr' => [
                'maxlength' => 100,
            ],
            'constraints' => [ new Length(['max' => 100]) ],
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $expression = $filterQuery->getExpr()->like('o.title', ':title');
                $parameters = array('title' => '%' . $values['value'] . '%');
                return $filterQuery->createCondition($expression, $parameters);
            }
        ]);

        $builder->add('category', EntityFilterType::class, [
            'label' => 'offer.category',
            'class' => 'PinoxAffiseBundle:Category',
            'choice_label' => 'name',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method'  => 'GET',
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_affise_offer_filter';
    }
}
