<?php

namespace Pinox\AffiseBundle\Form;

use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;

class AffiliateFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('affiliateId', IntegerType::class, [
            'label' => 'affiliate.affiliate_id',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
            'constraints' => [ new GreaterThanOrEqual(0) ]
        ]);

        $builder->add('email', TextType::class, [
            'label' => 'affiliate.email',
            'required' => false,
            'label_attr' => [ 'class' => 'filter-label' ],
            'attr' => [
                'maxlength' => 100,
            ],
            'constraints' => [ new Length(['max' => 100]) ],
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $expression = $filterQuery->getExpr()->like('a.email', ':email');
                $parameters = array('email' => '%' . $values['value'] . '%');
                return $filterQuery->createCondition($expression, $parameters);
            }

        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method'  => 'GET',
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_affise_affiliate_filter';
    }
}
