<?php

namespace Pinox\AffiseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferTopType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextareaType::class, [
            'label' => 'mail_chimp.title_template',
            'required' => false,
        ]);

        $builder->add('topOffers', TextType::class, [
            'label' => 'offer.top_offers',
            'required' => false,
            'attr' => [
                'maxlength' => 300,
            ],
        ]);

        $builder->add('hotOffers', TextType::class, [
            'label' => 'mail_chimp.hot_offers',
            'required' => false,
            'attr' => [
                'maxlength' => 300,
            ],
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_affise_offer_top';
    }
}
