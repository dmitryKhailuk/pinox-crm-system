<?php

namespace Pinox\DashboardBundle\Services;

use Doctrine\ORM\EntityManager;
use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;

class MailChimpApi
{

    use CurlRequestTrait;

    const API_KEY = '966f5522b23951a5e47a9784fb74ae49-us15';
    const URL_API = 'https://us15.api.mailchimp.com/3.0';

    /** @var array */
    private $headers = [];

    /** @var  EntityManager $em */
    private $em = null;

    /** @var MailChimpApi */
    public static $instance = null;

    /**
     * MailChumpApi constructor.
     * @param EntityManager $em
     */
    private function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    private function __clone()
    {
    }

    /**
     * @param EntityManager $em
     * @return MailChimpApi
     */
    public function getInstance(EntityManager $em)
    {
        if (null === self::$instance) {
            self::$instance = new MailChimpApi($em);
        }
        return self::$instance;
    }

    /**
     * @param array $headers
     * @return array
     */
    public function headersInitialization(array $headers = [])
    {
        if (count($headers) > 0) {
            $this->headers = $headers;
        } else {
            $this->headers = [
                'Content-Type: application/json',
                'Authorization: Basic '. base64_encode( 'user:' . self::API_KEY)
            ];
        }

        return $this->headers;
    }

    /**
     * @param $listId
     * @return array
     */
    public function allMembers($listId)
    {
        $url = self::URL_API . '/lists/' . $listId . '/members';
        $members = json_decode($this->curlGet($url, [], $this->headersInitialization()), true);
        if (is_array($members)) {
            return array_key_exists('members', $members) ? $members['members'] : [];
        }
        return [];
    }

    /**
     * @param $listId
     * @param array $parameters
     * @return array
     */
    public function addNewMembers($listId, array $parameters)
    {
        $url = self::URL_API .'/lists/' . $listId . '/members';
        $post = [
            'email_address' => $parameters['email'],
            'status' => $parameters['status'],
        ];
        $post = json_encode($post);

        return json_decode($this->curlPost($url, $post, $this->headersInitialization()), true);
    }

    /**
     * @param string $nameTemplate
     * @param string $html
     * @return array
     */
    public function createTemplate($nameTemplate, $html)
    {
        $url = self::URL_API .'/templates';
        $post = [
            'name' => $nameTemplate,
            'html' => $html,
        ];
        $post = json_encode($post);

        return json_decode($this->curlPost($url, $post, $this->headersInitialization()), true);
    }

}