<?php

namespace Pinox\DashboardBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo;
use Pinox\AffiliateServiceBundle\Entity\Webmaster;
use Pinox\AffiseBundle\Entity\Offer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CountryRepository")
 * @ORM\Table(
 *     name="countries",
 *     schema="public",
 *     indexes={
 *          @ORM\Index(name="countries_code_id_idx", columns={"code"}),
 *          @ORM\Index(name="countries_name_id_idx", columns={"name"})
 *      }
 * )
 */
class Country
{
    const COUNTRY_PER_PAGES = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="code", type="string", length=4, nullable=false)
     * @Assert\Length(max="4")
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(name="country", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $country;

    /**
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     * @Assert\Length(max="150")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="image", type="string", length=100, nullable=true)
     * @Assert\Length(max="100")
     * @var string
     */
    private $image;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\AffiseBundle\Entity\Offer",
     *      mappedBy="countries"
     * )
     * @var Offer
     */
    private $offers;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\AffiliateServiceBundle\Entity\Webmaster",
     *      mappedBy="countries"
     * )
     * @var Webmaster
     */
    private $webmasters;

    /**
     * @ORM\ManyToMany(
     *      targetEntity="Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo",
     *      mappedBy="countries"
     * )
     * @var AdvertiserInfo
     */
    private $advertisersInfo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->webmasters = new ArrayCollection();
        $this->advertisersInfo = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Country
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Country
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add offer
     *
     * @param \Pinox\AffiseBundle\Entity\Offer $offer
     *
     * @return Country
     */
    public function addOffer(Offer $offer)
    {
        $this->offers[] = $offer;

        return $this;
    }

    /**
     * Remove offer
     *
     * @param \Pinox\AffiseBundle\Entity\Offer $offer
     */
    public function removeOffer(Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * Add webmaster
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\Webmaster $webmaster
     *
     * @return Country
     */
    public function addWebmaster(Webmaster $webmaster)
    {
        $this->webmasters[] = $webmaster;

        return $this;
    }

    /**
     * Remove webmaster
     *
     * @param \Pinox\AffiliateServiceBundle\Entity\Webmaster $webmaster
     */
    public function removeWebmaster(Webmaster $webmaster)
    {
        $this->webmasters->removeElement($webmaster);
    }

    /**
     * Get webmasters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWebmasters()
    {
        return $this->webmasters;
    }

    /**
     * Add advertisersInfo
     *
     * @param \Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo $advertisersInfo
     *
     * @return Country
     */
    public function addAdvertisersInfo(AdvertiserInfo $advertisersInfo)
    {
        $this->advertisersInfo[] = $advertisersInfo;

        return $this;
    }

    /**
     * Remove advertisersInfo
     *
     * @param \Pinox\AdvertiserServiceBundle\Entity\AdvertiserInfo $advertisersInfo
     */
    public function removeAdvertisersInfo(AdvertiserInfo $advertisersInfo)
    {
        $this->advertisersInfo->removeElement($advertisersInfo);
    }

    /**
     * Get advertisersInfo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvertisersInfo()
    {
        return $this->advertisersInfo;
    }
}
