<?php

namespace Pinox\DashboardBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getCodeCountries()
    {
        return $this->createQueryBuilder('c')
            ->select('lower(c.code) as code, c.image')
            ->getQuery()->getArrayResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC');
    }
}
