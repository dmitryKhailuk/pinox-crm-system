<?php

namespace Pinox\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Pinox\BookkeepingBundle\Entity\Currency;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AvailabilityCashRepository")
 * @ORM\Table(
 *     name="availability_cash",
 *     schema="pinox_dashboard"
 * )
 */
class AvailabilityCash
{
    const CASH_PER_PAGES = 25;
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\Length(max="100")
     * @Assert\NotBlank()
     * @var string
     */
    private $name;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="Pinox\BookkeepingBundle\Entity\Currency",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;

    /**
     * @ORM\Column(name="payment", type="decimal", precision=15, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payment = 0;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=true, options={"default"=true})
     * @var bool
     */
    private $active = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AvailabilityCash
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return AvailabilityCash
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return AvailabilityCash
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set currency
     *
     * @param \Pinox\BookkeepingBundle\Entity\Currency $currency
     *
     * @return AvailabilityCash
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Pinox\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
