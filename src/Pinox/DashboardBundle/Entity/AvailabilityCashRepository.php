<?php

namespace Pinox\DashboardBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AvailabilityCashRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('ac')
            ->innerJoin('ac.currency', 'c')
            ->orderBy('ac.name', 'ASC');
    }

    /**
     * @return array
     */
    public function getTotalSumCashFromCurrency()
    {
        return $this->createQueryBuilder('ac')
            ->select('SUM(ac.payment) as payment, c.name as currency, c.code')
            ->where('ac.active = :active')
            ->setParameter('active', true)
            ->innerJoin('ac.currency', 'c')
            ->groupBy('c.code', 'c.name')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();
    }
}
