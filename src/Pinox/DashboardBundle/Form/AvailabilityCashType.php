<?php

namespace Pinox\DashboardBundle\Form;

use Pinox\BookkeepingBundle\Entity\CurrencyRepository;
use Pinox\DashboardBundle\Entity\AvailabilityCash;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AvailabilityCashType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('name', TextType::class, [
            'label' => 'availability_cash.name',
            'required' => true,
            'attr' => [
                'maxlength' => 100
            ]
        ]);

        $builder->add('payment', TextType::class, [
            'label' => 'availability_cash.payment',
            'required' => true,
        ]);

        $builder->add('currency', EntityType::class, [
            'label' => 'availability_cash.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
            'query_builder' => function (CurrencyRepository $repository) {
                return $repository->getListQueryBuilder();
            }
        ]);

        $builder->add('active', CheckboxType::class, [
            'label' => 'availability_cash.active',
            'required' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AvailabilityCash::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_dashboard_availability_cash';
    }
}
