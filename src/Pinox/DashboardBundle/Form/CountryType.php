<?php

namespace Pinox\DashboardBundle\Form;

use Pinox\DashboardBundle\Entity\Country;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('country', TextType::class, [
            'label' => 'country.country',
            'required' => true,
            'attr' => [
                'maxlength' => 100
            ]
        ]);

        $builder->add('name', TextType::class, [
            'label' => 'country.name',
            'required' => true,
            'attr' => [
                'maxlength' => 150
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Country::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_dashboard_country';
    }
}
