<?php

namespace Pinox\DashboardBundle\Command;

use Pinox\AffiseBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateTemplateWithOffersCommand extends ContainerAwareCommand
{

    const PINOX_URL = 'https://pinox.com';
    const SERVER_NAME = 'http://tools.pinox.com';
    const PINOX_SYSTEM = 'https://my.pinox.com/offer';

    protected function configure()
    {
        $this
            ->setName('mailchump:generate:template')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return string
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getManager();
        $pathFlags = $container->getParameter('flags_path');
        $offersByCategory = [];
        $date = new \DateTime('now');

        $offers = $em->getRepository('PinoxAffiseBundle:Offer')->getNotSendOffers();
        $codes = $em->getRepository('PinoxDashboardBundle:Country')->getCodeCountries();

        if (count($offers) > 0) {
            $topOffers = $container->get('pinox_affise_system')->getTopOffers(Offer::TOP_OFFERS_LIMIT);

            foreach ($offers as $key => $offer) {
                $offersByCategory[$offer->getCategory()->getName()][$key] = $offer;
            }

            $html = $container->get('templating')->render('PinoxDashboardBundle:templates:offers_mail.html.twig', [
                'url' => self::PINOX_URL,
                'pathFlags' => $pathFlags,
                'offers' => $offers,
                'topOffers' => $topOffers,
                'serverUrl' => self::SERVER_NAME,
                'myPinox' => self::PINOX_SYSTEM,
                'codes' => $codes,
                'offersByCategory' => $offersByCategory,
            ]);

            $container->get('pinox_mailchimp_api')->createTemplate('Offers_' . $date->format('d.m.Y'), $html);

            /** @var  $offer Offer */
            foreach ($offers as $offer) {
                $offer->setIsSend(true);
                $em->persist($offer);
            }

            $em->flush();
        }

        echo 'ok';
    }

}