<?php

namespace Pinox\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewPartnersToMailChumpCommand extends ContainerAwareCommand
{

    const STATUS_MAILCHUMP = 'subscribed';
    const ANSWER_MAILCHUMP_EXIST = 'Member Exists';
    const ANSWER_MAILCHUMP_INVALID = 'Invalid Resource';

    protected function configure()
    {
        $this
            ->setName('mailchump:new:partners')
            ->setDescription('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(10000);
        $em = $this->getContainer()->get('doctrine')->getManager();
        $mailChump = $this->getContainer()->get('pinox_mailchimp_api');

        $partners = $em->getRepository('PinoxAffiseBundle:Affiliate')->findBy(['inMailchump' => false]);

        foreach ($partners as $partner) {
            $response = $mailChump->addNewMembers('a45eb77fcc', [
                'email' => $partner->getEmail(),
                'status' => self::STATUS_MAILCHUMP,
            ]);
            if (is_array($response) && array_key_exists('title', $response)
                && (strcasecmp($response['title'], self::ANSWER_MAILCHUMP_EXIST) === 0
                    || strcasecmp($response['title'], self::ANSWER_MAILCHUMP_INVALID) === 0 ) ) {
                $partner->setInMailchump(true);
                $em->persist($partner);
            }

        }

        $em->flush();

        echo 'ok';
    }

}