<?php

namespace Pinox\DashboardBundle\Controller;

use Pinox\DashboardBundle\Entity\Country;
use Pinox\DashboardBundle\Form\CountryFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Pinox\DashboardBundle\Form\CountryType;
/**
 * @Route("/countries")
 * @Security("has_role('ROLE_SHOW_MODULE_AFFILIATE_SERVICE') or has_role('ROLE_SHOW_MODULE_ADVERTISER_SERVICE')")
 */
class CountryController extends Controller
{

    /**
     * @Route("/{page}", name="country_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxDashboardBundle:Country')
            ->getListQueryBuilder();

        $form = $this->createForm(CountryFilterType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && array_key_exists('name', $form->getData())) {
            $page = 1;
            $searchString = $form->getData()['name'];
            $queryBuilder = $queryBuilder->where("( c.name LIKE :name OR c.country LIKE :name)")
                ->setParameter('name', '%' . $searchString . '%');
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $page,
            Country::COUNTRY_PER_PAGES
        );

        return $this->render('PinoxDashboardBundle:Country:index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
        ]);
    }

    /**
     * @Route("/new", name="country_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $country = new Country();
        $form = $this->createForm(CountryType::class, $country);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $country->setCode('ALL');
            $country->setImage($country->getCountry() . 'png');
            $em->persist($country);
            $em->flush();

            return $this->redirect($this->generateUrl('country_index'));
        }

        return $this->render('PinoxDashboardBundle:Country:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="country_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("country", class="PinoxDashboardBundle:Country")
     * @param Request $request
     * @param Country $country
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Country $country)
    {
        $form = $this->createForm(CountryType::class, $country, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($country);
            $em->flush();

            return $this->redirect($this->generateUrl('country_index'));
        }

        return $this->render('PinoxDashboardBundle:Country:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

}
