<?php

namespace Pinox\DashboardBundle\Controller\Traits;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;

trait ProcessesEntityRemovalTrait
{
    /**
     * @param object $entity
     * @param Request $request
     * @param ContainerInterface $container
     */
    private function processEntityRemoval($entity, Request $request, ContainerInterface $container)
    {
        /** @var \Symfony\Component\Security\Csrf\CsrfTokenManagerInterface $tm */
        $tm = $container->get('security.csrf.token_manager');
        $token = new CsrfToken('delete', $request->request->get('_csrf_token'));

        if ($tm->isTokenValid($token)) {
            try {
                /** @var EntityManager $em */
                $em = $container->get('doctrine')->getManager();
                $em->remove($entity);
                $em->flush();

                $container->get('session')->getFlashBag()->add(
                    'success',
                    $container->get('translator')->trans('success_alerts.delete_success')
                );
            } catch (ForeignKeyConstraintViolationException $e) {
                $container->get('session')->getFlashBag()->add(
                    'danger',
                    $container->get('translator')->trans('danger_alerts.delete_fail')
                );
            }
        }
    }
}
