<?php

namespace Pinox\DashboardBundle\Controller\Traits;


trait CurlRequestTrait
{
    /**
     * request with POST method
     * @param $url string
     * @param array $post
     * @param array $headers
     * @param array $files
     * @return mixed
     */
    public function curlPost($url, $post = [], $headers = [], $files = []) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        return $this->curlRequest($ch, $url, $post, $headers, $files);
    }

    /**
     * * request with GET method
     * @param $url string
     * @param array $post
     * @param array $headers
     * @param array $files
     * @return mixed
     */
    public function curlGet($url, $post = [], $headers = [], $files = []) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        if ($post) {
            $url .= '?' . http_build_query($post);
        }
        return $this->curlRequest($ch, $url, $post, $headers, $files);
    }

    /**
     * @param $ch
     * @param $url string
     * @param array $post
     * @param array $headers
     * @param array $files
     * @return mixed
     */
    private function curlRequest($ch, $url, $post = [], $headers = [], $files = [])
    {
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        if (is_array($post) || is_object($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if ($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if ($files) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $files);
        }
        $request = curl_exec($ch);
        curl_close($ch);
        return $request;
    }

}