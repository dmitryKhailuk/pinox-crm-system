<?php

namespace Pinox\DashboardBundle\Controller;

use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\BookkeepingBundle\Entity\Type;
use Pinox\BookkeepingBundle\Services\ConverterCurrencyYahoo;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DashboardMainController extends Controller
{
    use GettingExchangeRatesAndAddToDb;

    /**
     * @Route("/", name="dashboard")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $totalInvestment = 0;
        $totalBills = 0;
        $totalRevenue = 0;
        $totalBalance = 0;
        $totalCash = 0;

        $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');

        $balances = $em->getRepository('PinoxAffiseBundle:Affiliate')->getBalanceActiveAffiliates();
        $investments = $em->getRepository('PinoxBookkeepingBundle:Debenture')->totalPaymentsGroupByCurrency();
        $bills = $em->getRepository('PinoxBookkeepingBundle:Bill')->getTotalSumBillsFromCurrency();
        $revenues = $em->getRepository('PinoxBookkeepingBundle:Revenue')->getTotalSumRevenuesFromCurrency();
        $cash = $em->getRepository('PinoxDashboardBundle:AvailabilityCash')->getTotalSumCashFromCurrency();

        $usdCurrency = $em->getRepository('PinoxBookkeepingBundle:Currency')->findOneBy(['code' => Currency::CODE_USD]);

        $rates['rate'] = $this->gettingExchangeRatesAndAddToDb($usdCurrency, $em);
        $rates['currencyName'] = $usdCurrency->getName();
        $rates['currencyCode'] = $usdCurrency->getCode();

        if (is_array($rates) && count($rates) > 0) {
            $balanceForConverter = [];
            foreach ($balances as $balance) {
                foreach ($balance as $item) {
                    foreach ($item as $key => $value) {
                        if ($value['balance'] > 0) {
                            $balanceForConverter[] = [
                                'payment' => $value['balance'],
                                'code' => $key,
                                'currency' => ''
                            ];
                        }
                    }
                }
            }

            $totalInvestment = $this->converterCurrencyAndGetTotalSum($converter, $investments, $rates);
            $totalBills = $this->converterCurrencyAndGetTotalSum($converter, $bills, $rates);
            $totalBalance = $this->converterCurrencyAndGetTotalSum($converter, $balanceForConverter, $rates);
            $totalCash = $this->converterCurrencyAndGetTotalSum($converter, $cash, $rates);
            $total = $converter->converterCurrency($revenues, $rates);

            foreach ($total as $item) {
                if ($item['type'] === Type::INCOMING) {
                    $totalRevenue += $item['payment'];
                } else {
                    $totalRevenue -= $item['payment'];
                }

            }

        }

        return $this->render('PinoxDashboardBundle:DashboardMain:index.html.twig', [
            'totalInvestment' => $totalInvestment,
            'totalBills' => $totalBills,
            'totalRevenue' => $totalRevenue,
            'totalBalance' => $totalBalance,
            'totalCash' => $totalCash,
        ]);
    }

    /**
     * @param ConverterCurrencyYahoo $converter
     * @param array $items
     * @param array $rates
     * @return int
     */
    private function converterCurrencyAndGetTotalSum($converter, array $items = [], array $rates)
    {
        $sum = 0;

        if (count($items) > 0) {
            $total = $converter->converterCurrency($items, $rates);
            foreach ($total as $item) {
                $sum += $item['payment'];
            }
        }
        return $sum;
    }
}
