<?php

namespace Pinox\DashboardBundle\Controller;

use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Pinox\DashboardBundle\Entity\AvailabilityCash;
use Pinox\DashboardBundle\Form\AvailabilityCashType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/cash")
 * @Security("has_role('ROLE_ADMIN')")
 */
class AvailabilityCashController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="cash_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxDashboardBundle:AvailabilityCash')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $page,
            AvailabilityCash::CASH_PER_PAGES
        );

        return $this->render('PinoxDashboardBundle:AvailabilityCash:index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="cash_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $availabilityCash = new AvailabilityCash();
        $form = $this->createForm(AvailabilityCashType::class, $availabilityCash);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($availabilityCash);
            $em->flush();

            return $this->redirect($this->generateUrl('cash_index'));
        }

        return $this->render('PinoxDashboardBundle:AvailabilityCash:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="cash_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("availabilityCash", class="PinoxDashboardBundle:AvailabilityCash")
     * @param Request $request
     * @param AvailabilityCash $availabilityCash
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, AvailabilityCash $availabilityCash)
    {
        $form = $this->createForm(AvailabilityCashType::class, $availabilityCash, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($availabilityCash);
            $em->flush();

            return $this->redirect($this->generateUrl('cash_index'));
        }

        return $this->render('PinoxDashboardBundle:AvailabilityCash:form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="cash_delete")
     * @Method("DELETE")
     * @ParamConverter("availabilityCash", class="PinoxDashboardBundle:AvailabilityCash")
     * @param Request $request
     * @param AvailabilityCash $availabilityCash
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, AvailabilityCash $availabilityCash)
    {
        $this->processEntityRemoval(
            $availabilityCash,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('cash_index'));
    }
}
