<?php

namespace Pinox\BookkeepingBundle\Services;

use Doctrine\ORM\EntityManager;
use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\DashboardBundle\Controller\Traits\CurlRequestTrait;

class ConverterCurrencyYahoo
{
    use CurlRequestTrait;

    const URL = 'https://query.yahooapis.com/v1/public/yql';
    const EXTRA_PARAMETERS_URL = '&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys';

    /**  @var ConverterCurrencyYahoo */
    public static $instance = null;

    /** @var  EntityManager $em */
    private $em;

    /** @var Currency[] */
    private $currencies = [];

    /**
     * ConverterCurrencyYahoo constructor.
     * @param EntityManager $em
     */
    private function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    private function __clone()
    {
    }

    /**
     * @param EntityManager $em
     * @return ConverterCurrencyYahoo
     */
    public static function getInstance(EntityManager $em)
    {
        if (self::$instance === null) {
            self::$instance = new ConverterCurrencyYahoo($em);
        }

        return self::$instance;
    }

    /**
     * @return array|Currency[]
     */
    public function getCurrencies()
    {
        if (count($this->currencies) <= 0 && empty($this->currencies)) {
            $this->currencies = $this->em->getRepository('PinoxBookkeepingBundle:Currency')->findAll();
        }

        return $this->currencies;
    }

    /**
     * @param Currency $currency
     * @param array $bills
     * @return array
     */
    public function getConverterBillsByCurrency(Currency $currency, array $bills)
    {
        $billsConverter = [];
        $code = $currency->getCode();
        $currency = $currency->getName();
        $currencyRates = $this->getCurrencyRateXchange($code);

        foreach ($bills as $bill) {
            $keyCurrency = $bill['code']. $code;
            if(array_key_exists($bill['code'] . $code, $currencyRates)) {

                if (!array_key_exists($bill['date'], $billsConverter)) {
                    $billsConverter[$bill['date']] = [
                        'date' => $bill['date'],
                        'currency' => $currency,
                        'code' => $bill['code'],
                        'positives_sum' => round($bill['positives_sum'] * $currencyRates[$keyCurrency]['Rate'] , 2),
                        'negative_sum' => round($bill['negative_sum'] * $currencyRates[$keyCurrency]['Rate'] , 2),
                    ];
                } else {
                    $billsConverter[$bill['date']]['positives_sum'] +=
                        round($bill['positives_sum'] * $currencyRates[$keyCurrency]['Rate'] , 2);

                    $billsConverter[$bill['date']]['negative_sum'] +=
                        round($bill['negative_sum'] * $currencyRates[$keyCurrency]['Rate'] , 2);
                }

            }
        }

        return $billsConverter;
    }

    /**
     * @param string $currencyCode
     * @return array
     */
    public function getCurrencyRateXchange($currencyCode)
    {
        $currencies = $this->getCurrencies();
        $currencyRateXchanges = [];
        $selectRate = '';
        if (count($currencies) > 0 ) {
            $selectRate = 'select * from yahoo.finance.xchange where pair in ';
            $selectRate .= '(';
            foreach ($currencies as $key => $currency) {
                if ($key !== 0) {
                    $selectRate .= ' , ';
                }
                $selectRate .= '"' . $currency->getCode() . $currencyCode .'"';
            }
            $selectRate .= ')';
        }

        $rates = $this->curlGet(self::URL . '?q=' . urlencode($selectRate) . '&' . self::EXTRA_PARAMETERS_URL);
        $currencyRates = json_decode($rates, true);

        if (array_key_exists('query', $currencyRates) && array_key_exists('results', $currencyRates['query'])) {
            foreach ($currencyRates['query']['results']['rate'] as $rate) {
                $currencyRateXchanges[$rate['id']] = $rate;
            }
        }

        return $currencyRateXchanges;
    }

    /**
     * @param array $items
     * @param array $rates
     * @return array
     */
    public function converterCurrency(array $items, array $rates)
    {
        $code = $this->getCurrencyCode($rates);
        foreach ($items as $key => $item) {
            $keyCurrency = $item['code']. $code;
            if(array_key_exists($keyCurrency, $rates['rate'])) {
                $items[$key]['payment'] = round($item['payment'] * $rates['rate'][$keyCurrency]['rate'] , 2);
                $items[$key]['currency'] = $rates['currencyName'];
            }
        }

        return $items;
    }

    /**
     * @param array $rates
     * @return string
     */
    private function getCurrencyCode(array $rates)
    {
        if (array_key_exists('currencyCode', $rates)) {
            return $rates['currencyCode'];
        }

        return '';
    }

}
