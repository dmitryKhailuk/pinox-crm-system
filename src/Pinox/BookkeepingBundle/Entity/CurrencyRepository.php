<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CurrencyRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingCurrenciesByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('t')
            ->where('lower(t.name) = lower(:name)')
            ->setParameter('name', $criteria['name'])
            ->getQuery()
            ->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC');
    }

}
