<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class RegularPaymentRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getForPagination()
    {
        return $this->createQueryBuilder('rp')
            ->select('rp.id, rp.name, rp.payment, rp.datePayment, c.name AS currency, c.code, pp.name AS periodicity, 
                t.name AS type')
            ->innerJoin('rp.currency', 'c')
            ->innerJoin('rp.periodicityPayment', 'pp')
            ->innerJoin('rp.type', 't')
            ->orderBy('rp.createdAt', 'ASC')
            ->getQuery()->getArrayResult();
    }
}
