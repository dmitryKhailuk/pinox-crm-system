<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ExchangeRateRepository")
 * @ORM\Table(
 *     name="exchange_rates",
 *     schema="pinox_bookkeeping",
 *     indexes={
 *          @ORM\Index(name="exchange_rates_code_unique_idx", columns={"code"}),
 *          @ORM\Index(name="exchange_rates_code_rate_unique_idx", columns={"code_rate"}),
 *      }
 * )
 */
class ExchangeRate
{
    use TimestampableEntity;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="code_rate", type="string", length=7, nullable=false)
     * @Assert\NotBlank()
     * @var string
     */
    private $codeRate;

    /**
     * @ORM\Column(name="rate", type="decimal", precision=10, scale=4, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $rate = 0;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Currency")
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeRate
     *
     * @param string $codeRate
     *
     * @return ExchangeRate
     */
    public function setCodeRate($codeRate)
    {
        $this->codeRate = $codeRate;

        return $this;
    }

    /**
     * Get codeRate
     *
     * @return string
     */
    public function getCodeRate()
    {
        return $this->codeRate;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return ExchangeRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set currency
     *
     * @param \Pinox\BookkeepingBundle\Entity\Currency $currency
     *
     * @return ExchangeRate
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Pinox\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
