<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ExchangeRateRepository extends EntityRepository
{
    /**
     * @param integer $currencyId
     * @return ExchangeRate[]
     */
    public function findExchangeRatesByCurrencyFoNowDate($currencyId)
    {
        $em = $this->getEntityManager();
        $sql = "SELECT er.id, er.code_rate, er.rate, er.currency_id 
                FROM pinox_bookkeeping.exchange_rates AS er
                INNER JOIN pinox_bookkeeping.currency AS c ON c.id = er.currency_id
                WHERE er.currency_id = :currency
                AND date(er.created_at) = :date
        ";

        $statement = $em->getConnection()->prepare($sql);
        $statement->execute(['currency' => $currencyId, 'date' => date('Y-m-d')]);
        return $statement->fetchAll();
    }

    /**
     * @param \DateTime $dateTo
     * @return array
     */
    public function getOldExchangeRates(\DateTime $dateTo)
    {
        return $this->createQueryBuilder('er')
            ->where('er.createdAt < :date')
            ->setParameter('date', $dateTo->format('Y-m-d'))
            ->getQuery()->getResult();
    }
}
