<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class DebentureRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getForPagination()
    {
        return $this->createQueryBuilder('d')
            ->select('d.id, d.name, d.payment, d.datePayment, c.name as currency, c.code')
            ->innerJoin('d.currency', 'c')
            ->orderBy('d.datePayment', 'DESC')
            ->getQuery()->getArrayResult();
    }

    /**
     * @return array
     */
    public function totalPaymentsGroupByNameAndCurrency()
    {
        return $this->createQueryBuilder('d')
            ->select('d.name, SUM(d.payment) as payment, c.name as currency, c.code')
            ->join('d.currency', 'c')
            ->groupBy('d.name', 'c.id')
            ->orderBy('d.name', 'ASC')
            ->getQuery()->getArrayResult();
    }

    /**
     * @return array
     */
    public function totalPaymentsGroupByCurrency()
    {
        return $this->createQueryBuilder('d')
            ->select('SUM(d.payment) as payment, c.name as currency, c.code')
            ->join('d.currency', 'c')
            ->groupBy('c.id')
            ->orderBy('c.name', 'ASC')
            ->getQuery()->getArrayResult();
    }
}
