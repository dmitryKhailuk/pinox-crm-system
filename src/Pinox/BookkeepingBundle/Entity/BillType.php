<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="BillTypeRepository")
 * @ORM\Table(
 *     name="bills_type",
 *     schema="pinox_bookkeeping",
 *     indexes={
 *          @ORM\Index(name="bills_type_name_type_unique_idx", columns={"name", "type_id"})
 *      }
 * )
 * @UniqueEntity(
 *     fields={"name", "type"},
 *     groups={"Default"},
 *     errorPath="name",
 *     repositoryMethod="findInterferingBillsTypeByCriteria",
 *     message="bill_type.unique_error"
 * )
 */
class BillType
{
    use TimestampableEntity;

    const BILLS_TYPE_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="20")
     * @var string
     */
    private $name;

    /**
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Type",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Type
     */
    private $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BillType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param \Pinox\BookkeepingBundle\Entity\Type $type
     *
     * @return BillType
     */
    public function setType(Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Pinox\BookkeepingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }
}
