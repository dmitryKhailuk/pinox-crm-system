<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TypeRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @return mixed
     */
    public function findInterferingTypesByCriteria(array $criteria)
    {
        return $this->createQueryBuilder('t')
            ->where('lower(t.name) = lower(:name)')
            ->setParameter('name', $criteria['name'])
            ->getQuery()
            ->execute();
    }

}
