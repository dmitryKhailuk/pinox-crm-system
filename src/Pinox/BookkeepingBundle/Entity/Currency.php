<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="CurrencyRepository")
 * @ORM\Table(
 *     name="currency",
 *     schema="pinox_bookkeeping",
 *     indexes={
 *          @ORM\Index(name="currency_name_unique_idx", columns={"name"})
 *      }
 * )
 * @UniqueEntity(
 *     "name",
 *     repositoryMethod="findInterferingCurrenciesByCriteria",
 *     message="currency.unique_error"
 * )
 */
class Currency
{
    const CURRENCIES_PER_PAGE = 25;
    const CODE_USD = 'USD';
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="20")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="code", type="string", length=4, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="4")
     * @var string
     */
    private $code;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Currency
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}
