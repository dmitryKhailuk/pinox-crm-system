<?php

namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\EntityRepository;

class BillRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder()
    {
        return $this->createQueryBuilder('b')
            ->select('b.id, b.name, b.payment, b.comment, b.createdAt, c.name AS currency, t.name AS type, c.code,
                bt.name AS billType, pt.name AS paymentType, b.dateOperation, b.hashtag')
            ->leftJoin('b.currency', 'c')
            ->leftJoin('b.billType', 'bt')
            ->leftJoin('b.type', 't')
            ->leftJoin('b.paymentType', 'pt')
            ->orderBy('b.createdAt', 'DESC');
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilderRevenue()
    {
        return $this->getListQueryBuilder()
            ->andWhere('t.id = :type')
            ->setParameter('type', Type::INCOMING);
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilderCost()
    {
        return $this->getListQueryBuilder()
            ->andWhere('t.id = :type')
            ->setParameter('type', Type::LOSS);
    }

    /**
     * @return array
     */
    public function getTotalSumBillsFromCurrency()
    {
        return $this->createQueryBuilder('b')
            ->select('SUM(b.payment) as payment, c.name as currency, c.code')
            ->innerJoin('b.type', 't')
            ->innerJoin('b.currency', 'c')
            ->andWhere('t.id = :type')
            ->setParameter('type', Type::LOSS)
            ->groupBy('c.code', 'c.name')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();
    }

}
