<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="TypeRepository")
 * @ORM\Table(
 *     name="types",
 *     schema="pinox_bookkeeping",
 *     indexes={
 *          @ORM\Index(name="types_name_unique_idx", columns={"name"})
 *      }
 * )
 * @UniqueEntity(
 *     "name",
 *     repositoryMethod="findInterferingTypesByCriteria",
 *     message="type.unique_error"
 * )
 */
class Type
{
    const INCOMING = 2;
    const LOSS = 1;
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="20")
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
