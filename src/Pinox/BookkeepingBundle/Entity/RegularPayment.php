<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="RegularPaymentRepository")
 * @ORM\Table(
 *     name="regular_payments",
 *     schema="pinox_bookkeeping",
 *     indexes={
 *          @ORM\Index(name="regular_payments_name_unique_idx", columns={"name"})
 *      }
 * )
 */
class RegularPayment
{
    use TimestampableEntity;

    const REGULAR_PAYMENT_PER_PAGES = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="20")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="payment", type="decimal", precision=15, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payment = 0;

    /**
     * @ORM\Column(name="date_payment", type="date", nullable=false)
     * @Assert\Date()
     * @var \DateTime
     */
    private $datePayment;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Currency",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;

    /**
     * @ORM\JoinColumn(name="periodicity_payment_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="PeriodicityPayment",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var PeriodicityPayment
     */
    private $periodicityPayment;

    /**
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Type",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Type
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return RegularPayment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return RegularPayment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set datePayment
     *
     * @param \DateTime $datePayment
     *
     * @return RegularPayment
     */
    public function setDatePayment($datePayment)
    {
        $this->datePayment = $datePayment;

        return $this;
    }

    /**
     * Get datePayment
     *
     * @return \DateTime
     */
    public function getDatePayment()
    {
        return $this->datePayment;
    }

    /**
     * Set currency
     *
     * @param \Pinox\BookkeepingBundle\Entity\Currency $currency
     *
     * @return RegularPayment
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Pinox\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set periodicityPayment
     *
     * @param \Pinox\BookkeepingBundle\Entity\PeriodicityPayment $periodicityPayment
     *
     * @return RegularPayment
     */
    public function setPeriodicityPayment(PeriodicityPayment $periodicityPayment = null)
    {
        $this->periodicityPayment = $periodicityPayment;

        return $this;
    }

    /**
     * Get periodicityPayment
     *
     * @return \Pinox\BookkeepingBundle\Entity\PeriodicityPayment
     */
    public function getPeriodicityPayment()
    {
        return $this->periodicityPayment;
    }

    /**
     * Set type
     *
     * @param \Pinox\BookkeepingBundle\Entity\Type $type
     *
     * @return RegularPayment
     */
    public function setType(Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Pinox\BookkeepingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }
}
