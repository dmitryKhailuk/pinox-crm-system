<?php
namespace Pinox\BookkeepingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="RevenueRepository")
 * @ORM\Table(
 *     name="revenues",
 *     schema="pinox_bookkeeping"
 * )
 */
class Revenue
{
    use TimestampableEntity;

    const REVENUES_PER_PAGE = 25;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="payment", type="decimal", precision=15, scale=2, nullable=false)
     * @Assert\GreaterThanOrEqual(value=0)
     * @var float
     */
    private $payment = 0;

    /**
     * @ORM\Column(name="comment", type="string", length=2000)
     * @Assert\Length(max=2000)
     */
    private $comment;

    /**
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Currency",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Currency
     */
    private $currency;

    /**
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="Type",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var Type
     */
    private $type;

    /**
     * @ORM\JoinColumn(name="bill_type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="BillType",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var BillType
     */
    private $billType;

    /**
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", nullable=false)
     * @ORM\ManyToOne(targetEntity="PaymentType",
     *     cascade={"persist"})
     * @Assert\NotBlank()
     * @var PaymentType
     */
    private $paymentType;

    /**
     * @ORM\Column(name="date_operation", type="date", nullable=true)
     * @Assert\Date()
     * @var \DateTime
     */
    private $dateOperation;

    /**
     * @ORM\Column(name="hashtag", type="string", length=250, nullable=true)
     * @Assert\Length(max="250")
     * @var string
     */
    private $hashtag;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Revenue
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return Revenue
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Revenue
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set dateOperation
     *
     * @param \DateTime $dateOperation
     *
     * @return Revenue
     */
    public function setDateOperation($dateOperation)
    {

        if ($dateOperation) {
            $this->dateOperation = $dateOperation;
        } else {
            $this->dateOperation = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get dateOperation
     *
     * @return \DateTime
     */
    public function getDateOperation()
    {
        return $this->dateOperation;
    }

    /**
     * Set hashtag
     *
     * @param string $hashtag
     *
     * @return Revenue
     */
    public function setHashtag($hashtag)
    {
        $this->hashtag = $hashtag;

        return $this;
    }

    /**
     * Get hashtag
     *
     * @return string
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }

    /**
     * Set currency
     *
     * @param \Pinox\BookkeepingBundle\Entity\Currency $currency
     *
     * @return Revenue
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return \Pinox\BookkeepingBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set type
     *
     * @param \Pinox\BookkeepingBundle\Entity\Type $type
     *
     * @return Revenue
     */
    public function setType(Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Pinox\BookkeepingBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set billType
     *
     * @param \Pinox\BookkeepingBundle\Entity\BillType $billType
     *
     * @return Revenue
     */
    public function setBillType(BillType $billType = null)
    {
        $this->billType = $billType;

        return $this;
    }

    /**
     * Get billType
     *
     * @return \Pinox\BookkeepingBundle\Entity\BillType
     */
    public function getBillType()
    {
        return $this->billType;
    }

    /**
     * Set paymentType
     *
     * @param \Pinox\BookkeepingBundle\Entity\PaymentType $paymentType
     *
     * @return Revenue
     */
    public function setPaymentType(PaymentType $paymentType = null)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return \Pinox\BookkeepingBundle\Entity\PaymentType
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
}
