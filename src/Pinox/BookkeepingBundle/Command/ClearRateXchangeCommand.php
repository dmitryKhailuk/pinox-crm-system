<?php

namespace Pinox\BookkeepingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearRateXchangeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ratexchange:remove')
            ->setDescription('');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime('-7 days');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $rates = $em->getRepository('PinoxBookkeepingBundle:ExchangeRate')->getOldExchangeRates($date);
        foreach ($rates as $rate) {
            $em->remove($rate);
        }
        $em->flush();

    }
}