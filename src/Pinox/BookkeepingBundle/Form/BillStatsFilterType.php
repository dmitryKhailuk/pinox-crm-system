<?php

namespace Pinox\BookkeepingBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillStatsFilterType extends AbstractType
{
    const FIRST_DATE_THIS_YEAR = '-01-01';
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateFrom', DateType::class, [
            'label' => 'bill.date_from',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'data' => new \DateTime(date('Y') . self::FIRST_DATE_THIS_YEAR),
            'label_attr' => [ 'class' => 'filter-label' ],
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('dateTo', DateType::class, [
            'label' => 'bill.date_to',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'data' => new \DateTime('now'),
            'label_attr' => [ 'class' => 'filter-label' ],
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('currency', EntityType::class, [
            'label'    => 'bill.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method'            => 'GET',
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_bookkeeping_bill_stats_filter';
    }
}