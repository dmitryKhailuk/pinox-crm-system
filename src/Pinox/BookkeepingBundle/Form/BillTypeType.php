<?php

namespace Pinox\BookkeepingBundle\Form;


use Pinox\BookkeepingBundle\Entity\BillType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'bill_type.name',
            'required' => true,
            'attr' => [
                'maxlength' => 20
            ]
        ]);

        $builder->add('type', EntityType::class, [
            'label'    => 'bill_type.type',
            'class' => 'PinoxBookkeepingBundle:Type',
            'choice_label' => 'name',
            'required' => false,
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BillType::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_bookkeeping_bills_type';
    }
}