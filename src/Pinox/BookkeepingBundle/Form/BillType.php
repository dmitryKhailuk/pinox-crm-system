<?php

namespace Pinox\BookkeepingBundle\Form;


use Pinox\BookkeepingBundle\Entity\Bill;
use Pinox\BookkeepingBundle\Entity\BillTypeRepository;
use Pinox\BookkeepingBundle\Entity\CurrencyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'bill.name',
            'required' => true,
            'attr' => [
                'maxlength' => 100
            ]
        ]);

        $builder->add('billType', EntityType::class, [
            'label'    => 'bill.bill_type',
            'class' => 'PinoxBookkeepingBundle:BillType',
            'choice_label' => 'name',
            'required' => false,
            'query_builder' => function (BillTypeRepository $repository) {
                return $repository->getListCostQueryBuilder();
            }
        ]);

        $builder->add('payment', TextType::class, [
            'label' => 'bill.payment',
            'required' => true,
        ]);

        $builder->add('currency', EntityType::class, [
            'label' => 'bill.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
            'query_builder' => function (CurrencyRepository $repository) {
                return $repository->getListQueryBuilder();
            }
        ]);

        $builder->add('paymentType', EntityType::class, [
            'label' => 'bill.payment_type',
            'class' => 'PinoxBookkeepingBundle:PaymentType',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('dateOperation', DateType::class, [
            'label' => 'bill.date_operation',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

        $builder->add('hashtag', TextType::class, [
            'label' => 'bill.hashtag',
            'required' => false,
        ]);

        $builder->add('comment', TextareaType::class, [
            'label' => 'bill.comment',
            'required' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bill::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_bookkeeping_bills';
    }
}