<?php

namespace Pinox\BookkeepingBundle\Form;


use Pinox\BookkeepingBundle\Entity\Currency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CurrencyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'currency.name',
            'required' => true,
            'attr' => [
                'maxlength' => 20
            ]
        ]);

        $builder->add('code', TextType::class, [
            'label' => 'currency.code',
            'required' => true,
            'attr' => [
                'maxlength' => 4
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Currency::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_bookkeeping_currency';
    }
}