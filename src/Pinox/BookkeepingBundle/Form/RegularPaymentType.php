<?php

namespace Pinox\BookkeepingBundle\Form;

use Pinox\BookkeepingBundle\Entity\RegularPayment;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegularPaymentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', EntityType::class, [
            'label'    => 'regular_payment.type',
            'class' => 'PinoxBookkeepingBundle:Type',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('name', TextType::class, [
            'label' => 'regular_payment.name',
            'required' => true,
            'attr' => [
                'maxlength' => 20
            ]
        ]);

        $builder->add('payment', TextType::class, [
            'label' => 'regular_payment.payment',
            'required' => true,
        ]);

        $builder->add('currency', EntityType::class, [
            'label'    => 'regular_payment.currency',
            'class' => 'PinoxBookkeepingBundle:Currency',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('periodicityPayment', EntityType::class, [
            'label'    => 'regular_payment.periodicity_payment',
            'class' => 'PinoxBookkeepingBundle:PeriodicityPayment',
            'choice_label' => 'name',
            'required' => false,
        ]);

        $builder->add('datePayment', DateType::class, [
            'label' => 'regular_payment.date_payment',
            'html5' => false,
            'required'=> false,
            'widget' => 'single_text',
            'format' => 'dd.MM.yyyy',
            'attr' => [
                'placeholder' => 'common.date_calendar',
                'data-show-as' => 'datepicker',
            ]
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RegularPayment::class,
        ]);

    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_bookkeeping_regular_payment';
    }
}