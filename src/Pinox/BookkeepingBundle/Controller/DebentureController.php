<?php

namespace Pinox\BookkeepingBundle\Controller;

use Pinox\BookkeepingBundle\Entity\Debenture;
use Pinox\BookkeepingBundle\Form\DebentureType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/debentures")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class DebentureController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="debenture_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        $debentures = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:Debenture')
            ->getForPagination();

        if ($request->getSession()->get('rates')) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $debentures = $converter->converterCurrency($debentures, $request->getSession()->get('rates'));
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $debentures,
            $page,
            Debenture::DEBENTURES_PER_PAGE
        );

        return $this->render('@PinoxBookkeeping/Debenture/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="debenture_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $debenture = new Debenture();
        $form = $this->createForm(DebentureType::class, $debenture);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($debenture);
            $em->flush();

            return $this->redirect($this->generateUrl('debenture_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/Debenture/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="debenture_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("debenture", class="PinoxBookkeepingBundle:Debenture")
     * @param Request $request
     * @param Debenture $debenture
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Debenture $debenture)
    {
        $form = $this->createForm(DebentureType::class, $debenture, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($debenture);
            $em->flush();

            return $this->redirect($this->generateUrl('debenture_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/Debenture/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="debenture_delete")
     * @Method("DELETE")
     * @ParamConverter("debenture", class="PinoxBookkeepingBundle:Debenture")
     * @param Request $request
     * @param Debenture $debenture
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Debenture $debenture)
    {
        $this->processEntityRemoval(
            $debenture,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('debenture_index'));
    }

    /**
     * @Route("/total_payments", name="debenture_view_total_payment")
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function viewTotalPaymentsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $totalPayments = $em->getRepository('PinoxBookkeepingBundle:Debenture')
            ->totalPaymentsGroupByNameAndCurrency();

        if ($request->getSession()->get('rates')) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $totalPayments = $converter->converterCurrency($totalPayments, $request->getSession()->get('rates'));

        }

        $paymentsGroupCurrency = $em->getRepository('PinoxBookkeepingBundle:Debenture')
            ->totalPaymentsGroupByCurrency();

        return $this->render('@PinoxBookkeeping/Debenture/totalPayment.html.twig', [
            'payments' => $totalPayments,
            'paymentsGroupCurrency' => $paymentsGroupCurrency,
        ]);
    }
}
