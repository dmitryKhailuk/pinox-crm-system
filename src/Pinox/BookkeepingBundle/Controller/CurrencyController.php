<?php

namespace Pinox\BookkeepingBundle\Controller;

use Pinox\BookkeepingBundle\Entity\Currency;
use Pinox\BookkeepingBundle\Entity\ExchangeRate;
use Pinox\BookkeepingBundle\Form\CurrencyType;
use Pinox\DashboardBundle\Controller\Traits\GettingExchangeRatesAndAddToDb;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/currencies")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class CurrencyController extends Controller
{
    use ProcessesEntityRemovalTrait, GettingExchangeRatesAndAddToDb;

    /**
     * @Route("/{page}", name="currency_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:Currency')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            Currency::CURRENCIES_PER_PAGE
        );

        return $this->render('@PinoxBookkeeping/Currency/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="currency_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $currency = new Currency();
        $form = $this->createForm(CurrencyType::class, $currency);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($currency);
            $em->flush();

            return $this->redirect($this->generateUrl('currency_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/Currency/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="currency_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("currency", class="PinoxBookkeepingBundle:Currency")
     * @param Request $request
     * @param Currency $currency
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Currency $currency)
    {
        $form = $this->createForm(CurrencyType::class, $currency, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($currency);
            $em->flush();

            return $this->redirect($this->generateUrl('currency_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/Currency/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="currency_delete")
     * @Method("DELETE")
     * @ParamConverter("currency", class="PinoxBookkeepingBundle:Currency")
     * @param Request $request
     * @param Currency $currency
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Currency $currency)
    {
        $this->processEntityRemoval(
            $currency,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('currency_index'));
    }

    /**
     * @Route("/lock/{id}", name="currency_lock")
     * @Method({"GET", "PUT"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @ParamConverter("currency", class="PinoxBookkeepingBundle:Currency")
     * @param Currency $currency
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function lockAction(Currency $currency)
    {
        $session = new Session();
        $exchangeRates = ['currencyCode' => $currency->getCode(), 'currencyName' => $currency->getName()];

        $em = $this->getDoctrine()->getManager();
        $rates = $this->gettingExchangeRatesAndAddToDb($currency, $em);

        $exchangeRates['rate'] = $rates;
        $session->set('rates', $exchangeRates);
        return $this->redirect($this->generateUrl('currency_index'));
    }

    /**
     * @Route("/delete_rate_currency", name="currency_delete_rate_currency")
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteRateCurrencyAction(Request $request)
    {
        $message = 'bad';
        if ($request->getSession()->get('rates')) {
            $request->getSession()->remove('rates');
            $message = 'ok';
        }

        return new JsonResponse($message);
    }
}
