<?php

namespace Pinox\BookkeepingBundle\Controller;

use Pinox\BookkeepingBundle\Controller\Traits\BillsByThisYearTrait;
use Pinox\BookkeepingBundle\Entity\PaymentType;
use Pinox\BookkeepingBundle\Entity\Revenue;
use Pinox\BookkeepingBundle\Entity\Type;
use Pinox\BookkeepingBundle\Form\BillFilterType;
use Pinox\BookkeepingBundle\Form\BillStatsFilterType;
use Pinox\BookkeepingBundle\Form\RevenueType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\QueryBuilder;
/**
 * @Route("/revenues")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class RevenueController extends Controller
{

    use ProcessesEntityRemovalTrait, BillsByThisYearTrait;

    /**
     * @Route("/{page}", name="revenue_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        $rates = $request->getSession()->get('rates');

        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:Revenue')
            ->getListQueryBuilder();

        $form = $this->createForm(BillFilterType::class, null, [
            BillFilterType::REVENUE_FILTER => true,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater $filterBuilder */
            $filterBuilder = $this->get('lexik_form_filter.query_builder_updater');
            $queryBuilder = $filterBuilder->addFilterConditions($form, $queryBuilder);
        }

        $revenues = $queryBuilder->getQuery()->getArrayResult();

        list($sum, $totalSum) = $this->getTotalSum($queryBuilder, $rates);

        if ($rates) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $revenues = $converter->converterCurrency($revenues, $rates);
        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $revenues,
            $page,
            Revenue::REVENUES_PER_PAGE
        );

        return $this->render('@PinoxBookkeeping/Revenue/index.html.twig', [
            'pagination' => $pagination,
            'filter' => $form->createView(),
            'totalSum' => $totalSum,
            'sum' => $sum
        ]);
    }

    /**
     * @Route("/new", name="revenue_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {

        $revenue = new Revenue();
        $form = $this->createForm(RevenueType::class, $revenue);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($revenue);
            if ($paymentWithWebMoneyCommission) {
                $revenue->setPayment($paymentWithWebMoneyCommission);
            }

            $em->persist($revenue);
            $em->flush();

            return $this->redirect($this->generateUrl('revenue_index'));
        }

        return $this->render('@PinoxBookkeeping/Revenue/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'new',
        ]);
    }

    /**
     * @Route("/edit/{id}", name="revenue_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("revenue", class="PinoxBookkeepingBundle:Revenue")
     * @param Request $request
     * @param Revenue $revenue
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, Revenue $revenue)
    {
        $form = $this->createForm(RevenueType::class, $revenue, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $paymentWithWebMoneyCommission = $this->getWebMoneyPayment($revenue);
            if ($paymentWithWebMoneyCommission) {
                $revenue->setPayment($paymentWithWebMoneyCommission);
            }
            $em->persist($revenue);
            $em->flush();

            return $this->redirect($this->generateUrl('revenue_index'));
        }

        return $this->render('@PinoxBookkeeping/Revenue/form.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
        ]);
    }

    /**
     * @Route("/delete/{id}", name="revenue_delete")
     * @Method("DELETE")
     * @ParamConverter("revenue", class="PinoxBookkeepingBundle:Revenue")
     * @param Request $request
     * @param Revenue $revenue
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, Revenue $revenue)
    {
        $this->processEntityRemoval(
            $revenue,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('revenue_index'));
    }

    /**
     * @Route("/revenues_by_month", name="revenue_bills_by_month")
     * @Method({"POST", "GET"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function revenuesByMonthAction(Request $request)
    {
        $dateTo = new \DateTime('now');
        $dateFrom = new \DateTime(date('Y') . BillStatsFilterType::FIRST_DATE_THIS_YEAR);
        $rates = $request->getSession()->get('rates');
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:Revenue')
            ->getListQueryBuilder();

        $tableName = $em->getClassMetadata(Revenue::class)->getSchemaName() . '.'
            . $em->getClassMetadata(Revenue::class)->getTableName();

        $form = $this->createForm(BillStatsFilterType::class);
        $form->handleRequest($request);

        $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);
        list($sum, $totalSum) = $this->getTotalSum($queryBuilder, $rates);

        if ($form->isSubmitted() && $form->isValid()) {
            $dateFrom = $form->get('dateFrom')->getData();
            $dateTo = $form->get('dateTo')->getData();
            $billsByYear = $this->getBillsByThisYear($em, $tableName, $dateFrom, $dateTo);

            if ($form->get('currency')->getData()) {
                $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
                $billsByYear = $converter->getConverterBillsByCurrency(
                    $form->get('currency')->getData(), $billsByYear);
            }
        }

        return $this->render('@PinoxBookkeeping/Bill/billsByMonth.html.twig', [
            'billsByYear' => $billsByYear,
            'filter' => $form->createView(),
            'type' => 'revenues',
            'sum' => $sum,
            'totalSum' => $totalSum,
        ]);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param $rates
     * @return array
     */
    private function getTotalSum(QueryBuilder $queryBuilder, $rates = [])
    {
        $sum = 0;

        $totalSum = $queryBuilder
            ->select('SUM(r.payment) as payment, c.name as currency, c.code, t.id as type')
            ->groupBy('c.code', 'c.name', 't.id')
            ->orderBy('c.code', 'ASC')
            ->getQuery()->getArrayResult();

        if (is_array($rates) && count($rates) > 0) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $total = $converter->converterCurrency($totalSum, $rates);
            foreach ($total as $item) {
                if ($item['type'] === Type::INCOMING) {
                    $sum += $item['payment'];
                } else {
                    $sum -= $item['payment'];
                }

            }
        }
        foreach ($totalSum as $key => $item) {
            foreach ($totalSum as $keyTotal => $sumItem)
                if ($sumItem['type'] === Type::LOSS && $item['code'] === $sumItem['code'] && $key !== $keyTotal) {
                    $totalSum[$key]['payment'] -= $sumItem['payment'];
                    unset($totalSum[$keyTotal]);
                }
        }

        return [$sum, $totalSum];
    }

    /**
     * @param $object
     * @return null|float
     */
    private function getWebMoneyPayment($object)
    {

        if (is_object($object->getPaymentType())
            && $object->getPaymentType()->getId() === PaymentType::PAYMENT_WEBMONEY) {
            return (float) $object->getPayment() + (($object->getPayment() * PaymentType::TAX_WEBMONEY)/100);
        }
        return null;
    }
}
