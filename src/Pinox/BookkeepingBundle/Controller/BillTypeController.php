<?php

namespace Pinox\BookkeepingBundle\Controller;

use Pinox\BookkeepingBundle\Entity\BillType;
use Pinox\BookkeepingBundle\Form\BillTypeType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/bill_types")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class BillTypeController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="bill_type_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction($page)
    {
        /** @var \Doctrine\ORM\QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:BillType')
            ->getListQueryBuilder();

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $page,
            BillType::BILLS_TYPE_PER_PAGE
        );

        return $this->render('@PinoxBookkeeping/BillType/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="bill_type_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $billType = new BillType();
        $form = $this->createForm(BillTypeType::class, $billType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billType);
            $em->flush();

            return $this->redirect($this->generateUrl('bill_type_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/BillType/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="bill_type_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("billType", class="PinoxBookkeepingBundle:BillType")
     * @param Request $request
     * @param BillType $billType
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, BillType $billType)
    {
        $form = $this->createForm(BillTypeType::class, $billType, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($billType);
            $em->flush();

            return $this->redirect($this->generateUrl('bill_type_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/BillType/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="bill_type_delete")
     * @Method("DELETE")
     * @ParamConverter("billType", class="PinoxBookkeepingBundle:BillType")
     * @param Request $request
     * @param BillType $billType
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, BillType $billType)
    {
        $this->processEntityRemoval(
            $billType,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('bill_type_index'));
    }

    /**
     * @Route("/get_bill_types", name="bill_type_get_bill_types")
     * @Method({"POST", "GET"})
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \LogicException
     */
    public function getBillTypesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $typeId = $request->query->get('value');
        $billTypes = $em->getRepository('PinoxBookkeepingBundle:BillType')->findBillTypesByType($typeId);
        return new JsonResponse($billTypes);
    }
}
