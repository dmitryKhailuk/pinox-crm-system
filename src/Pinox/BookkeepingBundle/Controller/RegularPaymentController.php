<?php

namespace Pinox\BookkeepingBundle\Controller;

use Pinox\BookkeepingBundle\Entity\RegularPayment;
use Pinox\BookkeepingBundle\Form\RegularPaymentType;
use Pinox\DashboardBundle\Controller\Traits\ProcessesEntityRemovalTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/regular_payments")
 * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING')")
 */
class RegularPaymentController extends Controller
{
    use ProcessesEntityRemovalTrait;

    /**
     * @Route("/{page}", name="regular_payment_index", defaults={"page": 1}, requirements={"page": "\d+"})
     * @Method("GET")
     * @Security("has_role('ROLE_SHOW_MODULE_BOOKKEEPING') or has_role('ROLE_DEMO_USER')")
     * @param int $page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function indexAction(Request $request, $page)
    {
        $regularPayments = $this->getDoctrine()->getRepository('PinoxBookkeepingBundle:RegularPayment')
            ->getForPagination();

        if ($request->getSession()->get('rates')) {
            $converter = $this->get('pinox_bookkeeping_converter_currency_yahoo');
            $regularPayments = $converter->converterCurrency($regularPayments, $request->getSession()->get('rates'));

        }

        /** @var \Knp\Component\Pager\Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $regularPayments,
            $page,
            RegularPayment::REGULAR_PAYMENT_PER_PAGES
        );

        return $this->render('@PinoxBookkeeping/RegularPayment/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/new", name="regular_payment_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function newAction(Request $request)
    {
        $regularPayment = new RegularPayment();
        $form = $this->createForm(RegularPaymentType::class, $regularPayment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($regularPayment);
            $em->flush();

            return $this->redirect($this->generateUrl('regular_payment_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/RegularPayment/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'new',
            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="regular_payment_edit")
     * @Method({"GET", "PUT"})
     * @ParamConverter("regularPayment", class="PinoxBookkeepingBundle:RegularPayment")
     * @param Request $request
     * @param RegularPayment $regularPayment
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function editAction(Request $request, RegularPayment $regularPayment)
    {
        $form = $this->createForm(RegularPaymentType::class, $regularPayment, [
            'method' => 'PUT',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($regularPayment);
            $em->flush();

            return $this->redirect($this->generateUrl('regular_payment_index'));
        }

        return $this->render(
            '@PinoxBookkeeping/Debenture/form.html.twig', [
                'form'   => $form->createView(),
                'action' => 'edit',
            ]
        );
    }

    /**
     * @Route("/delete/{id}", name="regular_payment_delete")
     * @Method("DELETE")
     * @ParamConverter("regularPayment", class="PinoxBookkeepingBundle:RegularPayment")
     * @param Request $request
     * @param RegularPayment $regularPayment
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     */
    public function deleteAction(Request $request, RegularPayment $regularPayment)
    {
        $this->processEntityRemoval(
            $regularPayment,
            $request,
            $this->container
        );

        return $this->redirect($this->generateUrl('regular_payment_index'));
    }

}
