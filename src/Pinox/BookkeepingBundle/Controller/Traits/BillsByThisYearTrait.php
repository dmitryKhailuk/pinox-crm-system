<?php

namespace Pinox\BookkeepingBundle\Controller\Traits;

use Doctrine\ORM\EntityManager;
use Pinox\BookkeepingBundle\Entity\Type;

trait BillsByThisYearTrait
{

    /**
     * @param EntityManager $em
     * @param string $tableName
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @return array
     */
    public function getBillsByThisYear(EntityManager $em, $tableName,
                                       \DateTime $dateFrom = null, \DateTime $dateTo = null)
    {
        $qb = $em->getConnection()
            ->createQueryBuilder()
            ->select("date_trunc('month', b.date_operation) AS date, c.name AS currency, c.code as code, 
                SUM(CASE WHEN b.type_id = :positive THEN b.payment ELSE 0 END) AS positives_sum, 
                SUM(CASE WHEN b.type_id = :negative THEN b.payment ELSE 0 END) AS negative_sum")
            ->from($tableName, 'b')
            ->innerJoin('b', 'pinox_bookkeeping.currency', 'c', 'c.id = b.currency_id')
            ->groupBy("c.id, date_trunc('month', b.date_operation)")
            ->setParameters(['positive'=> Type::INCOMING, 'negative' => Type::LOSS]);

        if (null !== $dateFrom && is_object($dateFrom)) {
            $qb = $qb->andWhere('b.created_at >= :date_from')
                ->setParameter('date_from', $dateFrom->format('Y-m-d'));
        }

        if (null !== $dateTo && is_object($dateTo)) {
            $qb = $qb->andWhere('b.created_at <= :date_to')
                ->setParameter('date_to', $dateTo->format('Y-m-d') . ' 23:59:59');
        }
        return  $qb->execute()->fetchAll();
    }

}
