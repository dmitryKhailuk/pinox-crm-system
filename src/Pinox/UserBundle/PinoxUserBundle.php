<?php

namespace Pinox\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PinoxUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
