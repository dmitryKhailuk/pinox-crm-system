<?php

namespace Pinox\UserBundle\Form;

use Pinox\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    /** @var string password repeat  */
    private $passwordConfirm = null;

    /** @var string */
    private $acceptTermsAndConditions = false;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Email',
            ]
        ]);

        $builder->add('username', TextType::class, [
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Username',
            ]
        ]);

        $builder->add('password', PasswordType::class, [
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Password',
            ]
        ]);

        $builder->add('passwordConfirm', PasswordType::class, [
            'mapped' => $this->passwordConfirm,
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Confirm Password',
            ]
        ]);

        $builder->add('acceptTermsAndConditions', CheckboxType::class, [
            'mapped' => $this->acceptTermsAndConditions,
            'label' => false,
            'attr' => [
                'class' => 'checkbox-signup',
            ]
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'pinox_user_registration_type';
    }

}