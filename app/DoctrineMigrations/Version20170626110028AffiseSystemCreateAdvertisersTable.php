<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170626110028AffiseSystemCreateAdvertisersTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql(
            "CREATE TABLE affise_system.advertisers (
                  id SERIAL NOT NULL,
                  affise_id VARCHAR(50) NOT NULL,
                  title VARCHAR(80) DEFAULT NULL,
                  contact VARCHAR(80) DEFAULT NULL,
                  email VARCHAR (70) DEFAULT NULL,
                  url TEXT DEFAULT NULL,
                  manager VARCHAR (80) DEFAULT NULL,
                  skype VARCHAR (40) DEFAULT NULL,
                  offers INTEGER DEFAULT 0,
                  note TEXT DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX advertisers_affise_id_unq_idx
                ON affise_system.advertisers (affise_id)');

        $this->addSql('
            CREATE INDEX advertisers_email_unq_idx
                ON affise_system.advertisers (email)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP INDEX affise_system.advertisers_affise_id_unq_idx;;
        ');

        $this->addSql('
            DROP INDEX affise_system.advertisers_email_unq_idx;;
        ');

        $this->addSql('
            DROP TABLE affise_system.advertisers;
        ');
    }
}
