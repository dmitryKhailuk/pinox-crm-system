<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170529141023MigrateDataToCountries extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO countries (code, country, name, image) VALUES
                ('AW', 'Aruba', 'Аруба', 'Aruba.png'),
                ('BI', 'Burundi', 'Бурунди', 'Burundi.png'),
                ('BJ', 'Benin', 'Бенин', 'Benin.png'),
                ('BM', 'Bermuda', 'Бермудские Острова', 'Bermuda.png'),
                ('BN', 'Brunei Darussalam', 'Бруней', 'Brunei-Darussalam.png'),
                ('BT', 'Buthan', 'Бутан', 'Buthan.png'),
                ('BV', 'Bouvet Island', 'Буве', 'Bouvet-Island.png'),
                ('BW', 'Botswana', 'Ботсвана', 'Botswana.png'),
                ('CI', 'Ivory Coast', 'Кот-дИвуар', 'Cote-dIvoire.png'),
                ('CR', 'Costa Rica', 'Коста-Рика', 'Costa-Rica.png'),
                ('DO', 'Dominican Republic', 'Доминиканская Республика', 'Dominican-Republic.png'),
                ('ET', 'Ethiopia', 'Эфиопия', 'Ethiopia.png'),
                ('FM', 'Micronesia', 'Микронезия', 'Micronesia.png'),
                ('GA', 'Gabon', 'Габон', 'Gabon.png'),
                ('GD', 'Grenada', 'Гренада', 'Grenada.png'),
                ('GH', 'Ghana', 'Гана', 'Ghana.png'),
                ('GI', 'Gibraltar', 'Гибралтар', 'Gibraltar.png'),
                ('GM', 'Gambia', 'Гамбия', 'Gambia.png'),
                ('GN', 'Guinea', 'Гвинея', 'Guinea.png'),
                ('GQ', 'Equatorial Guinea', 'Экваториальная Гвинея', 'Equatorial-Guinea.png'),
                ('GT', 'Guatemala', 'Гватемала', 'Guatemala.png'),
                ('GY', 'Guyana', 'Гвиана', 'Guyana.png'),
                ('HN', 'Honduras', 'Гондурас', 'Honduras.png'),
                ('HT', 'Haiti', 'Гаити', 'Haiti.png'),
                ('ID', 'Indonesia', 'Индонезия', 'Indonesia.png'),
                ('IR', 'Iran', 'Иран', 'Iran.png'),
                ('IS', 'Iceland', 'Исландия', 'Iceland.png'),
                ('JM', 'Jamaica', 'Ямайка', 'Jamaica.png'),
                ('JO', 'Jordan', 'Иордания', 'Jordan.png'),
                ('KE', 'Kenya', 'Кения', 'Kenya.png'),
                ('KH', 'Cambodia', 'Камбоджа', 'Cambodia.png'),
                ('KI', 'Kiribati', 'Кирибати', 'Kiribati.png'),
                ('KW', 'Kuwait', 'Кувейт', 'Kuwait.png'),
                ('KY', 'Cayman Islands', 'Каймановы Острова', 'Cayman-Islands.png'),
                ('LA', 'Laos', 'Лаос', 'Laos.png'),
                ('LB', 'Lebanon', 'Ливан', 'Lebanon.png'),
                ('LC', 'Saint Lucia', 'Сент-Люсия', 'Saint-Lucia.png'),
                ('LK', 'Sri Lanka', 'Шри-Ланка', 'Sri-Lanka.png'),
                ('LY', 'Libya', 'Ливия', 'Libya.png'),
                ('MG', 'Madagascar', 'Мадагаскар', 'Madagascar.png'),
                ('ML', 'Mali', 'Мали', 'Mali.png'),
                ('MM', 'Myanmar', 'Мьянма', 'Myanmar.png'),
                ('MN', 'Mongolia', 'Монголия', 'Mongolia.png'),
                ('MO', 'Macau', 'Аомынь', 'Macau.png'),
                ('MT', 'Malta', 'Мальта', 'Malta.png'),
                ('MU', 'Mauritius', 'Маврикий', 'Mauritius.png'),
                ('MW', 'Malawi', 'Малави', 'Malawi.png'),
                ('MY', 'Malaysia', 'Малайзия', 'Malaysia.png'),
                ('MZ', 'Mozambique', 'Мозамбик', 'Mozambique.png'),
                ('NA', 'Namibia', 'Намибия', 'Namibia.png'),
                ('NE', 'Niger', 'Нигер', 'Niger.png'),
                ('NG', 'Nigeria', 'Нигерия', 'Nigeria.png'),
                ('NI', 'Nicaragua', 'Никарагуа', 'Nicaragua.png'),
                ('NP', 'Nepal', 'Непал', 'Nepal.png'),
                ('NR', 'Nauru', 'Науру', 'Nauru.png'),
                ('NU', 'Niue', 'Ниуэ', 'Niue.png'),
                ('NZ', 'New Zealand', 'Новая Зеландия', 'New-Zealand.png'),
                ('OM', 'Oman', 'Оман', 'Oman.png'),
                ('PG', 'Papua New', 'Папуа — Новая Гвинея', 'Papua-New.png'),
                ('PH', 'Philippines', 'Филиппины', 'Philippines.png'),
                ('PH', 'Philippines', 'Филиппины', 'Philippines.png'),
                ('PR', 'Puerto Rico', 'Пуэрто-Рико', 'Puerto-Rico.png'),
                ('PW', 'Palau', 'Палау', 'Palau.png'),
                ('QA', 'Qatar', 'Катар', 'Qatar.png'),
                ('RW', 'Rwanda', 'Руанда', 'Rwanda.png'),
                ('SB', 'Solomon Islands', 'Соломоновы Острова', 'Solomon-Islands.png'),
                ('SC', 'Seychelles', 'Сейшельские острова', 'Seychelles.png'),
                ('SD', 'Sudan', 'Судан', 'Sudan.png'),
                ('SG', 'Singapore', 'Сингапур', 'Singapore.png'),
                ('SL', 'Sierra Leone', 'Сьерра-Леоне', 'Sierra-Leone.png'),
                ('SM', 'San Marino', 'Сан-Марино', 'San-Marino.png'),
                ('SO', 'Somalia', 'Сомали', 'Somalia.png'),
                ('SR', 'Suriname', 'Суринам', 'Suriname.png'),
                ('SV', 'El Salvador', 'Сальвадор', 'El-Salvador.png'),
                ('TD', 'Chad', 'Чад', 'Chad.png'),
                ('TG', 'Togo', 'Того', 'Togo.png'),
                ('TN', 'Tunisia', 'Tunisia', 'Tunisia.png'),
                ('TO', 'Tonga', 'Тонга', 'Tonga.png'),
                ('TV', 'Tuvalu', 'Тувалу', 'Tuvalu.png'),
                ('TW', 'Taiwan', 'Тайвань', 'Taiwan.png'),
                ('TZ', 'Tanzania', 'Танзания', 'Tanzania.png'),
                ('VU', 'Vanuatu', 'Вануату', 'Vanuatu.png'),
                ('WS', 'Samoa', 'Самоа', 'Samoa.png'),
                ('YE', 'Yemen', 'Йемен', 'Yemen.png'),
                ('ZA', 'South Africa', 'Южно-Африканская Республика', 'South-Africa.png'),
                ('ZM', 'Zambia', 'Замбия', 'Zambia.png'),
                ('ZW', 'Zimbabwe', 'Зимбабве', 'Zimbabwe.png'),
                ('JE', 'Jersey', 'Джерси', 'Jersey.png'),
                ('YT', 'Mayotte', 'Майотта', 'Mayotte.png'),
                ('ER', 'Eritrea', 'Эритрея', 'Eritrea.png')
            ");
    }


    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // not need
    }
}
