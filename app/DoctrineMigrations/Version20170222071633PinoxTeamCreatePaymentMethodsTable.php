<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170222071633PinoxTeamCreatePaymentMethodsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_team.payment_methods (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX payment_methods_name_unique_idx
                ON pinox_team.payment_methods (lower(name))');

        $this->addSql("
            INSERT INTO pinox_team.payment_methods (name) VALUES
                ('постоплата'),
                ('предоплата')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->addSql('
            DROP TABLE pinox_team.payment_methods;
        ');

    }
}
