<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170327085054PinoxBookkeepingCreateDebentureTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.debenture (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  date_payment DATE NOT NULL,
                  currency_id INTEGER NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX debenture_name_unique_idx
                ON pinox_bookkeeping.debenture (lower(name))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_bookkeeping.debenture;
        ');
    }
}
