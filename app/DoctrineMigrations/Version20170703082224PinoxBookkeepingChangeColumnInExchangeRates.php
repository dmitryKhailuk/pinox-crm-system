<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170703082224PinoxBookkeepingChangeColumnInExchangeRates extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('DELETE FROM pinox_bookkeeping.exchange_rates');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.exchange_rates
              DROP COLUMN rate');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.exchange_rates
              ADD COLUMN rate DECIMAL(10,4) DEFAULT 0');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.exchange_rates
              DROP COLUMN rate');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.exchange_rates
              ADD COLUMN rate DECIMAL(10,2) DEFAULT 0');
    }
}
