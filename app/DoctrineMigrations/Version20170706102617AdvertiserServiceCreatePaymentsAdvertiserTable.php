<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170706102617AdvertiserServiceCreatePaymentsAdvertiserTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE advertiser_service.payments_advertiser (
                  id SERIAL NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  date_payment DATE DEFAULT NULL,
                  currency_id INTEGER NOT NULL,
                  advertiser_id INTEGER NOT NULL,
                  comment TEXT DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser 
                ADD CONSTRAINT fk_payment_advertisers_advertiser_id_advertisers_id 
                FOREIGN KEY (advertiser_id)
                REFERENCES affise_system.advertisers (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser 
                ADD CONSTRAINT fk_payment_advertisers_currency_id_currency_id 
                FOREIGN KEY (currency_id)
                REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser
                DROP CONSTRAINT fk_payment_advertisers_advertiser_id_advertisers_id;
        ');

        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser
                DROP CONSTRAINT fk_payment_advertisers_currency_id_currency_id;
        ');

        $this->addSql('
            DROP TABLE advertiser_service.payments_advertiser;
        ');

    }
}
