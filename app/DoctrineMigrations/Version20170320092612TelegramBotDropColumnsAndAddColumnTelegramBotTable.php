<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170320092612TelegramBotDropColumnsAndAddColumnTelegramBotTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              DROP COLUMN description'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              DROP COLUMN short_desc'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              DROP COLUMN commands'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              ADD COLUMN commands JSONB DEFAULT NULL'
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              ADD COLUMN description TEXT DEFAULT NULL'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              ADD COLUMN short_desc VARCHAR(128) DEFAULT NULL'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              ADD COLUMN commands TEXT DEFAULT NULLL'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              DROP COLUMN commands JSONB DEFAULT NULL'
        );

        $this->addSql('
            ALTER TABLE pinox_tool.telegram_bot 
              ADD COLUMN commands TEXT DEFAULT NULLL'
        );
    }
}
