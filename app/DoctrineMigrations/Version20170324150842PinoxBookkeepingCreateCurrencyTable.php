<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170324150842PinoxBookkeepingCreateCurrencyTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.currency (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  code VARCHAR (4) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX currency_name_unique_idx
                ON pinox_bookkeeping.currency (lower(name))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_bookkeeping.currency;
        ');
    }
}
