<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170328102202PinoxBookkeepingCreateRegularPaymentsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.regular_payments (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  date_payment DATE NOT NULL,
                  currency_id INTEGER NOT NULL,
                  periodicity_payment_id INTEGER NOT NULL,
                  type_id INTEGER NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX regular_payments_name_unique_idx
                ON pinox_bookkeeping.regular_payments (lower(name))');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments 
                ADD CONSTRAINT fk_regular_payments_currency_id_currency_id 
                FOREIGN KEY (currency_id)
                REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments 
                ADD CONSTRAINT fk_regular_payments_periodicity_payment_id_periodicity_payments_id 
                FOREIGN KEY (periodicity_payment_id)
                REFERENCES pinox_bookkeeping.periodicity_payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments 
                ADD CONSTRAINT fk_regular_payments_type_id_types_id 
                FOREIGN KEY (type_id)
                REFERENCES pinox_bookkeeping.types (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments
                DROP CONSTRAINT fk_regular_payments_currency_id_currency_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments
                DROP CONSTRAINT fk_regular_payments_periodicity_payment_id_periodicity_payments_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.regular_payments
                DROP CONSTRAINT fk_regular_payments_type_id_types_id;
        ');

        $this->addSql('
            DROP TABLE pinox_bookkeeping.regular_payments;
        ');

    }
}
