<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170707100630AffiseSystemAddColumnConvertedChargeToAdvertisers extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE affise_system.advertisers
              ADD COLUMN converted_charge DECIMAL(12,2) DEFAULT 0');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE affise_system.advertisers
              DROP COLUMN converted_charge');

    }
}
