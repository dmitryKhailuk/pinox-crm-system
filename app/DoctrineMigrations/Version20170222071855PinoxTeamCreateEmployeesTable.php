<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170222071855PinoxTeamCreateEmployeesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_team.employees (
                  id SERIAL NOT NULL,
                  name VARCHAR(35) NOT NULL,
                  surname VARCHAR(35) NOT NULL,
                  patronymic VARCHAR(35) DEFAULT NULL,
                  birthday DATE DEFAULT NULL,
                  passport VARCHAR (20) DEFAULT NULL,
                  phone VARCHAR (15) DEFAULT NULL,
                  address VARCHAR (150) DEFAULT NULL,
                  salary INTEGER DEFAULT 0, 
                  card_number VARCHAR (20) DEFAULT NULL,
                  date_employment DATE NOT NULL,
                  date_dismissal DATE DEFAULT NULL,
                  function_id INTEGER NOT NULL,
                  payment_method_id INTEGER NOT NULL,
                  comment TEXT DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );


        $this->addSql('
            CREATE INDEX employees_name_surname_unique_idx
                ON pinox_team.employees (lower(name), lower(surname))');

        $this->addSql('
            ALTER TABLE pinox_team.employees ADD CONSTRAINT fk_employees_function_id_functions_id 
                FOREIGN KEY (function_id)
                REFERENCES pinox_team.functions (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE pinox_team.employees ADD CONSTRAINT fk_employees_payment_method_id_payment_methods_id 
                FOREIGN KEY (payment_method_id)
                REFERENCES pinox_team.payment_methods (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE pinox_team.employees DROP CONSTRAINT fk_employees_function_id_functions_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_team.employees DROP CONSTRAINT fk_employees_payment_method_id_payment_methods_id;
        ');

        $this->addSql('
            DROP TABLE pinox_team.employees;
        ');

    }
}
