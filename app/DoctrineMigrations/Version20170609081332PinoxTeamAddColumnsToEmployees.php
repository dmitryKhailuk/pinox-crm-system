<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170609081332PinoxTeamAddColumnsToEmployees extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_team.employees
              ADD COLUMN currency_id INTEGER DEFAULT NULL ');

        $this->addSql('
            ALTER TABLE pinox_team.employees
                ADD CONSTRAINT fk_employees_currency_id_currency_id 
                FOREIGN KEY (currency_id)
                REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_team.employees
                DROP CONSTRAINT fk_employees_currency_id_currency_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_team.employees
              DROP COLUMN currency_id');


    }
}
