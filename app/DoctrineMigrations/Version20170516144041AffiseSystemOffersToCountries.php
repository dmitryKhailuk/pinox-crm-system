<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170516144041AffiseSystemOffersToCountries extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affise_system.offers_countries (
                  id SERIAL NOT NULL,
                  offer_id INTEGER NOT NULL,
                  country_id INTEGER NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE affise_system.offers_countries 
                ADD CONSTRAINT fk_offers_countries_offer_id_offers_id 
                FOREIGN KEY (offer_id)
                REFERENCES affise_system.offers (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE affise_system.offers_countries 
                ADD CONSTRAINT fk_offers_countries_country_id_countries_id 
                FOREIGN KEY (country_id)
                REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE affise_system.offers_countries
                DROP CONSTRAINT fk_offers_countries_offer_id_offers_id;
        ');

        $this->addSql('
            ALTER TABLE affise_system.offers_countries
                DROP CONSTRAINT fk_offers_countries_country_id_countries_id;
        ');

        $this->addSql('
            DROP TABLE affise_system.offers_countries;
        ');


    }
}
