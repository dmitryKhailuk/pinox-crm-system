<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170328104754PinoxBookkeepingAddConstraintForTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.debenture ADD CONSTRAINT fk_debenture_currency_id_currencies_id 
                FOREIGN KEY (currency_id)
                REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills_type
                DROP CONSTRAINT fk_bills_type_type_id_types_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills_type ADD CONSTRAINT fk_bills_type_type_id_types_id 
                FOREIGN KEY (type_id)
                REFERENCES pinox_bookkeeping.types (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.debenture
                DROP CONSTRAINT fk_debenture_currency_id_currency_id;
        ');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills_type
                DROP CONSTRAINT fk_bills_type_type_id_types_id;
        ');


    }
}
