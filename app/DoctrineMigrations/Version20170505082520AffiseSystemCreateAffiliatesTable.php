<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170505082520AffiseSystemCreateAffiliatesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affise_system.affiliates (
                  id SERIAL NOT NULL,
                  login VARCHAR(150) NOT NULL,
                  email VARCHAR(150) NOT NULL,
                  affise_id INTEGER NOT NULL,
                  active BOOLEAN NOT NULL DEFAULT FALSE,
                  api_key VARCHAR(50) DEFAULT NULL,
                  in_mailchump BOOLEAN NOT NULL DEFAULT FALSE,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX affiliates_affise_id_idx
                ON affise_system.affiliates (affise_id)');

        $this->addSql('
            CREATE INDEX affiliates_email_idx
                ON affise_system.affiliates (email)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP INDEX affise_system.affiliates_affise_id_idx;;
        ');

        $this->addSql('
            DROP INDEX affise_system.affiliates_email_idx;;
        ');

        $this->addSql('
            DROP TABLE affise_system.affiliates;
        ');
    }
}
