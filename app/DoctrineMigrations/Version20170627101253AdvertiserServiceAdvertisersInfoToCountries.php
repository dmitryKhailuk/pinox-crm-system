<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170627101253AdvertiserServiceAdvertisersInfoToCountries extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE advertiser_service.advertisers_info_countries (
                  id SERIAL NOT NULL,
                  advertiser_info_id INTEGER NOT NULL,
                  country_id INTEGER NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info_countries 
                ADD CONSTRAINT fk_advertisers_info_countries_advertiser_id_advs_id 
                FOREIGN KEY (advertiser_info_id)
                REFERENCES advertiser_service.advertisers_info (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info_countries 
                ADD CONSTRAINT fk_advertisers_info_countries_country_id_countries_id 
                FOREIGN KEY (country_id)
                REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info_countries
                DROP CONSTRAINT fk_advertisers_info_countries_advertiser_id_advs_id;
        ');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info_countries
                DROP CONSTRAINT fk_advertisers_info_countries_country_id_countries_id;
        ');

        $this->addSql('
            DROP TABLE advertiser_service.advertisers_info_countries;
        ');

    }
}
