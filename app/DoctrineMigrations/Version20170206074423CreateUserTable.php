<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170206074423CreateUserTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE auth.user (
                  id SERIAL NOT NULL,
                  username VARCHAR(50) NOT NULL,
                  username_canonical VARCHAR(50) NOT NULL,
                  email VARCHAR(40) NOT NULL,
                  email_canonical VARCHAR(40) NOT NULL,
                  enabled BOOLEAN NOT NULL,
                  salt VARCHAR(255) DEFAULT NULL::character varying,
                  password VARCHAR(255) NOT NULL,
                  last_login timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
                  confirmation_token character varying(180) DEFAULT NULL::character varying,
                  password_requested_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
                  roles text NOT NULL, -- (DC2Type:array)
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE auth.user;');
    }
}
