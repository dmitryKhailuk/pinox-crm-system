<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170529091052AffiseSystemCreateCategoriesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affise_system.categories (
                  id SERIAL NOT NULL,
                  affise_id VARCHAR(50) NOT NULL,
                  name VARCHAR(80) NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX categories_name_unq_idx
                ON affise_system.categories (name)');

        $this->addSql('
            CREATE INDEX categories_affise_id_unq_idx
                ON affise_system.categories (affise_id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE affise_system.categories;
        ');
    }
}
