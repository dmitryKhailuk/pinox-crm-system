<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170405094336PinoxBookkeepingCreateExchangeRatesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.exchange_rates (
                  id SERIAL NOT NULL,
                  code_rate VARCHAR(7) NOT NULL,
                  rate DECIMAL(10,2) DEFAULT 0,
                  currency_id INTEGER NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX exchange_rates_currency_id_idx
                ON pinox_bookkeeping.exchange_rates (currency_id)');

        $this->addSql('
            CREATE INDEX exchange_rates_code_rate_idx
                ON pinox_bookkeeping.exchange_rates (code_rate)');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.exchange_rates ADD CONSTRAINT fk_exchange_rates_currency_id_currency_id 
                FOREIGN KEY (currency_id)
                REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_bookkeeping.exchange_rates;
        ');

    }
}
