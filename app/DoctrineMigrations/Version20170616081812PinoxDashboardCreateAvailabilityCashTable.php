<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170616081812PinoxDashboardCreateAvailabilityCashTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_dashboard.availability_cash (
                  id SERIAL NOT NULL,
                  name VARCHAR(100) NOT NULL,
                  currency_id INTEGER NOT NULL,
                  payment DECIMAL(15,2) DEFAULT 0,
                  active BOOLEAN DEFAULT TRUE,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE pinox_dashboard.availability_cash 
                  ADD CONSTRAINT fk_availability_cash_currency_id_currency_id 
                  FOREIGN KEY (currency_id)
                  REFERENCES pinox_bookkeeping.currency (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_dashboard.availability_cash 
                DROP CONSTRAINT fk_availability_cash_currency_id_currency_id;
        ');
        $this->addSql('
            DROP TABLE pinox_dashboard.availability_cash;
        ');

    }
}
