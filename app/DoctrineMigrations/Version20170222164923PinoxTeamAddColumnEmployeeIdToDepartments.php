<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170222164923PinoxTeamAddColumnEmployeeIdToDepartments extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE pinox_team.departments 
              ADD COLUMN employee_id INTEGER 
              REFERENCES pinox_team.employees (id)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_team.departments DROP COLUMN employee_id;
        ');

    }
}
