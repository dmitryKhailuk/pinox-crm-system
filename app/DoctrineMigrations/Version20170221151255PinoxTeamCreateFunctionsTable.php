<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170221151255PinoxTeamCreateFunctionsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_team.functions (
                  id SERIAL NOT NULL,
                  name VARCHAR(30) NOT NULL,
                  department_id INTEGER DEFAULT NULL,
                  enabled BOOLEAN DEFAULT TRUE,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );


        $this->addSql('
            CREATE UNIQUE INDEX functions_name_department_id_unique_idx
                ON pinox_team.functions (lower(name), department_id)');

        $this->addSql('
            ALTER TABLE pinox_team.functions ADD CONSTRAINT fk_functions_department_id_departments_id 
                FOREIGN KEY (department_id)
                REFERENCES pinox_team.departments (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_team.functions DROP CONSTRAINT fk_functions_department_id_departments_id;
        ');

        $this->addSql('
            DROP TABLE pinox_team.functions;
        ');
    }
}
