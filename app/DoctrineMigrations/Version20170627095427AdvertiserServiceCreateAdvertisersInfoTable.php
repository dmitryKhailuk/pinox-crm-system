<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170627095427AdvertiserServiceCreateAdvertisersInfoTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE advertiser_service.advertisers_info (
                  id SERIAL NOT NULL,
                  traffic_source_id INTEGER DEFAULT NULL,
                  vertical_offer_id INTEGER DEFAULT NULL,
                  advertiser_id INTEGER NOT NULL,
                  comment TEXT DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info 
                ADD CONSTRAINT fk_advertisers_info_traffic_sources_id 
                FOREIGN KEY (traffic_source_id)
                REFERENCES affiliate_service.traffic_sources (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info 
                ADD CONSTRAINT fk_advertisers_info_vertical_offers_id 
                FOREIGN KEY (vertical_offer_id)
                REFERENCES affiliate_service.vertical_offers (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info 
                ADD CONSTRAINT fk_advertisers_info_advertiser_id_advertisers_id 
                FOREIGN KEY (advertiser_id)
                REFERENCES affise_system.advertisers (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info
                DROP CONSTRAINT fk_advertisers_info_traffic_sources_id;
        ');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info
                DROP CONSTRAINT fk_advertisers_info_vertical_offers_id;
        ');

        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info
                DROP CONSTRAINT fk_advertisers_info_advertiser_id_advertisers_id;
        ');

        $this->addSql('
            DROP TABLE advertiser_service.advertisers_info;
        ');

    }
}
