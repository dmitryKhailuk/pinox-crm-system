<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170310101805PinoxToolCreateTelegramBotTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_tool.telegram_bot (
                  id SERIAL NOT NULL,
                  name_bot VARCHAR(20) NOT NULL,
                  token VARCHAR(255) NOT NULL,
                  description TEXT DEFAULT NULL,
                  short_desc VARCHAR(128) DEFAULT NULL,
                  commands TEXT DEFAULT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql("
            INSERT INTO pinox_tool.telegram_bot (name_bot, token) VALUES
                ('PinoxBot', '319880294:AAED5j3vhmFHbtL5v1sqy2KQC8_EOQi8Z0E')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_tool.telegram_bot;
        ');

    }
}
