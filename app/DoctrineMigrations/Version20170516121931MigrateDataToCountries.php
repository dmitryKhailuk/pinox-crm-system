<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170516121931MigrateDataToCountries extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            INSERT INTO countries (code, country, name, image) VALUES
                ('AD', 'Andorra', 'Андорра', 'Andorra.png'),
                ('AE', 'United Arab Emirates', 'Объединенные Арабские Эмираты', 'United-Arab-Emirates.png'),
                ('AG', 'Antigua and Barbuda', 'Антигуа и Барбуда', 'Antigua-and-Barbuda.png'),
                ('AI', 'Anguilla', 'Антигуа и Барбуда', 'Anguilla.png'),
                ('AL', 'Albania', 'Албания', 'Albania.png'),
                ('AM', 'Armenia', 'Армения', 'Armenia.png'),
                ('AN', 'Netherland Antilles', 'Антильские Острова', 'Netherland-Antilles.png'),
                ('AO', 'Angola', 'Ангола', 'Angola.png'),
                ('AR', 'Argentina', 'Аргентина', 'Argentina.png'),
                ('AT', 'Austria', 'Австрия', 'Austria.png'),
                ('AU', 'Australia', 'Австралия', 'Australia.png'),
                ('AZ', 'Azerbaijan', 'Азербайджан', 'Azerbaijan.png'),
                ('BA', 'Bosnia-Herzegovina', 'Босния и Герцеговина', 'Bosnia-Herzegovina.png'),
                ('BB', 'Barbados', 'Барбадос', 'Barbados.png'),
                ('BD', 'Bangladesh', 'Бангладеш', 'Bangladesh.png'),
                ('BE', 'Belgium', 'Бельгия', 'Belgium.png'),
                ('BG', 'Bulgaria', 'Болгария', 'Bulgaria.png'),
                ('BH', 'Bahrain', 'Бахрейн', 'Bahrain.png'),
                ('BO', 'Bolivia', 'Боливия', 'Bolivia.png'),
                ('BR', 'Brazil', 'Бразилия', 'Brazil.png'),
                ('BS', 'Bahamas', 'Багамские Острова', 'Bahamas.png'),
                ('BY', 'Belarus', 'Беларусь', 'Belarus.png'),
                ('BZ', 'Belize', 'Белиз', 'Belize.png'),
                ('CA', 'Canada', 'Канада', 'Canada.png'),
                ('CH', 'Switzerland', 'Швейцария', 'Switzerland.png'),
                ('CL', 'Chile', 'Чили', 'Chile.png'),
                ('CM', 'Cameroon', 'Камерун', 'Cameroon.png'),
                ('CN', 'China', 'Китай', 'China.png'),
                ('CO', 'Colombia', 'Колумбия', 'Colombia.png'),
                ('CU', 'Cuba', 'Куба', 'Cuba.png'),
                ('CY', 'Cyprus', 'Кипр', 'Cyprus.png'),
                ('CZ', 'Czech Republic', 'Чехия', 'Czech-Republic.png'),
                ('DE', 'Germany', 'Германия', 'Germany.png'),
                ('DK', 'Denmark', 'Дания', 'Denmark.png'),
                ('DM', 'Dominica', 'Доминика', 'Dominica.png'),
                ('DZ', 'Algeria', 'Алжир', 'Algeria.png'),
                ('EC', 'Ecuador', 'Эквадор', 'Ecuador.png'),
                ('EE', 'Estonia', 'Эстония', 'Estonia.png'),
                ('EG', 'Egypt', 'Египет', 'Egypt.png'),
                ('ES', 'Spain', 'Испания', 'Spain.png'),
                ('FI', 'Finland', 'Финляндия', 'Finland.png'),
                ('FR', 'France', 'Франция', 'France.png'),
                ('GB', 'United Kingdom', 'Великобритания', 'United-Kingdom.png'),
                ('GE', 'Georgia', 'Грузия', 'Georgia.png'),
                ('GR', 'Greece', 'Греция', 'Greece.png'),
                ('HK', 'Hong Kong', 'Гонконг', 'Hong-Kong.png'),
                ('HR', 'Croatia', 'Хорватия', 'Croatia.png'),
                ('HU', 'Hungary', 'Венгрия', 'Hungary.png'),
                ('IE', 'Ireland', 'Ирландия', 'Ireland.png'),
                ('IL', 'Israel', 'Израиль', 'Israel.png'),
                ('IN', 'India', 'Индия', 'India.png'),
                ('IQ', 'Iraq', 'Ирак', 'Iraq.png'),
                ('IT', 'Italy', 'Италия', 'Italy.png'),
                ('JO', 'Jordan', 'Иордания', 'Jordan.png'),
                ('JP', 'Japan', 'Япония', 'Japan.png'),
                ('KG', 'Kirgistan', 'Кыргызстан', 'Kirgistan.png'),
                ('KZ', 'Kazachstan', 'Казахстан', 'Kazachstan.png'),
                ('LI', 'Liechtenstein', 'Лихтенштейн', 'Liechtenstein.png'),
                ('LT', 'Lithuania', 'Литва', 'Lithuania.png'),
                ('LU', 'Luxembourg', 'Люксембург', 'Luxembourg.png'),
                ('LV', 'Latvia', 'Латвия', 'Latvia.png'),
                ('MA', 'Morocco', 'Морокко', 'Morocco.png'),
                ('MC', 'Monaco', 'Монако', 'Monaco.png'),
                ('MD', 'Moldavia', 'Молдова', 'Moldavia.png'),
                ('MV', 'Maldives', 'Мальдивы', 'Maldives.png'),
                ('MX', 'Mexico', 'Мексика', 'Mexico.png'),
                ('NL', 'Netherlands', 'Нидерланды', 'Netherlands.png'),
                ('NO', 'Norway', 'Норвегия', 'Norway.png'),
                ('NZ', 'New Zealand', 'Новая Зеландия', 'New-Zealand.png'),
                ('PA', 'Panama', 'Панама', 'Panama.png'),
                ('PE', 'Peru', 'Перу', 'Peru.png'),
                ('PK', 'Pakistan', 'Пакистан', 'Pakistan.png'),
                ('PL', 'Poland', 'Польша', 'Poland.png'),
                ('PT', 'Portugal', 'Португалия', 'Portugal.png'),
                ('PY', 'Paraguay', 'Парагвай', 'Paraguay.png'),
                ('RO', 'Romania', 'Румыния', 'Romania.png'),
                ('RU', 'Russia', 'Россия', 'Russia.png'),
                ('SA', 'Saudi Arabia', 'Саудовская Аравия', 'Saudi-Arabia.png'),
                ('SE', 'Sweden', 'Швеция', 'Sweden.png'),
                ('SI', 'Slovenia', 'Словения', 'Slovenia.png'),
                ('SK', 'Slovak Republic', 'Словакия', 'Slovak-Republic.png'),
                ('SN', 'Senegal', 'Сенегал', 'Senegal.png'),
                ('SY', 'Syria', 'Сирия', 'Syria.png'),
                ('TH', 'Thailand', 'Таиланд', 'Thailand.png'),
                ('TJ', 'Tadjikistan', 'Таджикистан', 'Tadjikistan.png'),
                ('TM', 'Turkmenistan', 'Туркменистан', 'Turkmenistan.png'),
                ('TR', 'Turkey', 'Турция', 'Turkey.png'),
                ('UA', 'Ukraine', 'Украина', 'Ukraine.png'),
                ('UG', 'Uganda', 'Уганда', 'Uganda.png'),
                ('US', 'United States', 'Соединенные Штаты Америки', 'United-States.png'),
                ('UY', 'Uruguay', 'Уругвай', 'Uruguay.png'),
                ('UZ', 'Uzbekistan', 'Узбекистан', 'Uzbekistan.png'),
                ('VE', 'Venezuela', 'Венесуэла', 'Venezuela.png'),
                ('VN', 'Vietnam', 'Вьетнам', 'Vietnam.png'),
                ('ME', 'Montenegro', 'Черногория', 'Montenegro.png'),
                ('RS', 'Serbia', 'Сербия', 'Serbia.png'),
                ('AF', 'Afghanistan', 'Афганистан', 'Afghanistan.png'),
                ('MK', 'Republic of Macedonia', 'Македония', 'Republic-of-Macedonia.png'),
                ('PS', 'Palestine', 'Палестина', 'Palestine.png'),
                ('NC', 'Northern Cyprus', 'Турецкая Республика Северного Кипра', 'Northern-Cyprus.png')
            ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // not need
    }
}
