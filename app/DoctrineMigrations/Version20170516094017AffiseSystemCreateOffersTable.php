<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170516094017AffiseSystemCreateOffersTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affise_system.offers (
                  id SERIAL NOT NULL,
                  affise_id INTEGER NOT NULL,
                  offer_affise_id VARCHAR(60) NOT NULL,
                  title VARCHAR(250) DEFAULT NULL,
                  preview_url VARCHAR(255) DEFAULT NULL,
                  active BOOLEAN NOT NULL DEFAULT FALSE,
                  description TEXT DEFAULT NULL,
                  privacy VARCHAR(40) DEFAULT NULL,
                  is_top BOOLEAN DEFAULT FALSE,
                  currency VARCHAR(5) DEFAULT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX offers_privacy_id_idx
                ON affise_system.offers (privacy)');


        $this->addSql('
            CREATE INDEX offers_offer_affise_id_id_idx
                ON affise_system.offers (offer_affise_id)');

        $this->addSql('
            CREATE INDEX offers_currency_id_idx
                ON affise_system.offers (currency)');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE affise_system.offers;
        ');

    }
}
