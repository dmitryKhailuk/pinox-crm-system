<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170614135516AffiliateServiceCreateWebmastersTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affiliate_service.webmasters (
                  id SERIAL NOT NULL,
                  traffic_source_id INTEGER DEFAULT NULL,
                  vertical_offer_id INTEGER DEFAULT NULL,
                  affiliate_id INTEGER NOT NULL,
                  status_id INTEGER DEFAULT NULL,
                  reminder TEXT DEFAULT NULL,
                  date_reminder DATE DEFAULT NULL,
                  comment TEXT DEFAULT NULL,
                  recalled BOOLEAN DEFAULT FALSE,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters 
                ADD CONSTRAINT fk_webmasters_traffic_source_id_traffic_sources_id 
                FOREIGN KEY (traffic_source_id)
                REFERENCES affiliate_service.traffic_sources (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters 
                ADD CONSTRAINT fk_webmasters_vertical_offer_id_vertical_offers_id 
                FOREIGN KEY (vertical_offer_id)
                REFERENCES affiliate_service.vertical_offers (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters 
                ADD CONSTRAINT fk_webmasters_status_id_statuses_id 
                FOREIGN KEY (status_id)
                REFERENCES affiliate_service.statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters 
                ADD CONSTRAINT fk_webmasters_affiliate_id_affiliates_id 
                FOREIGN KEY (affiliate_id)
                REFERENCES affise_system.affiliates (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE affiliate_service.webmasters
                DROP CONSTRAINT fk_webmasters_traffic_source_id_traffic_sources_id;
        ');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters
                DROP CONSTRAINT fk_webmasters_vertical_offer_id_vertical_offers_id;
        ');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters
                DROP CONSTRAINT fk_webmasters_status_id_statuses_id;
        ');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters
                DROP CONSTRAINT fk_webmasters_affiliate_id_affiliates_id;
        ');

        $this->addSql('
            DROP TABLE affiliate_service.webmasters;
        ');

    }
}
