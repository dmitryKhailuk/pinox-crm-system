<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170328102059PinoxBookkeepingCreatePeriodicityPaymentsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.periodicity_payments (
                  id SERIAL NOT NULL,
                  name VARCHAR(50) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX periodicity_payments_name_unique_idx
                ON pinox_bookkeeping.periodicity_payments (lower(name))');

        $this->addSql("
            INSERT INTO pinox_bookkeeping.periodicity_payments (name) VALUES
                ('раз в неделю'),
                ('раз в 2 недели'),
                ('раз в месяц'),
                ('раз в год')
            ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_bookkeeping.periodicity_payments;
        ');

    }
}
