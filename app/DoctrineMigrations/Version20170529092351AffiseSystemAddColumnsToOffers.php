<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170529092351AffiseSystemAddColumnsToOffers extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE affise_system.offers
              ADD COLUMN is_send BOOLEAN DEFAULT FALSE');

        $this->addSql('
            ALTER TABLE affise_system.offers
              ADD COLUMN cr DECIMAL(10,2) DEFAULT 0');

        $this->addSql('
            ALTER TABLE affise_system.offers
              ADD COLUMN epc DECIMAL(10,2) DEFAULT 0');

        $this->addSql('
            ALTER TABLE affise_system.offers
              ADD COLUMN category_id INTEGER DEFAULT NULL');

        $this->addSql('
            ALTER TABLE affise_system.offers ADD CONSTRAINT fk_offers_category_id_categories_id 
                FOREIGN KEY (category_id)
                REFERENCES affise_system.categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE affise_system.offers
                DROP CONSTRAINT fk_offers_category_id_categories_id;
        ');

        $this->addSql('
            ALTER TABLE affise_system.offers
              DROP COLUMN is_send');

        $this->addSql('
            ALTER TABLE affise_system.offers
              DROP COLUMN cr');

        $this->addSql('
            ALTER TABLE affise_system.offers
              DROP COLUMN epc');

        $this->addSql('
            ALTER TABLE affise_system.offers
              DROP COLUMN category_id');
    }
}
