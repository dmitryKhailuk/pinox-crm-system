<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170516085427CountriesTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE countries (
                  id SERIAL NOT NULL,
                  code VARCHAR(4) NOT NULL,
                  country VARCHAR(100) NOT NULL,
                  name VARCHAR(150) DEFAULT NULL,
                  image VARCHAR(100) DEFAULT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX countries_code_id_idx
                ON countries (lower(code))');

        $this->addSql('
            CREATE INDEX countries_name_id_idx
                ON countries (name)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE countries;
        ');

    }
}
