<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170320134432TelegramBotAccountsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_tool.accounts (
                  id SERIAL NOT NULL,
                  chat_id VARCHAR(128) NOT NULL,
                  api_key VARCHAR(128) NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX accounts_chat_id_unique_indx
                ON pinox_tool.accounts (chat_id)'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_tool.accounts;
        ');

    }
}
