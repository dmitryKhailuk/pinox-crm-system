<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170609162606PinoxToolsCreateParticipantsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_tool.participants (
                  id SERIAL NOT NULL,
                  email VARCHAR(100) NOT NULL,
                  name VARCHAR(120) NOT NULL,
                  distance INTEGER DEFAULT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX participants_email_unq_idx
                ON pinox_tool.participants (email)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_tool.participants;
        ');
    }
}
