<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170615130926AffiliateServiceWebmastersToCountries extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE affiliate_service.webmasters_countries (
                  id SERIAL NOT NULL,
                  webmaster_id INTEGER NOT NULL,
                  country_id INTEGER NOT NULL,
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters_countries 
                ADD CONSTRAINT fk_webmasters_countries_webmaster_id_webmasters_id 
                FOREIGN KEY (webmaster_id)
                REFERENCES affiliate_service.webmasters (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters_countries 
                ADD CONSTRAINT fk_webmasters_countries_country_id_countries_id 
                FOREIGN KEY (country_id)
                REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters_countries
                DROP CONSTRAINT fk_webmasters_countries_webmaster_id_webmasters_id;
        ');

        $this->addSql('
            ALTER TABLE affiliate_service.webmasters_countries
                DROP CONSTRAINT fk_webmasters_countries_country_id_countries_id;
        ');

        $this->addSql('
            DROP TABLE affiliate_service.webmasters_countries;
        ');


    }
}
