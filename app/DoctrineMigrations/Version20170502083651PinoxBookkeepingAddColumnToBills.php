<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170502083651PinoxBookkeepingAddColumnToBills extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills
              ADD COLUMN date_operation DATE DEFAULT NULL');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills
              ADD COLUMN hashtag VARCHAR(250) DEFAULT NULL');

        $this->addSql('
            CREATE INDEX bills_hashtag_lower_idx
                ON pinox_bookkeeping.bills (lower(hashtag))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills DROP COLUMN date_operation;
        ');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills DROP COLUMN hashtag;
        ');

        $this->addSql('
            DROP INDEX pinox_bookkeeping.bills_hashtag_lower_idx;;
        ');

    }
}
