<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170629074621AdvertiserServiceAddColumnToAdvertisersInfo extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info
              ADD COLUMN active BOOLEAN DEFAULT TRUE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.advertisers_info
              DROP COLUMN active');
    }
}
