<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170220085947PinoxTeamCreateDepartmentsTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_team.departments (
                  id SERIAL NOT NULL,
                  name VARCHAR(50) NOT NULL,
                  parent_id INTEGER DEFAULT NULL,
                  enabled BOOLEAN DEFAULT TRUE,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE INDEX departments_parent_id_idx
                ON pinox_team.departments (parent_id);');

        $this->addSql('
            CREATE UNIQUE INDEX departments_name_unique_idx
                ON pinox_team.departments (lower(name))');

        $this->addSql('
            ALTER TABLE pinox_team.departments ADD CONSTRAINT fk_departments_parent_id_departments_id 
                FOREIGN KEY (parent_id)
                REFERENCES pinox_team.departments (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_team.departments DROP CONSTRAINT fk_departments_parent_id_departments_id;
        ');

        $this->addSql('
            DROP TABLE pinox_team.departments;
        ');

    }
}
