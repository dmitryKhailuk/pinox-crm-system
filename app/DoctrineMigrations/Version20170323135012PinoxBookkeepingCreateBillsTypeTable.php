<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170323135012PinoxBookkeepingCreateBillsTypeTable extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql(
            "CREATE TABLE pinox_bookkeeping.bills_type (
                  id SERIAL NOT NULL,
                  name VARCHAR(20) NOT NULL,
                  type_id INTEGER NOT NULL,
                  created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
                  PRIMARY KEY(id)
            );"
        );

        $this->addSql('
            CREATE UNIQUE INDEX bills_type_name_type_unique_idx
                ON pinox_bookkeeping.bills_type (lower(name), type_id)');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills_type ADD CONSTRAINT fk_bills_type_type_id_types_id 
                FOREIGN KEY (type_id)
                REFERENCES pinox_bookkeeping.types (id) NOT DEFERRABLE INITIALLY IMMEDIATE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DROP TABLE pinox_bookkeeping.bills_type;
        ');
    }
}
