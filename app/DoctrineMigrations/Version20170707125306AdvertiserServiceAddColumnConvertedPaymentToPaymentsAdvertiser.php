<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;


class Version20170707125306AdvertiserServiceAddColumnConvertedPaymentToPaymentsAdvertiser extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser
              ADD COLUMN converted_payment DECIMAL(12,2) DEFAULT 0');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE advertiser_service.payments_advertiser
              DROP COLUMN converted_payment');

    }
}
