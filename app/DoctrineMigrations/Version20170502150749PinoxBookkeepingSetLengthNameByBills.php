<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20170502150749PinoxBookkeepingSetLengthNameByBills extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills DROP COLUMN name');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills
              ADD COLUMN name VARCHAR (100) NOT NULL DEFAULT 1');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills DROP COLUMN name');

        $this->addSql('
            ALTER TABLE pinox_bookkeeping.bills
              ADD COLUMN name VARCHAR (20) NOT NULL');

    }
}
