pinox-crm
=========

A Symfony project created on January 30, 2017, 2:16 pm.

Работает в звязке nginx + php7-fpm. База данных на postgresql   
Настройка конфигуратора для nginx лешит в файле /server_settings.conf   

Устанавливаем компосер php -r "readfile('https://getcomposer.org/installer');" | php     
обновляем симфони php composer.phar install                                          
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX var/cache var/logs              
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX var/sessions var/logs           
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/cache var/logs             
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx var/sessions var/logs          
при первой уставоки необходимо выполнить sudo chown www-data:www-data {путь к проекту}     <br>



doctrine:migrations                                                                 
  :diff     Generate a migration by comparing your current database to your mapping information.        
  :execute  Execute a single migration version up or down manually.                                       
  :generate Generate a blank migration class.       
  :migrate  Execute a migration to a specified version or the latest available version.     
  :status   View the status of a set of migrations.     
  :version  Manually add and delete migration versions from the version table.  
 
пользователя с супер провами можно создать при поможи консоли fosuserbundle
необходим чтобы попасть в админ панель. /admin-panel
    
Навесить стили на систему можно при помощи команды 
sudo php bin/console assetic:dump
    
/***************** POSTGRESQL *************/
    
создание базы на локале 
sudo -u postgres psql -c "ALTER USER postgres PASSWORD 'postgres';" 
sudo -u postgres createdb "db_name" --template=template0 --encoding='UTF8' --lc-collate='ru_RU.UTF-8' --lc-ctype='ru_RU.UTF-8'  
    
sudo service postgresql restart 

/***************** POSTGRESQL (END) ************/   
